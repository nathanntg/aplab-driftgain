#include "stdafx.h"
#include "ExperimentBody.h"
#include "DriftGainApp.h"
#include <math.h>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <sys/stat.h>

// some math libraries define this, others do not
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

// #define SIMULATE 1

// uncomment to disable debugging
#define NDEBUG 1

// a debug macro to print extra logging
#ifdef NDEBUG
#define debug(e) ((void)0)
#else /* !NDEBUG */
#define debug(e) e
#endif /* NDEBUG */

// whether or not to try and load all images
// uncomment and it loads all images up front, otherwise loads at the beginning of each trial
// #define ALLOCATE_ALL 1

// whether or not to show a red cross at eye position
// #defnie DEBUG_RED_CROSS

// calibration increment (amount to move "+" when pressing joypad buttons
#define CALIBRATE_INCREMENT 1

// stream IDs
#define STREAM_X 0
#define STREAM_Y 1
#define STREAM_STATE 2
#define STREAM_MOTION 3
#define STREAM_SEARCH 3
#define STREAM_DETECT 3

// stream_state values to indicate what is happening
#define STREAM_STATE_STABILIZE 0
#define STREAM_STATE_EYEMOVE 1
#define STREAM_STATE_TOOFAST 2
#define STREAM_STATE_EDGE 4

// stream_motion values to indicate motion detection
#define STREAM_MOTION_NO_RESPONSE 0
#define STREAM_MOTION_NOT_VISIBLE 1
#define STREAM_MOTION_VISIBLE 2

// stream_search values to indicate what is happening
#define STREAM_SEARCH_NO_RESPONSE 0
#define STREAM_SEARCH_FOUND 1

// stream_detect values to indicate what is happening
#define STREAM_DETECT_NO_RESPONSE 0
#define STREAM_DETECT_START_CHANGE 1
#define STREAM_DETECT_IDENTIFY_BEFORE 2
#define STREAM_DETECT_IDENTIFY_AFTER 3

// timing constants (not worth new parameters)
#define TIME_CHANGE_MIN_BEFORE 750.f
#define TIME_CHANGE_RANGE 4250.f
#define TIME_CHANGE_TRANSITION 750.f

// number of degrees for rotation (not worth new parameters)
#define CHANGE_ROTATE_DEGREES 15.f

// whether or not to pause the stabilization in certain instances
// comment out to continue to stabilize
#define PAUSE_STABILIZATION_EDGE 0.05
#define PAUSE_STABILIZATION_FAST 4.f

// round image positions
#define ROUND_IMAGE_POS

// add in operator to allow ORing EOS_TRIGGERS together (allows one call to CTriggers::any instead of multiple)
inline EOS_TRIGGERS operator|(EOS_TRIGGERS a, EOS_TRIGGERS b) {
    return static_cast<EOS_TRIGGERS>(static_cast<int>(a) | static_cast<int>(b));
}

// constructor
ExperimentBody::ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params) : CExperiment(pxWidth, pxHeight, RefreshRate) {
    setExperimentName("DriftGain");

    // store parameter configuration file
    m_paramsFile = Params;

    // get subject name
    m_subjectName = m_paramsFile->getString(CFG_SUBJECT_NAME);

	// get task
	std::string task_string = m_paramsFile->getString(CFG_TASK);
	if ("FREEVIEW" == task_string) {
		m_task = TASK_FREE_VIEW;
	}
	else if ("STIMULI" == task_string) {
		m_task = TASK_STIMULI;
	}
    else if ("SEARCH" == task_string) {
        m_task = TASK_SEARCH;
    }
    else if ("SCOTOMA" == task_string) {
        m_task = TASK_SCOTOMA;
    }
    else if ("DETECT_MOTION" == task_string) {
        m_task = TASK_DETECT_MOTION;
    }
    else if ("DETECT_CHANGE" == task_string) {
        m_task = TASK_DETECT_CHANGE;
    }
	else {
		m_task = TASK_FREE_VIEW;
	}

    // fast stabilization
    m_fastStabilization = m_paramsFile->getBoolean(CFG_FAST_STABILIZATION);
}

// random generator function:
int random_int(int i) {
    return std::rand() % i;
}

void ExperimentBody::initialize() {
	// deactivate unneeded features
    disable(CExperiment::EIS_PHOTOCELL); // photo cell for measuring timing of stimuli
    disable(CExperiment::EIS_STAT1); // statistic output to screen
    disable(CExperiment::EIS_STAT2); // statistic output to screen
    disable(CExperiment::EIS_NOTRACK_ICON); // tracking status icon on screen
    disable(CExperiment::EIS_JP_STRT); // ability to exit using "joypad start" button

    // enable statistics
    debug(enable(CExperiment::EIS_STAT1)); // statistic output to screen
    debug(enable(CExperiment::EIS_STAT2)); // statistic output to screen

    // seed trial number and default gain
    m_trialNumber = 0;
    m_driftGain = 1.f; // no gain
    setDriftAngle(0.f);

    // no active image
    m_imageIndex = 0;
    m_active = NULL;


    //--------------- CALIBRATION -----------------------
    // set the pixel increment for the test calibration procedure during the exp
    m_xShift = 0;
    m_yShift = 0;
    m_xPos = 0;
    m_yPos = 0;

    // crosses for the recalibration trials
    m_redCross = addObject(new CImagePlane("images/redcross.tga"));
    m_redCross->enableTrasparency(true);
    m_redCross->hide();

    m_whiteCross = addObject(new CImagePlane("images/whitecross.tga"));
    m_whiteCross->enableTrasparency(true);
    m_whiteCross->hide();

    // always start with a calibration
    if (m_paramsFile->getInteger(CFG_CALIBRATE_EVERY) < 5) {
        m_trialsUntilCalibration = 0;
    }
    else {
        m_trialsUntilCalibration = m_paramsFile->getInteger(CFG_CALIBRATE_EVERY);
    }

    // initial state: loading
    hideAllObjects();
    m_state = STATE_LOADING;
    m_timer.start(1000);
	
	// print task
	switch (m_task) {
		case TASK_FREE_VIEW:
			CEnvironment::Instance()->outputMessage("Task: FREE VIEW. Subject: %s", m_subjectName.c_str());
			break;
		case TASK_SEARCH:
			CEnvironment::Instance()->outputMessage("Task: SEARCH. Subject: %s", m_subjectName.c_str());
			break;
        case TASK_SCOTOMA:
            CEnvironment::Instance()->outputMessage("Task: SCOTOMA. Subject: %s", m_subjectName.c_str());
            break;
		case TASK_STIMULI:
			CEnvironment::Instance()->outputMessage("Task: STIMULI. Subject: %s", m_subjectName.c_str());
			break;
		case TASK_DETECT_MOTION:
			CEnvironment::Instance()->outputMessage("Task: DETECT MOTION. Subject: %s", m_subjectName.c_str());
			break;
        case TASK_DETECT_CHANGE:
            CEnvironment::Instance()->outputMessage("Task: DETECT CHANGE. Subject: %s", m_subjectName.c_str());
            break;
	}

    // enable slow stabilization
	if (m_fastStabilization) {
		CEnvironment::Instance()->outputMessage("Fast stabilization.");
	}
	else {
		CEnvironment::Instance()->outputMessage("Slow stabilization.");
	}
    CStabilizer::Instance()->enableSlowStabilization(!m_fastStabilization);

    // seed random number generator
	std::srand((unsigned)time(NULL));
}

void ExperimentBody::finalize() {
	// Stop recording
	endTrial();
}

void ExperimentBody::_loadDriftParams() {
    std::string file = "data/" + m_subjectName + "/gain.txt", cur_params;

    // get the image list
    ifstream list_in;

	struct stat buffer;   
	if (stat(file.c_str(), &buffer) == 0) {
		list_in.open(file.c_str());
	}
	else {
        CEnvironment::Instance()->outputMessage("Using global gain parameters.");

		// use global file
        file = "data/gain.txt";
		list_in.open(file.c_str());
	}

	// unable to open
    if (!list_in.is_open()) {
        CEnvironment::Instance()->outputMessage("No gain parameters found. Using default values.");
        return;
    }

    // load a list of file names
    while (std::getline(list_in, cur_params)) { // read a line
        debug(CEnvironment::Instance()->outputMessage("Read %s.", cur_params.c_str()));

        // push to the back
        m_driftParams.push_back(cur_params);
    }

    // shuffle the list of parameters
	std::random_shuffle(m_driftParams.begin(), m_driftParams.end(), random_int);

    // start at parameter 0
    m_driftParamIndex = 0;
	
	debug(CEnvironment::Instance()->outputMessage("Found %d drift parameters.", m_driftParams.size()));
}

void ExperimentBody::_shuffleImageIndices() {
    // shuffle the list of images
	std::random_shuffle(m_imageIndices.begin(), m_imageIndices.end(), random_int);
}

void ExperimentBody::_loadImages() {
    std::string directory = "images/", list_file, cur_file;

    if (TASK_STIMULI == m_task) {
        directory += "stimuli/";
    }
    else if (TASK_DETECT_CHANGE == m_task) {
        directory += "change/";
    }
    else if (TASK_SEARCH == m_task) {
        directory += "search/";
    }
    else {
        directory += "natural/";
    }

    // get the image list
	list_file = directory + "images.txt";
	ifstream list_in(list_file.c_str());

    // open file
    if (!list_in.is_open()) {
        CEnvironment::Instance()->outputMessage("Unable to load the image list (%simages.txt).  Please check that the file exists.", directory.c_str());
        return;
    }

    // image
    CImagePlane* cur_image;

    // load a list of file names
    while (std::getline(list_in, cur_file)) { // read a line
        // add object
#ifdef ALLOCATE_ALL
		debug(CEnvironment::Instance()->outputMessage("Preloading %s%s.", directory.c_str(), cur_file.c_str()));
        cur_image = addObject(new CImagePlane(directory + cur_file));
#else
        cur_image = addObject(new CImagePlane());
#endif
        cur_image->pxSetPosition(0, 0);
        cur_image->hide();

        // store index, image and image name
        m_imageIndices.push_back(m_images.size());
        m_images.push_back(cur_image);
        m_imageNames.push_back(cur_file);
    }

    // close file
    list_in.close();

    // shuffle the list of images
    _shuffleImageIndices();
	
	// print images found
	debug(CEnvironment::Instance()->outputMessage("Found %d images for the task.", m_images.size()));
}

void ExperimentBody::_loadJostleData(const std::string &file_name) {
    double drift_gain, drift_angle;
    unsigned int ts;
    double adj_x, adj_y;

    // load file name
    CInFileDSPDataStream in_trial = CInFileDSPDataStream();

    // log it
    CEnvironment::Instance()->outputMessage("Read jostle list: %s.", file_name.c_str());

    // clear jostle lists
    m_xJostle.clear();
    m_yJostle.clear();

    // load trial
    in_trial.load(file_name);

    // get information
    drift_gain = in_trial.retrieveDoubleVariable("DriftGain");
    drift_angle = in_trial.retrieveDoubleVariable("DriftAngle");

    // found values
    CEnvironment::Instance()->outputMessage("Jostle list had gain %f and angle %f.", drift_gain, drift_angle);

    // iterate over data
    while (!in_trial.eos()) {
        // read image adjustment
        if (in_trial.retrieveStreamData(STREAM_X, &ts, &adj_x) && in_trial.retrieveStreamData(STREAM_Y, &ts, &adj_y)) {
            // add to jostle list
            m_xJostle.push_back((float)adj_x);
            m_yJostle.push_back((float)adj_y);
        }

        // next frame
        in_trial.nextFrame();
    }

    // free space
    in_trial.clear();

    // log it
    CEnvironment::Instance()->outputMessage("Found jostle %d jostle points.", m_xJostle.size());
}

bool ExperimentBody::_advanceImage() {
    size_t currentImageIndex;

	debug(CEnvironment::Instance()->outputMessage("Advancing image."));

    // hide current image
    if (m_active) {
        m_active->hide();
        m_active = NULL;
    }

    // ensure there are more images remaining
    if (0 >= m_remainingImages) {
        debug(CEnvironment::Instance()->outputMessage("No more images remaining in the session."));
        return false;
    }

    // decrement remaining images
    --m_remainingImages;

    // has gain specific images
    if (m_gainSpecificImages.empty()) { // NO
        // end of image list
        if (m_images.size() <= m_imageIndex) {
            // don't loop for free view
            if (TASK_FREE_VIEW == m_task) {
                CEnvironment::Instance()->outputMessage("Exhausted image list.");
                return false;
            }

            // reshuffle
            _shuffleImageIndices();

            // reset index
            m_imageIndex = 0;
        }

        // use image index
        currentImageIndex = m_imageIndices[m_imageIndex];

        // advance image index for next iteration
        ++m_imageIndex;
    }
    else { // YES, has gain specific image
        // use popped value for next image index
        currentImageIndex = m_gainSpecificImages.back();

        // remove it
        m_gainSpecificImages.pop_back();
    }

    // print new image name
    debug(CEnvironment::Instance()->outputMessage("Showing image: %s.", m_imageNames[currentImageIndex].c_str()));

    // add to list of trial images
    if (m_trialImages.empty()) {
        m_trialImages += m_imageNames[currentImageIndex];
    }
    else {
        m_trialImages += ";" + m_imageNames[currentImageIndex];
    }

    // set active image
    m_active = m_images[currentImageIndex];
    m_active->show();

    // set eyes moving, since the image just moved (don't stabilize)
    m_eyeMovement = true;
    m_activeXPos = 0.f;
    m_activeYPos = 0.f;

    return true;
}

void ExperimentBody::eventRender(unsigned int FrameCount, CEOSData* Samples) {
    // velocity signal from the dsp
    float velocity, x, y;
    velocity = Samples->vchannels[4][7];
    // add the recalibration offset
    CConverter::Instance()->a2p(Samples->x1, Samples->y1, x, y);
    x += m_xShift;
    y += m_yShift;

    // debug: (x = 0, y = 0)
#ifdef SIMULATE
	x = 0;
	y = 0;
#endif

    switch (m_state) {
        case STATE_LOADING:
            COGLEngine::Instance()->clearScreen();
            glColor3d(255, 255, 255);
            printCentered(CFontEngine::FONTS_ARIAL_18, 0, 0, "Loading...");

            // Set the background color for the loading screen
            COGLEngine::Instance()->setBackgroundColor(200, 0, 0);

            // Copy the parameter file into the subject directory
            if (m_timer.isExpired()) {
                // load mask
                if (TASK_DETECT_MOTION == m_task) {
                    debug(CEnvironment::Instance()->outputMessage("Loading mask."));
                    m_mask = addObject(new CImagePlane("images/mask.tga"));
                    m_mask->pxSetPosition(0, 0);
                    m_mask->hide();
                    m_mask->enableTrasparency(true);
                }

                // load scotoma
                if (TASK_SCOTOMA == m_task) {
                    debug(CEnvironment::Instance()->outputMessage("Loading scotoma."));
                    m_scotoma = addObject(new CImagePlane("images/scotoma.tga"));
                    m_scotoma->pxSetPosition(0, 0);
                    m_scotoma->hide();
                    m_scotoma->enableTrasparency(true);
                }

                // load images
				debug(CEnvironment::Instance()->outputMessage("Loading images."));
                _loadImages();

                // load drift recipes
				debug(CEnvironment::Instance()->outputMessage("Loading drift parameters."));
                _loadDriftParams();

                // go to the task
                _switchToState(STATE_CUE);
            }

            break;

        case STATE_CUE:
            // print task description top center of screen
            glColor3d(255, 255, 255);
            printCentered(CFontEngine::FONTS_ARIAL_18, 0, -0.7f * m_pxHeight / 2.f, _getTaskDescription());

            // Set the background color for the experiment
            COGLEngine::Instance()->setBackgroundColor(0, 0, 0);

            // begin task
            if (m_timer.isExpired()) {
#ifndef ALLOCATE_ALL
                // load images on demand
                size_t maxi;
                size_t index = m_imageIndex, image, gain_specific = m_gainSpecificImages.size();
                std::string directory = "images/";

                // maximum number of images that will be shown
                if (TASK_STIMULI == m_task) {
                    maxi = m_paramsFile->getInteger(CFG_STIMULI_PER_SESSION);
                    directory += "stimuli/";
                }
                else {
                    maxi = m_paramsFile->getInteger(CFG_IMAGE_PER_SESSION);
                    directory += "natural/";
                }

                for (size_t i = 0; i < maxi; ++i) {
                    if (i < gain_specific) {
                        // load from back, since popped from back
                        image = m_gainSpecificImages[gain_specific - i - 1];
                    }
                    else {
                        // lookup index in shuffled vector
                        image = m_imageIndices[index - gain_specific];
                    }

                    // load image
                    if (!m_images[image]->getTexture()) {
						// loading image
						debug(CEnvironment::Instance()->outputMessage("Loading image %s%s.", directory.c_str(), m_imageNames[image].c_str()));

                        m_images[image]->loadImage(directory + m_imageNames[image]);
                    }

                    // increment index
                    ++index;
                    if (index >= m_imageIndices.size()) { // loop around
                        index = 0;
                    }
                }
#endif

                // switch state
                _switchToState(STATE_TASK);
            }

            break;

        case STATE_TESTCALIBRATION:
            // Set the background color for the calibration
            COGLEngine::Instance()->setBackgroundColor(127, 127, 127);

            // hide all objects
            hideAllObjects();
            if (m_timer.isExpired()) {
                // get
                moveToFront(m_redCross);

                // set red cross position based on x / y position (controlled by joypad)
                m_redCross->pxSetPosition(x + m_xPos, y + m_yPos);
                m_redCross->show();

                // center
                m_whiteCross->pxSetPosition(0,0);
                m_whiteCross->show();
            }
            break;

        case STATE_CHANGE:
            // rotate
            if (m_active) {
                // use x^2 to create an "ease in" transition
                float s = (m_timer.getTime() / m_timer.getDuration());
                s *= s;
                m_active->degSetAngle(s * CHANGE_ROTATE_DEGREES);
            }

        case STATE_TASK:
            // has image expired?
            if (m_timer.isExpired() || !m_active) {
                // if task is detect motion and there is still an active image, then count as time out
                if (TASK_DETECT_MOTION == m_task && m_active) {
                    // output message
                    CEnvironment::Instance()->outputMessage("MOTION: No Response.");

                    // add to trial stream
                    storeTrialStream(STREAM_MOTION, STREAM_MOTION_NO_RESPONSE);
                }

                // if task is search and there is an active image
                if ((TASK_SCOTOMA == m_task || TASK_SEARCH == m_task) && m_active) {
                    // output message
                    CEnvironment::Instance()->outputMessage("SEARCH: No Response.");

                    // add to trial stream
                    storeTrialStream(STREAM_SEARCH, STREAM_SEARCH_NO_RESPONSE);

                    // switch to the cue state
                    _switchToState(STATE_CUE);
                    break;
                }

                // if task is detect change
                if (TASK_DETECT_CHANGE == m_task && m_active) {
                    if (STATE_CHANGE == m_state) {
                        // output message
                        CEnvironment::Instance()->outputMessage("DETECT: No Response.");

                        // add to trial stream
                        storeTrialStream(STREAM_DETECT, STREAM_DETECT_NO_RESPONSE);

                        // switch to the feedback state
                        m_feedback = "Too late. Try again.";
                        _switchToState(STATE_FEEDBACK);
                        break;
                    }

                    // output message
                    CEnvironment::Instance()->outputMessage("DETECT: Change.");

                    // add to trial stream
                    storeTrialStream(STREAM_DETECT, STREAM_DETECT_START_CHANGE);

                    // switch state
                    _switchToState(STATE_CHANGE);

                    // still stabilize (seamless)
                    _stabilizeActiveObject(Samples);

                    break;
                }

                // advance image
                if (_advanceImage()) { // image advanced successfully
                    // get duration
                    float time;
                    if (TASK_DETECT_CHANGE == m_task) {
                        // randomly generate time until change
                        float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
                        time = TIME_CHANGE_MIN_BEFORE + (TIME_CHANGE_RANGE * r);
                    }
                    else if (TASK_STIMULI == m_task) {
                        time = m_paramsFile->getFloat(CFG_STIMULI_TIME);
                    }
                    else {
                        time = m_paramsFile->getFloat(CFG_IMAGE_TIME);
                    }

                    // start timer
                    m_timer.start(time);
                }
                else { // no new images to show or done with session
                    // switch to the cue state
                    _switchToState(STATE_CUE);
                    break;
                }
				
#ifdef DEBUG_RED_CROSS
				// move front
				moveToFront(m_redCross);
                m_redCross->show();
#endif
            }

            // has active object? stabilize it
            if (m_active) {
                if (TASK_DETECT_MOTION == m_task && 10 < m_driftGain) {
                    _jostleActiveObject(Samples);
                }
                else {
                    _stabilizeActiveObject(Samples);
                }

#ifdef DEBUG_RED_CROSS
                // set red cross position based on eye position
                m_redCross->pxSetPosition(x + m_xPos, y + m_yPos);
#endif
            }

            break;

        case STATE_FEEDBACK:
            // print task description top center of screen
            glColor3d(255, 255, 255);
            printCentered(CFontEngine::FONTS_ARIAL_18, 0, -0.7f * m_pxHeight / 2.f, m_feedback);

            // Set the background color for the experiment
            COGLEngine::Instance()->setBackgroundColor(0, 0, 0);

            // begin task
            if (m_timer.isExpired()) {
                // switch state
                _switchToState(STATE_CUE);
            }

            break;
    }
}

void ExperimentBody::eventJoypad() {
    if (STATE_TESTCALIBRATION == m_state) {
        if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_UP)) { // cursor: up
            m_yPos += CALIBRATE_INCREMENT;
        }
        else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_DOWN)) { // cursor: down
            m_yPos -= CALIBRATE_INCREMENT;
        }
        else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_RGHT)) { // cursor: right
            m_xPos += CALIBRATE_INCREMENT;
        }
        else if (CDriver_Joypad::Instance()->getButtonPressed(CDriver_Joypad::JPAD_BUTTON_LEFT)) { // cursor: left
            m_xPos -= CALIBRATE_INCREMENT;

        }
        if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) { // r1 button: finalize response
            // end test calibration, only if timer has expired
            if (m_timer.isExpired()) {
                ////CEnvironment::Instance()->outputMessage("RsponseFinalize event Joypad");
                _switchToState(STATE_CUE);
            }
        }
    }
    else if (STATE_TASK == m_state) {
        if (TASK_STIMULI == m_task) {
            // done with the current session
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
                _switchToState(STATE_CUE);
            }
        }
        else if (TASK_SEARCH == m_task || TASK_SCOTOMA == m_task) {
            // TODO: potentially make it detect if within certain radius of target? need to have meta data for all photos
            // done with the current session
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
                // record it
                storeTrialStream(STREAM_SEARCH, STREAM_SEARCH_FOUND);

                // switch back to CUE
                _switchToState(STATE_CUE);
            }
        }
        else if (TASK_DETECT_MOTION == m_task) {
            // minimum duration of 200ms
            if (200 > m_timer.getTime()) {
                return;
            }

            // right: yes motion
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
                // log to output
                CEnvironment::Instance()->outputMessage("MOTION: YES Visible.");

                // track stream data
                storeTrialStream(STREAM_MOTION, STREAM_MOTION_VISIBLE);

                // hide image (causes image to advance)
                m_active->hide();
                m_active = NULL;
            }

            // left: no motion
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_L1)) {
                // log to output
                CEnvironment::Instance()->outputMessage("MOTION: NOT Visible.");

                // track stream data
                storeTrialStream(STREAM_MOTION, STREAM_MOTION_NOT_VISIBLE);

                // hide image (causes image to advance)
                m_active->hide();
                m_active = NULL;
            }
        }
        else if (TASK_DETECT_CHANGE == m_task) {
            // minimum duration of 200ms
            if (200 > m_timer.getTime()) {
                return;
            }

            // right: yes motion
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
                // log to output
                CEnvironment::Instance()->outputMessage("DETECT: wrong.");

                // write to stream
                storeTrialStream(STREAM_DETECT, STREAM_DETECT_IDENTIFY_BEFORE);

                // show feedback
                m_feedback = "Too soon! Try again.";
                _switchToState(STATE_FEEDBACK);
            }
        }
    }
    else if (STATE_CHANGE == m_state) {
        if (TASK_DETECT_CHANGE == m_task) {
            // right: yes motion
            if (CDriver_Joypad::Instance()->getButtonStatus(CDriver_Joypad::JPAD_BUTTON_R1)) {
                // log to output
                CEnvironment::Instance()->outputMessage("DETECT: right.");

                // write to stream
                storeTrialStream(STREAM_DETECT, STREAM_DETECT_IDENTIFY_AFTER);

                // show feedback
                if (350 > m_timer.getTime()) {
                    m_feedback = "Great work!";
                }
                else {
                    m_feedback = "Good work.";
                }
                _switchToState(STATE_FEEDBACK);
            }
        }
    }
}

bool ExperimentBody::_isNonDriftMovement(CEOSData* Samples) {
    // should not track?
    if (CTriggers::any(Samples->triggers, Samples->samplesNumber, EOS_TRIG_1_EMEVENT | EOS_TRIG_BLINK | EOS_TRIG_NOTRACK)) {
        return true;
    }

    // potentially use velocity measurement?
//    for (int i = 0; i < Samples->samplesNumber; i++) {
//        // TODO: find out if VCHANNEL_7 is indeed velocity, and figure out units
//        if (Samples->vchannels[i][EOS_VCHANNEL_7] > 180) {
//            return true;
//        }
//    }

    return false;
}

void ExperimentBody::setDriftAngle(float angle) {
    m_driftAngle = angle;

    // precalculate cos and sin for reuse
    m_driftAngleCos = cos(angle);
    m_driftAngleSin = sin(angle);
}

void ExperimentBody::_jostleActiveObject(CEOSData* Samples) {
    // get stabilized position
    float eye_x, eye_y, delta_x, delta_y;

    // get eye position
    CStabilizer::Instance()->stabilize(Samples, eye_x, eye_y);

    // is making an eye movement
    if (_isNonDriftMovement(Samples)) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_EYEMOVE); // state: eye movement

        m_eyeMovement = true;
        return;
    }

    // delta (eye movement since last stabilization)
    delta_x = (eye_x - m_xDriftOffset);
    delta_y = (eye_y - m_yDriftOffset);

    // movements more than 2 arcmin should be viewed as eye movements
#ifdef PAUSE_STABILIZATION_FAST
    if (PAUSE_STABILIZATION_FAST < ((delta_x * delta_x) + (delta_y * delta_y))) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_TOOFAST); // state: eye moving too fast
		debug(CEnvironment::Instance()->outputMessage("IGNORE - Too Fast"));

        // view as start of eye movement (don't stabilize)
        m_eyeMovement = true;

		// update drift offset for measuring velocity
		m_xDriftOffset = eye_x;
		m_yDriftOffset = eye_y;

        return;
    }
#endif

    // edge detection
#ifdef PAUSE_STABILIZATION_EDGE
    if (fabs(eye_x + m_xShift) > m_pxWidth * 0.5 * (1 - PAUSE_STABILIZATION_EDGE) || fabs(eye_y + m_yShift) > m_pxHeight * 0.5 * (1 - PAUSE_STABILIZATION_EDGE)) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_EDGE); // state: eye near edge
		debug(CEnvironment::Instance()->outputMessage("IGNORE - Near Edge"));

        // view as start of eye movement (don't stabilize)
        m_eyeMovement = true;

        return;
    }
#endif

    // if was moving during last sample? don't do any stabilization
    if (m_eyeMovement) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_STABILIZE); // state: stabilize

        // end of eye movement
        m_eyeMovement = false;
    }
    else {
        float adj_x, adj_y;

        // read from jostle list
        adj_x = m_xJostle[m_jostleIndex] * (m_driftGain - 20.f); // allow scaling (e.g., 21 == 100% jostle)
        adj_y = m_yJostle[m_jostleIndex] * (m_driftGain - 20.f); // allow scaling

        // advance jostle index
        if (++m_jostleIndex >= m_xJostle.size()) {
            m_jostleIndex = 0;
        }

        // store stream data
        storeTrialStream(STREAM_X, adj_x);
        storeTrialStream(STREAM_Y, adj_y);
        storeTrialStream(STREAM_STATE, STREAM_STATE_STABILIZE); // state: stabilize

#ifdef ROUND_IMAGE_POS
        // update position
        m_activeXPos += adj_x;
        m_activeYPos += adj_y;

        // update position
        m_active->pxSetPosition(static_cast<float>(CMath::round(m_activeXPos)), static_cast<float>(CMath::round(m_activeYPos)));
#else
        // get current position
        m_active->pxGetPosition(image_x, image_y);

        // update position
        m_active->pxSetPosition(image_x + adj_x, image_y + adj_y);
#endif
    }

    // relative point from which to stabilize
    m_xDriftOffset = eye_x;
    m_yDriftOffset = eye_y;
}

void ExperimentBody::_stabilizeActiveObject(CEOSData* Samples) {
    // get stabilized position
    float eye_x, eye_y, delta_x, delta_y;

    // get eye position
    CStabilizer::Instance()->stabilize(Samples, eye_x, eye_y);

    // if scotoma
    if (TASK_SCOTOMA == m_task) {
        // adjust scotoma position
        m_scotoma->pxSetPosition(eye_x + m_xPos, eye_y + m_yPos);
    }

    // is making an eye movement
    if (_isNonDriftMovement(Samples)) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_EYEMOVE); // state: eye movement

        m_eyeMovement = true;
        return;
    }

    // delta (eye movement since last stabilization)
    delta_x = (eye_x - m_xDriftOffset);
    delta_y = (eye_y - m_yDriftOffset);

    // movements more than 2 arcmin should be viewed as eye movements
#ifdef PAUSE_STABILIZATION_FAST
    if (PAUSE_STABILIZATION_FAST < ((delta_x * delta_x) + (delta_y * delta_y))) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_TOOFAST); // state: eye moving too fast
		debug(CEnvironment::Instance()->outputMessage("IGNORE - Too Fast"));

        // view as start of eye movement (don't stabilize)
        m_eyeMovement = true;

		// update drift offset for measuring velocity
		m_xDriftOffset = eye_x;
		m_yDriftOffset = eye_y;

        return;
    }
#endif

    // edge detection
#ifdef PAUSE_STABILIZATION_EDGE
    if (fabs(eye_x + m_xShift) > m_pxWidth * 0.5 * (1 - PAUSE_STABILIZATION_EDGE) || fabs(eye_y + m_yShift) > m_pxHeight * 0.5 * (1 - PAUSE_STABILIZATION_EDGE)) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_EDGE); // state: eye near edge
		debug(CEnvironment::Instance()->outputMessage("IGNORE - Near Edge"));

        // view as start of eye movement (don't stabilize)
        m_eyeMovement = true;

        return;
    }
#endif

    // if was moving during last sample? don't do any stabilization
    if (m_eyeMovement) {
        storeTrialStream(STREAM_STATE, STREAM_STATE_STABILIZE); // state: stabilize

        // end of eye movement
        m_eyeMovement = false;
    }
    else {
        float adj_x, adj_y;
#ifndef ROUND_IMAGE_POS
        float image_x, image_y;
#endif

        // the adjustment is the eye_movement - gain * rotated/scaled modulation
        // adding the eye movement stabilizes things (so 0 gain eliminates all drift)

        // calculated adjustment
        adj_x = delta_x - m_driftGain * (delta_x * m_driftAngleCos - delta_y * m_driftAngleSin);
        adj_y = delta_y - m_driftGain * (delta_x * m_driftAngleSin + delta_y * m_driftAngleCos);

        // store stream data
        storeTrialStream(STREAM_X, adj_x);
        storeTrialStream(STREAM_Y, adj_y);
        storeTrialStream(STREAM_STATE, STREAM_STATE_STABILIZE); // state: stabilize

#ifdef ROUND_IMAGE_POS
        // update position
        m_activeXPos += adj_x;
        m_activeYPos += adj_y;

        // update position
        m_active->pxSetPosition(static_cast<float>(CMath::round(m_activeXPos)), static_cast<float>(CMath::round(m_activeYPos)));
#else
        // get current position
        m_active->pxGetPosition(image_x, image_y);

        // update position
        m_active->pxSetPosition(image_x + adj_x, image_y + adj_y);
#endif
    }

    // relative point from which to stabilize
    m_xDriftOffset = eye_x;
    m_yDriftOffset = eye_y;
}

std::string ExperimentBody::_getTaskDescription() {
    switch (m_task) {
        case TASK_FREE_VIEW:
            return "Look at the following photos.";
        case TASK_SEARCH:
        case TASK_SCOTOMA:
            return "Search the photo. Press the controller button when you see target.";
        case TASK_STIMULI:
            return "Watch the photos that appear. Press the controller button when you see an animal.";
        case TASK_DETECT_MOTION:
            return "For each image, look for motion. Press left trigger for no motion. Press right trigger for yes motion.";
        case TASK_DETECT_CHANGE:
            return "Watch the image. Press right trigger when it changes.";
        default:
            return "Unknown task.";
    }
}

void ExperimentBody::_configureFromNextDriftParam() {
    // get current parameter
    std::string param = m_driftParams[m_driftParamIndex], cur, key, val;
    double d_gain = 1., d_angle = 0.;
    std::string::size_type pos;
    vector<size_t> images;
    int i = 0;

    // increment index
    if (++m_driftParamIndex >= m_driftParams.size()) {
        m_driftParamIndex = 0;
    }

    // parse line
    while (!param.empty()) {
        // split
        pos = param.find(";");
        if (std::string::npos != pos) {
            cur = param.substr(0, pos);
            param.erase(0, pos + 1);
        }
        else {
            cur = param;
            param.clear();
        }

        // find equal
        pos = cur.find("=");
        if (std::string::npos != pos) {
            // get ke
            key = cur.substr(0, pos);
            CUtilities::trimString(key);

            // get value
            val = cur.substr(pos + 1);
        }
        else {
            val = cur;
            switch (i) {
                case 0:
                    key = "gain";
                    break;
                case 1:
                    key = "angle";
                    break;
                case 2:
                    key = "images";
                    break;
                default:
                    key = "unknown";
            }
        }
        CUtilities::trimString(val);

        // assign
        if (0 == key.compare("gain")) {
            d_gain = std::atof(val.c_str());
        }
        else if (0 == key.compare("angle")) {
            d_angle = std::atof(val.c_str());
        }
        else if (0 == key.compare("images")) {
            std::stringstream ss(val);
            std::string item;
            while (std::getline(ss, item, ',')) {
                CUtilities::trimString(item);
                images.push_back((size_t)std::atoi(item.c_str()));
            }
        }
        else {
            CEnvironment::Instance()->outputMessage("Unknown gain parameter %s in %s.", key.c_str(), param.c_str());
        }

        ++i;
    }

    // debug
    CEnvironment::Instance()->outputMessage("Updating drift gain x%f, with angle %f degrees.", d_gain, d_angle);

    // has images? add them to gain specific list
    m_gainSpecificImages.clear();
    if (!images.empty()) {
        size_t cur_image;

        // shuffle the list of images
        std::random_shuffle(images.begin(), images.end(), random_int);

        // debug
        CEnvironment::Instance()->outputMessage("Found %d images for the specific gain.", images.size());

        // copy validated list of images over
        while (!images.empty()) {
            // get last image
            cur_image = images.back();
            images.pop_back();

            // make sure it is valid
            if (cur_image >= m_imageNames.size()) {
                CEnvironment::Instance()->outputMessage("[ERROR] Found invalid image index: %d.", cur_image);
            }
            else {
				// debug name
				debug(CEnvironment::Instance()->outputMessage("Gain specific image: %s", m_imageNames[cur_image].c_str()));

                m_gainSpecificImages.push_back(cur_image);
            }
        }
    }

    // convert degrees to radians
    if (fabs(d_angle) > 10) {
        d_angle = d_angle * M_PI / 180.;
    }

    // update values
    m_driftGain = (float)d_gain;
    setDriftAngle((float)d_angle);
}

void ExperimentBody::_setupState(STATE newState) {
    // set up new state
    switch (newState) {
        case STATE_LOADING:
            // _setupState is not called for loading
            break;

        case STATE_TESTCALIBRATION:
            // reset position
            m_xPos = 0.f;
            m_yPos = 0.f;

            // start timer (ensures ample time to complete calibration)
            m_timer.start(500);

			// start trial
			startTrial();

            break;

        case STATE_CUE:
            // sufficient trials?
            if (m_paramsFile->getInteger(CFG_NUMBER_OF_TRIALS) <= m_trialNumber) {
                _setupState(STATE_FINISH);
                return;
            }

            // recalibrate?
            if (0 == m_trialsUntilCalibration) {
                // switch to test calibration
                _setupState(STATE_TESTCALIBRATION);
                return;
            }

            // assign new drift parameters here to allow loading drift-specific images
            // has drift parameters?
            if (!m_driftParams.empty()) {
                _configureFromNextDriftParam();
            }

            // start 2 second timer during which cue is shown
            m_timer.start(2000);

            break;

        case STATE_TASK:
            // start with blank list of images
            m_trialImages.clear();

            // start assuming the eye is moving to reset the drift offsets
            m_eyeMovement = true;

            // set number of remaining images
            if (TASK_STIMULI == m_task) {
                m_remainingImages = m_paramsFile->getInteger(CFG_STIMULI_PER_SESSION);
            }
            else {
                m_remainingImages = m_paramsFile->getInteger(CFG_IMAGE_PER_SESSION);
            }

            // if motion detection...
            if (TASK_DETECT_MOTION == m_task) {
                // SHOW MASK

                // move to front
                moveToFront(m_mask);

                // show mask
                m_mask->show();

                // PREPARE JOSTLE LIST
                if (m_driftGain > 10) {
                    // make file name
                    std::string jostle_file = "data/" + m_subjectName + "/jostle.eis";
                    struct stat buffer;

                    // jostle file already exist?
                    if (stat(jostle_file.c_str(), &buffer) == 0) {
                        // load jostle file
                        _loadJostleData(jostle_file);
                    }
                    else {
                        CEnvironment::Instance()->outputMessage("No jostle source ready, preparing one now.");

                        // override gain and angle
                        m_driftGain = 2.f;
                        setDriftAngle(0.f);
                    }

                    // set jostle index
                    m_jostleIndex = 0;
                }
            }

            // if scotoma...
            if (TASK_SCOTOMA == m_task) {
                // SHOW SCOTOMA

                // move to front
                moveToFront(m_scotoma);

                // show scotoma
                m_scotoma->show();
            }

            // start trial
            startTrial();

            break;

        case STATE_CHANGE:
            // set time for transition phase
            m_timer.setDuration(TIME_CHANGE_TRANSITION);
            break;

        case STATE_FEEDBACK:
            if (m_feedback.empty()) {
                _setupState(STATE_CUE);
                return;
            }

            // start 1.5 second timer during which feedback is shown
            m_timer.start(1500);
            break;

        case STATE_FINISH:
            declareFinished();
            break;
    }

    // set new state
    m_state = newState;
}

void ExperimentBody::_switchToState(STATE newState) {
    // clean up old state
    STATE oldState = m_state;
    switch (oldState) {
        case STATE_LOADING:
            // no clean up needed
            break;

        case STATE_TESTCALIBRATION:
            // store offset
            m_xShift = m_xPos + m_xShift;
            m_yShift = m_yPos + m_yShift;

            // print debugging
            debug(CEnvironment::Instance()->outputMessage("State Test Calibration, xshift: %.2f", m_xShift));
            debug(CEnvironment::Instance()->outputMessage("State Test Calibration, yshift: %.2f", m_yShift));
            debug(CEnvironment::Instance()->outputMessage("----------------------------------------------------------------"));

            // hide crosses
            m_redCross->hide();
            m_whiteCross->hide();

            // end trial
            endTrial();

            // reset counter until calibration
            m_trialsUntilCalibration = m_paramsFile->getInteger(CFG_CALIBRATE_EVERY);

            break;

        case STATE_CUE:
            // no clean up needed
            break;

        case STATE_TASK:
            // changing from TASK -> CHANGE, leave experiment running as before; clean up during end of STATE_CHANGE
            if (STATE_CHANGE == newState) {
                break;
            }
        case STATE_CHANGE:
            // undo change
            if (STATE_CHANGE == oldState && m_active) {
                m_active->degSetAngle(0.f);
            }

            // run clean up when leaving CHANGE

            // end trial
            endTrial();

            // save trial data
            _saveTrialData();

            // hide mask
            if (TASK_DETECT_MOTION == m_task) {
                m_mask->hide();
            }

            // hide scotoma
            if (TASK_DETECT_MOTION == m_task) {
                m_scotoma->hide();
            }

            // free up memory
#ifndef ALLOCATE_ALL
            for(std::vector<CImagePlane *>::size_type i = 0; i != m_images.size(); i++) {
                if (m_images[i]->getTexture()) {
                    m_images[i]->removeTexture();
                }
            }
#endif

            // increment trial number
            ++m_trialNumber;

            // decrement trials until calibration
            --m_trialsUntilCalibration;

            break;

        case STATE_FEEDBACK:
            // clear feedback message
            m_feedback.clear();
            break;

        case STATE_FINISH:
            // no clean up needed (can't leave state)
            break;
    }

    // setup the new state
    _setupState(newState);
}

void ExperimentBody::_saveTrialData() {
    // store task
    std::string task_name;
    switch (m_task) {
        case TASK_SEARCH:
            task_name = "search";
            break;
        case TASK_SCOTOMA:
            task_name = "scotoma";
            break;
        case TASK_STIMULI:
            task_name = "stimuli";
            break;
        case TASK_FREE_VIEW:
            task_name = "free_view";
            break;
        case TASK_DETECT_MOTION:
            task_name = "detect_motion";
            break;
        case TASK_DETECT_CHANGE:
            task_name = "detect_change";
            break;
    }

    // store task and subject information
    storeTrialVariable("Task", task_name);
    storeTrialVariable("SubjectName", m_subjectName);

    // store configuration information
    storeTrialVariable("DriftGain", m_driftGain);
    storeTrialVariable("DriftAngle", m_driftAngle);

    // save display px x, y and refresh rate
    storeTrialVariable("HRes", m_pxWidth);
    storeTrialVariable("VRes", m_pxHeight);
    storeTrialVariable("RefreshRate", m_refreshRate);

    // image name
    storeTrialVariable("Trial", m_trialNumber);
    storeTrialVariable("ImageNames", m_trialImages);

    // store calibration information
    storeTrialVariable("XShift", m_xShift);
    storeTrialVariable("YShift", m_yShift);

    // stabilization info
    storeTrialVariable("FastStabilization", m_fastStabilization);

    // store information into a directory with the subject's name
    debug(CEnvironment::Instance()->outputMessage("Trial Saved"));
    saveTrial("data/" + m_subjectName);

    // store as jostle
    if (TASK_DETECT_MOTION == m_task && 0.01 > abs(m_driftGain - 2.f) && 0.01 > abs(m_driftAngle)) {
        // make file name
        std::string jostle_file = "data/" + m_subjectName + "/jostle.eis";
        struct stat buffer;

        // doesn't already exist?
        if (stat(jostle_file.c_str(), &buffer) != 0) {
            debug(CEnvironment::Instance()->outputMessage("Jostle Saved"));
            saveTrialAs(jostle_file);
        }
    }
}
