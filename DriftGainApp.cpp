// DriftGainApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DriftGainApp.h"
#include "emil-console/emil-console.h"
#include "ExperimentBody.h"

#include <windows.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CDriftGainApp
BEGIN_MESSAGE_MAP(CDriftGainApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

// CDriftGainApp construction
CDriftGainApp::CDriftGainApp()
{
	// Initialize the manager for the configuration parameters
    m_paramsFile.addVariable(CFG_SUBJECT_NAME, std::string(""));
    m_paramsFile.addVariable(CFG_TASK, std::string("FREEVIEW"));
    //m_paramsFile.addVariable(CFG_FOLDER, std::string(""));
    m_paramsFile.addVariable(CFG_NUMBER_OF_TRIALS, 5);
    m_paramsFile.addVariable(CFG_CALIBRATE_EVERY, 1);

    // Stimuli series task
    m_paramsFile.addVariable(CFG_STIMULI_PER_SESSION, 20);
    m_paramsFile.addVariable(CFG_STIMULI_TIME, 700.f);

    // Search or free view task
    m_paramsFile.addVariable(CFG_IMAGE_PER_SESSION, 20);
    m_paramsFile.addVariable(CFG_IMAGE_TIME, 5000.f);

    // Stabilization
    m_paramsFile.addVariable(CFG_FAST_STABILIZATION, true);

    m_paramsFile.addVariable(CFG_VRes, 600);
    m_paramsFile.addVariable(CFG_HRes, 800);
    m_paramsFile.addVariable(CFG_RRate, 200);
}

// The one and only CDriftGainApp object
CDriftGainApp theApp;

// CSimpleSampleApp initialization
BOOL CDriftGainApp::InitInstance()
{ 
	// InitCommonControls() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	InitCommonControls();
	CWinApp::InitInstance();
	AfxEnableControlContainer();

    // Load the specified configuration file
    try {
        m_paramsFile.loadFile("data/params.cfg");
    }
    catch (xCfgFile &x) {
        // Handle an exception which is handled by the CEnvironment class
        CEnvironment::Instance()->outputMessage(CEnvironment::ENV_MSG_ERROR, "ERROR LOADING CONFIGURATION: %s", x.what().c_str());

        return FALSE;
    }

	// Initialize the library
	// Insert this line if the library needs to load a specific configuration file
	CWinEMIL::Instance()->initialize("C:/EyeRIS/latest/system/config/emil-library.cfg");

	// run calibration
    // IMPORTANT: run calibration with lower resolution in order to have a better estimation of gaze position all over the display
    CWinEMIL::Instance()->addExperiment(new ExAutoCalibrator2(800, 600, 200));
	CWinEMIL::Instance()->addExperiment(new ExManualCalibrator2(800, 600, 200));

    // get screen configuration
    int HResolution = m_paramsFile.getInteger(CFG_HRes);
    int VResolution = m_paramsFile.getInteger(CFG_VRes);
    int RefreshRate = m_paramsFile.getInteger(CFG_RRate);

    // run actual experiment
	CWinEMIL::Instance()->addExperiment(new ExperimentBody(HResolution, VResolution, RefreshRate, &m_paramsFile));

    // show console
	CEMILConsole dlg("C:/EyeRIS/latest/system/config/emil-console.cfg");
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	CWinEMIL::Destroy();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
