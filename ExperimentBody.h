#pragma once

#include "emil/emil.h"

class ExperimentBody: public CExperiment
{
public:
	/// Default constructor
	/** @param pxWidth Width of the monitor in pixels
		@param pxHeight Height of the monitor in pixels
		@param RefreshRate Refresh rate of the monitor, in Hz
        @param Params The configuration file with parameters */
	ExperimentBody(int pxWidth, int pxHeight, int RefreshRate, CCfgFile* Params);

	/// Standard event handlers
	virtual void initialize();
	virtual void finalize();
	virtual void eventRender(unsigned int FrameCount, CEOSData* Samples);
	virtual void eventJoypad();

    void setDriftAngle(float angle);

private:
    // possible tasks (does not change)
    enum TASK {
        TASK_FREE_VIEW, // free viewing
        TASK_SEARCH, // unused
        TASK_SCOTOMA, // search with ocular scotoma
        TASK_STIMULI, // unused
        TASK_DETECT_MOTION, // detect motion in stimulus
        TASK_DETECT_CHANGE // detect change in stimulus
    } m_task;

    // current experiment state
    enum STATE {
        STATE_LOADING,
        STATE_TESTCALIBRATION,
        STATE_CUE,
        STATE_TASK,
        STATE_CHANGE, // just for detect change trial
        STATE_FEEDBACK, // feedback on interactive trials; shown for 1.5 seconds
        STATE_FINISH
    } m_state;

    // change states
    void _shuffleImageIndices();
    bool _advanceImage();
    void _loadImages();
    void _loadJostleData(const std::string &file_name);
    void _loadDriftParams();
    void _configureFromNextDriftParam();
    void _setupState(STATE newState);
    void _switchToState(STATE newState);
    bool _isNonDriftMovement(CEOSData* Samples);
    void _stabilizeActiveObject(CEOSData* Samples);
    void _jostleActiveObject(CEOSData* Samples);
    void _saveTrialData();
    std::string _getTaskDescription();

    // Configuration file
    CCfgFile* m_paramsFile;

    // Experiment details
    std::string m_subjectName;
    std::string m_trialImages;

    // Feedback
    std::string m_feedback;

    // fast stabilization
    bool m_fastStabilization;

    // Stimuli and variables for the test calibration
    CImagePlane* m_redCross;
    CImagePlane* m_whiteCross;
    float m_xShift; // x offset based on test calibration
    float m_yShift; // y offset based on test calibration
    float m_xPos; // x offset of cross
    float m_yPos; // y offset of cross
    int m_trialsUntilCalibration;

	// Experiment variables
    std::vector<CImagePlane*> m_images;
    std::vector<std::string> m_imageNames;
	std::vector<size_t> m_imageIndices;
    std::vector<size_t> m_gainSpecificImages;
    size_t m_imageIndex;

    // Stabilization object
    CImagePlane* m_active; // the item actively getting moved
    float m_activeXPos;
    float m_activeYPos;

    // Scotoma
    CImagePlane* m_scotoma;

    // Mask (motion detection)
    CImagePlane* m_mask;
    std::vector<float> m_xJostle;
    std::vector<float> m_yJostle;
    size_t m_jostleIndex;

    // Drift / gain "recipe" to allow specifying conditions to try (loaded at beginning, shuffled, then used sequentially)
    std::vector<std::string> m_driftParams;
    size_t m_driftParamIndex;

    // State information
    CTimer m_timer; // timer until next image or stimuli is shown
    int m_remainingImages; // number of images remaining in current session
    int m_trialNumber;
    bool m_eyeMovement; // is mid eye movement?
    float m_xDriftOffset; // the offset from which drift is measured
    float m_yDriftOffset;
    float m_driftGain;
    float m_driftAngle; // angle in radians

    // Cache drift angle matrix values
    float m_driftAngleCos;
    float m_driftAngleSin;
};