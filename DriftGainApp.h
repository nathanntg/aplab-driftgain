// SimpleSample.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include "emil/emil.h"

// names of entries in the configuration file
const std::string CFG_SUBJECT_NAME = "Subject";
const std::string CFG_TASK = "Task"; // can be STIMULI, FREEVIEW or SEARCH
//const std::string CFG_FOLDER = "Folder";
const std::string CFG_NUMBER_OF_TRIALS = "NumberOfTrials";
const std::string CFG_CALIBRATE_EVERY = "CalibrateEvery";

// Task: stimuli series
const std::string CFG_STIMULI_PER_SESSION = "StimuliPerSession";
const std::string CFG_STIMULI_TIME = "StimuliTime";

// Task: search or free view
const std::string CFG_IMAGE_PER_SESSION = "ImagesPerSession";
const std::string CFG_IMAGE_TIME = "ImageTime";

// Stabilization
const std::string CFG_FAST_STABILIZATION = "FastStabilization";

// Screen configuration
const std::string CFG_VRes = "VRes";
const std::string CFG_HRes = "HRes";
const std::string CFG_RRate = "RRate";

class CDriftGainApp : public CWinApp
{
public:
    CDriftGainApp();

    // Overrides
	virtual BOOL InitInstance();

    // Implementation
	DECLARE_MESSAGE_MAP()

    // Configuration file
    CCfgFile m_paramsFile;
};

extern CDriftGainApp theApp;