function [x, y] = trace(Data, idx, target_dur)

% build result
eye_x = Data.x{idx};
eye_y = Data.y{idx};
Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% state changes
states = split_states(Result.samples, Data.stream02{idx}, Result.analysis);

% get states
d = sortrows([abs(states.stabilize.duration - target_dur)', (1:numel(states.stabilize.duration))'], 1);

% drift number
n = d(1, 1);

% timing
strt = states.stabilize.start(n);
dur = states.stabilize.duration(n);

x = double(eye_x(strt:(strt + dur - 1)) - eye_x(strt));
y = double(eye_y(strt:(strt + dur - 1)) - eye_y(strt));

x = sgolayfilt(x, 3, 41);
y = sgolayfilt(y, 3, 41);

end

