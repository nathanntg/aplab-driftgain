function ret = cb_rel_speed(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, tm, CS, par)

% minimum duration
if dur < 100
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 8
	par = 41;
end
if nargin < 7
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

% time blocks
if nargin < 6
    tm = 20; % ms
end

%% RELATIVE SPEED

rel_x = cur_eye_x - cur_adj_x;
rel_y = cur_eye_y - cur_adj_y;

% apply filter, if specified
if ~isempty(par)
    rel_x = sgolayfilt(rel_x, 3, par);
    rel_y = sgolayfilt(rel_y, 3, par);
    
    % trim noise
    rel_x = rel_x(CS:end + 1 - CS);
    rel_y = rel_y(CS:end + 1 - CS);
end

% speed vector
diff_x = rel_x(1 + tm:end) - rel_x(1:end - tm);
diff_y = rel_y(1 + tm:end) - rel_y(1:end - tm);

spd = sqrt(diff_x .^ 2 + diff_y .^ 2) * 1000 / tm;

% calculate velocity
ret = mean(spd);

end
