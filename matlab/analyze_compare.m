% find files
files = find_files('../Analysis/drifts');

% groups
x = [];
groups = {};

% min duration
min_duration = 100;

% use drifts during stabilization
stab = true;

metric = 'Velocity';

% iterate
for file = transpose(files)
    %x = file{1};
    if regexp(file{1}, 'mat$')
        if stab
            if regexp(file{1}, '/stab-')
                
            else
                continue;
            end
        else
            if regexp(file{1}, '/stab-')
                continue;
            end
        end
        
        % load it
        load(file{1});
        
        if 0 ~= angle
            %continue
        end
        
        strt = [drifts.start];
        dur = [drifts.duration];
        
        % fprintf('%.1f\t%d\n', sum(dur > 100));
        
        % get values
        if strcmp('Velocity', metric)
            vel = [drifts.velocity];
            dur(isnan(vel)) = -1;
            values = vel(dur >= min_duration);
        elseif strcmp('Speed', metric)
            values = amp(dur >= min_duration) ./ dur(dur >= min_duration);
        elseif strcmp('Duration', metric)
            values = dur(dur >= min_duration);
        elseif strcmp('Amplitude', metric)
            amp = [drifts.amplitude];
            values = amp(dur >= min_duration);
        elseif strcmp('Curvature', metric)
            curve = [drifts.curvature];
            values = curve(dur >= min_duration);
        elseif strcmp('Curvature2', metric)
            curve = [drifts.curvature2];
            values = curve(dur >= min_duration);
        elseif strcmp('Length', metric)
            len = [drifts.length];
            values = len(dur >= min_duration);
        else
            error('Invalid metric.');
        end
        
        % file name
        file_prefix = sprintf('../Analysis/%s-gain%.1f-angle%.2f', metric, gain, angle);
        
        % stats
        fprintf('Gain: %.1f\nAngle: %.2f\nMean %s: %.3f\nStandard Deviation: %.3f\n\n', gain, angle, metric, mean(values), std(values));
        
        if gain ~= 1 || angle ~= 0
            if exist('v10','var')
                [h, p] = ttest2(v10, values);
                if h
                    fprintf('REJECT NULL %.5f\n\n', p);
                end
            end
        else
            new_v10 = values;
        end
        
        % histogram
        hist(values);
        title(sprintf('%s histogram; Gain: %.1f; Angle: %.2f', metric, gain, angle));
        %pause;
        %print(gcf, [file_prefix '-hist.png'], '-dpng', '-r300');
        
        % scatter plot
        scatter(strt(dur >= min_duration), values);
        title(sprintf('%s over time; Gain: %.1f; Angle: %.2f', metric, gain, angle));
        %lsline;
        %pause;
        %print(gcf, [file_prefix '-time.png'], '-dpng', '-r300');
        
        x = [x; values'];
        
        % make groups
        group = cellstr(sprintf('Gain: %.1f', gain));
        groups = [groups; repmat(group, [size(values, 2) 1])];
    end
end

if exist('new_v10', 'var')
    v10 = new_v10;
end

boxplot(x, groups);
delete(findobj(gca,'tag','Outliers'));
title(sprintf('%s across trials', metric));
print(gcf, ['../Analysis/' metric '-box.png'], '-dpng', '-r300');