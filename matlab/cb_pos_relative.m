function ret = cb_pos_relative(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur)
ret = [reshape(cur_eye_x - cur_eye_x(1), dur, 1), reshape(cur_eye_y - cur_eye_y(1), dur, 1)];
end

