function drifts_with_stab_events = extract_stabilized_drift_events(Result, stream)
%EXTRACT_STABILIZED_DRIFT_EVENTS Returns cleaned up drift event list.
%   Uses the stabilization status stream (stream) to extract drift segments
%   during which the image on the screen is being stabilized (excludes
%   edges, fast movements and other eye movement data).


% no stabilization was running
no_stab_val = stream.data;
no_stab_tm = stream.ts;

% full time
no_stab = zeros(size(Result.x.position));
for t = 1:max(size(no_stab_tm))
	if no_stab_val(t) == 0
		continue;
	end
	
	from = no_stab_tm(t);
    if t >= size(no_stab_tm, 2)
        to = max(size(Result.x.position));
    else
        to = no_stab_tm(t + 1) - 1;
    end
	no_stab(from:to) = 1;
end

% convert to events
no_stab_events = vector2events(no_stab);

% minimum duration
no_stab_events = filterBy('duration', no_stab_events, 10);

% make drifts
drifts_with_stab_events = sdiffEvents(1, Result.drifts, no_stab_events);

% add back amplitude and angle
drifts_with_stab_events = updateAmplitudeAngle(Result, drifts_with_stab_events);

end

