function [arcmin_x, arcmin_y] = p2a( px_x, px_y )
%P2A Summary of this function goes here
%   Detailed explanation goes here

% arcmin to pix
monitor_distance = 1200; %mm
monitor_width = 390; %mm
monitor_height = 290; %mm
resolution_width = 800; %px
resolution_height = 600; %px

arcmin_x = rad2deg(atan(px_x * monitor_width / monitor_distance / resolution_width)) * 60;
arcmin_y = rad2deg(atan(px_y * monitor_height / monitor_distance / resolution_height)) * 60;

end

