function ret = cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, tm, CS, min_dur, par)

% default min duration
if nargin < 8
    min_dur = 100;
end

% minimum duration
if dur < min_dur
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 9
	par = 41;
end
if nargin < 7
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

% time blocks
if nargin < 6
    tm = 20; % ms
end

% EYE SPEED
% apply filter, if specified
if ~isempty(par)
    cur_eye_x = sgolayfilt(cur_eye_x, 3, par);
    cur_eye_y = sgolayfilt(cur_eye_y, 3, par);
end

% calculate differences
x1 = diff(cur_eye_x(CS:end+1-CS));
y1 = diff(cur_eye_y(CS:end+1-CS)); 

% speed vector
spd = sqrt(x1 .^ 2 + y1 .^ 2) * 1000;

% windowed average
max_t = numel(spd);
max_j = ceil(max_t / tm);
ret = zeros(max_j, 2);
for j = 1:max_j
    t = (j - 1) * tm + 1;
    ret(j, :) = [t mean(spd(t:min(j * tm, max_t)))];
end

end

