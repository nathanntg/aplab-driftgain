function ret = cb_speed(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, CS, par)

% minimum duration
if dur < 100
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 7
	par = 41;
end
if nargin < 6
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

%% EYE SPEED
% apply filter, if specified
if ~isempty(par)
    cur_eye_x = sgolayfilt(cur_eye_x, 3, par);
    cur_eye_y = sgolayfilt(cur_eye_y, 3, par);
end

% calculate differences
x1 = diff(cur_eye_x(CS:end-CS));
y1 = diff(cur_eye_y(CS:end-CS)); 

% calculate velocity
eye_speed = mean(sqrt(x1.^ 2 + y1.^ 2) * 1000);

%% IMAGE SPEED
% [c_x, c_y] = p2a(1, 1);
% cur_adj_x = cur_adj_x * c_x;
% cur_adj_x = cur_adj_x * c_y;

% apply filter, if specified
if ~isempty(par)
    cur_adj_x = sgolayfilt(cur_adj_x, 3, par);
    cur_adj_y = sgolayfilt(cur_adj_y, 3, par);
end

% calculate differences
x1 = diff(cur_adj_x(CS:end-CS));
y1 = diff(cur_adj_y(CS:end-CS)); 

% calculate velocity
img_speed = mean(sqrt(x1.^ 2 + y1.^ 2) * 1000);

ret = [eye_speed, img_speed];

end

