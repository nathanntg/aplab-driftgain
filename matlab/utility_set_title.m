function utility_set_title(description, fast, subject)

% Optional: fast
% true: fast only; false: slow only; nan: all
if nargin < 2
    fast = [];
end

% fast: subject
if ~isnan(fast)
    if fast
        description = [description '; Fast stabilization'];
    else
        description = [description '; Fast stabilization'];
    end
end

% Optional: subject
% '' or string
if nargin >= 3 && ~isempty(subject)
    subjects = {'Mia' 'Dylan'};
    for j = 1:numel(subjects)
        if strcmp(subjects{j}, subject)
            description = [description '; Subject ' int2str(j)];
            break;
        end
    end
end

title(description);

end

