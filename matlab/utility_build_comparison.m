function [group, results, count] = utility_build_comparison(cb, fast, subject, convert_p2a, cb_analyze)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% directories
dirs = {'7 Mia - Fast' '8 Mia - Slow' '9 Dylan - Fast' '10 Dylan - Slow' '12 Patrick - Fast'};

% Optional: fast
% true: fast only; false: slow only; nan: all
if nargin < 2
    fast = nan;
end

% Optional: subject
% '' or string
if nargin < 3
    subject = '';
end

% Optional: convert p2a
if nargin < 4
    convert_p2a = false;
end

% Optional: analyze callback (allow for saccadic analysis)
if nargin < 5
    cb_analyze = @utility_analyze;
end

% build
group = [];
results = [];
count = [];
for d = dirs
    if ~isempty(subject) && isempty(strfind(d{1}, subject))
        continue;
    end
    
    load(fullfile('..', 'Raw', d{1}, 'processed.mat'));
    for idx = 1:numel(Data.x)
        gain = Data.user{idx}.DriftGain;
        
        % fast
        if ~isnan(fast)
            if fast && ~Data.user{idx}.FastStabilization
                continue;
            end
            if ~fast && Data.user{idx}.FastStabilization
                continue;
            end
        end

        % calculate
        [res, n] = cb_analyze(Data, idx, cb, convert_p2a);
        
        % append
        results = [results; res];
        group = [group; gain * ones(size(res, 1), 1)];
        count = [count; gain n];
    end
end

end

