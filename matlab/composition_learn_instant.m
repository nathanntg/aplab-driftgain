set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Mia';

% OUTPUT name
nm = 'learn-instant';

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true, @cb_analyze_state2);

%% OUTPUT
% gains
gains = sort(unique(group));

% all at once
for g = gains'
    speed = all_res(group == g, :);
    
    % rows from first 5s
    rows = speed(:, 1) < 5000;
    
    % make scatter plot
    scatter(speed(rows, 1), speed(rows, 3), 50, 'filled');
    xlabel('Time in trial (ms)');
    ylabel('Speed at start of drift');
    xlim([0 5000]);
    title(sprintf('Initial speed over time (adaptation); gain %.1f', g));
    
    % draw line
    d = dataset();
    d.time = speed(rows, 1);
    d.rel = speed(rows, 2);
    d.speed = speed(rows, 3);
    mdl = LinearModel.fit(d, 'speed ~ time + rel');
    %mdl = fitglm(d, 'speed ~ time + rel', 'Distribution', 'gamma');
    
    % plot it
    x = linspace(0, 5000);
    hold on;
    plot(x', mdl.predict([x' zeros(size(x'))]), 'r--');
    hold off;
    
    legend('Observations', sprintf('Linear model (p = %.3f)',  mdl.Coefficients.pValue(2)), 'Location', 'SouthEast');
    
    % skip it
    %if mdl.Coefficients.pValue(2) > 0.1
    %    continue
    %end
    
    fprintf('Gain: %.1f; Slope: %f\n', g, mdl.Coefficients.Estimate(2));
    
    % print
    print(gcf, utility_build_name(nm, sprintf('gain%d', round(g * 10)), fast, subject), '-dpng', '-r300');
end
