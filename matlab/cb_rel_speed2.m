function ret = cb_rel_speed2(cur_t, cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur)

% minimum duration
if dur < 100
    ret = nan(1, 2);
    return;
end

%% RELATIVE SPEED

% REL SPEED
rel_x = diff(cur_eye_x) - diff(cur_adj_x);
rel_y = diff(cur_eye_y) - diff(cur_adj_y);

% calculate velocity
ret = mean(sqrt(rel_x .^ 2 + rel_y .^ 2) ./ diff(cur_t) * 1000);

end

