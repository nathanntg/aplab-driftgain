function [Curvature1 Curvature2] = CalculateCurvature(hor, ver, par)

% par is the smoothing factor for the sgolay low pass filter
% curvature = 1 segment is straight curvature = 0 full curvature

% default value
if nargin < 3
    par = 41;
end

% minimum length
if numel(hor) <= (par + 1)
    Curvature1 = nan;
    Curvature2 = nan;
    return;
end

% magnitude
Amplitude = sqrt((hor(1)-hor(end))^2+(ver(1)-ver(end))^2);
% length
Length1 = CalculateLengthTrace(hor,ver,1.09, par);
Length2  = DouglasPeucker([hor; ver], 0.0001, par, 0);
% curvature
c = (Amplitude/Length1);
if c>1
    c = 1;
end
if ~isnan(c)
    Curvature1 = 1-c;
end

c = (Amplitude/Length2);
if c>1
    c = 1;
end
if ~isnan(c)
    Curvature2 = 1-c;
end

if Curvature2<0.01
    Curvature2 = nan;
end
if Curvature1<0.01
    Curvature1 = nan;
end