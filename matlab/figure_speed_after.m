%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% AFTER ms
after = 200;

% OUTPUT name
nm = ['speed-after-' int2str(after)];

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_speed_instant, fast, subject, true);

%% OUTPUT
all_speed = all_res(all_res(:, 1) >= after, 2);
mod_group = group(all_res(:, 1) >= after);

[sig_set, p_value] = utility_signifigance(mod_group, all_speed, true);

utility_plot_sem(mod_group, all_speed, utility_sig_idx_to_val(mod_group, sig_set), p_value, 'Mean speed (arcmin/sec)');
ylim([0 200]);
utility_set_title('Instantaneous drift speed across trials', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(mod_group, all_speed, sig_set, p_value, 'Drift speed (arcmin/sec)');
ylim([0 375]);
utility_set_title('Instantaneous drift speed across trials', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');
