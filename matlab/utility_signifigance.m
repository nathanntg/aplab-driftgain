function [sig_set, p_value] = utility_signifigance(group, results, by_index, cb_test, threshold)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

% by index or by value? index useful for boxplots
if nargin < 3
    by_index = false;
end

% signifigance test
if nargin < 4
    cb_test = @(x, y) nth_output(2, @ttest2, x, y);
end

% default threshold
if nargin < 5
    threshold = 0.05;
end

sig_set = {};
p_value = [];
gains = sort(unique(group));
indices = (numel(gains) - 1):-1:1;
for g_idx = indices
    % get adjacent values
    h_idx = g_idx + 1;
    g = gains(g_idx);
    h = gains(h_idx);
    
    % run test
    p = cb_test(results(group == g), results(group == h));

    % compare with threshold
    if p <= threshold
        % append
        if by_index
            sig_set{end + 1} = [g_idx, h_idx];
        else
            sig_set{end + 1} = [g, h];
        end
        p_value(end + 1) = p;
    end
end

end

