function ret = cb_brownian(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur )

% don't filter
%cur_eye_x = sgolayfilt(cur_eye_x, 3, 41);
%cur_eye_y = sgolayfilt(cur_eye_y, 3, 41);

[d1, d2, d3, bias] = estimateDiffCoefficients(cur_eye_x, cur_eye_y, 1000);

% calculate norm of bias
mt = numel(bias.biasMeanX) + 1;
bias_norm = mean(sqrt(((bias.biasMeanX ./ (2:mt) * 1000) .^ 2) + ((bias.biasMeanY ./ (2:mt) * 1000) .^ 2)));

ret = [d1, d2, d3, bias_norm];

end

