% This function calculates the are covered by the 2D displacement distribution
% at each delta t.
%
% function [Area] = DiffusionCoefficient2D (Fix,varargin)
%
% INPUT:
% Fix   : matrix with the eye movements data. This matrix should be
%         organized in the the following way:
%         Fix{sbj} is an array containing each fixation as a structure.
%         A given drift's horizontal and vertical angles (in arcmin)
%         can be accessed via Fix{sbj}(fx).x and Fix{sbj}(fx).x. where fx
%         is the fixation index
%
% Properties: Optional properties
%             'MaxTime'       : Maximum temporal interval
%                               (default: 256 ms)
%             'SpanLimit'     : Maximum span of an event to be considered
%                               events with a higher span will be discarded
%                               (default: 60 arcmin)
%             'FigKey'        : Set FigKey at one to generate a figure
%                               (default: 0)
%             'TimeInterval'  : Temporal segment of the event considered
%                               if it is set at 1 all the event segment is selected
%                               otherwise a 2x1 vector will indicate the temporal
%                               start and end of the segment
%                              (default: [1])
%             'GenerateMovie' : Set GenerateMovie at 1 to generate a movie of
%                               the evolution of the event displacement at
%                               different delta t
%                               (default: 0)
%             'MovieName'     : string used to name the title of the movie
%                               (default: 'test')
%             'FigName'       : string used to save the figures
%                               (default: 'test')
%
% OUTPUT:
% Area           : Area (armin^2) covered by the displacements distribution at each delta t.
% Dsq            : Displacement squared (arcmin) at each delta t
% Time           : The temporal bins over which the area has been calculated.
% NFix           : Number of events considered for each delta t.
% RegCoef        : Regression coefficient for the fit of the area vs. time.
% theta          : degrees of rotations of the major axis of the ellips for the
%                 final value of delta t
% ax             : major and minor axis at the final value of delta t (the ratio
%                 between the two axis gives a measure of the distribution symmetry.
% xyCorrelation  :
%
% HISTORY:
% This function is based on the DiffusionCoefficients_DPI.m function by
% Murat, which is based on Xutao diffusion analysis function
%
% 2013 @APLAB

function [DCoef_Area, DCoef_Dsq, DCoef_AreaEig, Area, AreaEig, Dsq, SingleSegmentD, SingleSegmentDsq, Time, NFix, angle, axisRatio, xyCorrelation, Mean, std_th] = Calculate2DArea(Fix, varargin)

% set default parameters
NT = 256;
FIGKEY = 0;
TIMEINTERVAL = 1; %ms
FSAMPLING = 1000;
LIMIT = 15; % arcmin (used for the movie boundaries)
GENERATEMOVIE = 0;
MOVIENAME = 'test';
FIGNAME = 'test';
DIMENSIONS = 2;

% Interpret the user parameters
k = 1;
Properties = varargin;
while k <= length(Properties) && ischar(Properties{k})
    switch (Properties{k})
        case 'MaxTime'
            NT = Properties{k + 1};
            k = k +2;
        case 'FigKey'
            FIGKEY = Properties{k + 1};
            k = k +2;
        case 'TimeInterval'
            TIMEINTERVAL = Properties{k + 1};
            k = k +2;
        case 'GenerateMovie'
            GENERATEMOVIE = Properties{k + 1};
            k = k +2;
        case 'MovieName'
            MOVIENAME = Properties{k + 1};
            k = k +2;
        case 'FigName'
            FIGNAME = Properties{k + 1};
            k = k +2;
        otherwise
            warning('Unrecognized property %s', Properties{k});
            k = k + 1;
    end
end

if 2 == length(TIMEINTERVAL)
    if TIMEINTERVAL(2) < NT
        warning('Maximum Time %.0f ms is larger than the Time Interval considered [%.0f-%.0f]',...
            NT, TIMEINTERVAL(1),TIMEINTERVAL(2));
        NT = TIMEINTERVAL(2)-TIMEINTERVAL(1)-1;
        warning('Maximum time has been reset at %.0f' , NT);
    end
end

Time = linspace(1,NT-1,NT-1)./FSAMPLING;


[Area, AreaEig, Dsq, SingleSegmentDsq, NFix, angle, axisRatio, xyCorrelation, Mean, std_th] = probEyeMov (Fix, Time);

% regress the area and find the regression coefficients
max_pp = size(SingleSegmentDsq, 1);
SingleSegmentD = zeros(max_pp, 1);
for pp = 1:max_pp
    RegCoef = regress(SingleSegmentDsq(pp,:)', Time');
    SingleSegmentD(pp) = (RegCoef(1)/(2*DIMENSIONS)); 
end
RegCoefArea = regress(Area, Time');
RegCoefAreaEig = regress(AreaEig, Time');
RegCoefDsq = regress(Dsq, Time');

% rate of increase per sec
DCoef_Area = (RegCoefArea(1)/(2*DIMENSIONS));
DCoef_Dsq = (RegCoefDsq(1)/(2*DIMENSIONS));
DCoef_AreaEig = (RegCoefAreaEig(1)/(2*DIMENSIONS));

if FIGKEY == 1
    figure
    plot(Time, Area, 'b', 'LineWidth', 2)
    hold on
    plot(Time, Time*RegCoef(1)+RegCoef(2), '--b')
    set(gca, 'FontSize', 10)
    xlabel('Time (ms)')
    ylabel('Area (arcmin^2)')
    text(10,25, sprintf('Regression coeff: %.3f (arcmin^2/s)', RegCoef(1)), 'FontSize', 10);
    ylim([0  30]);
end

function  [Area, AreaEig, Dsq, SingleSegmentDsq, NFix, angle, axisRatio, xyCorrelation, Mean, std_th] = probEyeMov(fixs, time)
    max_dt = numel(time);

    Mean = zeros(max_dt, 2);
    SingleSegmentDsq = zeros(length(fixs), max_dt);
    xyCorrelation = zeros(max_dt, 1);
    Area = zeros(max_dt, 1);
    AreaEig = zeros(max_dt, 1);
    Dsq = zeros(max_dt, 1);
    axisRatio = zeros(max_dt, 1);
    angle = zeros(max_dt, 1);
    
    NFix = zeros(1,length(time));

    % create figure for movie frames
    if GENERATEMOVIE
        figure
        
        aviobj = VideoWriter(sprintf('%s.avi',MOVIENAME ), 'Uncompressed AVI');%, 'compression', 'none', 'fps', 30, 'quality', 100);
        aviobj.FrameRate = 25;
        open(aviobj);
    end
    
    % loop through all delta t
    for dt = 1:max_dt
        d_x= [];
        d_y = [];
        dd2 = [];
        % loop through all events
        for fx = 1:length(fixs)

            if length(TIMEINTERVAL)==2
                hor = fixs(fx).x(TIMEINTERVAL(1):TIMEINTERVAL(2));
                ver = fixs(fx).y(TIMEINTERVAL(1):TIMEINTERVAL(2));
            else
                hor = fixs(fx).x(TIMEINTERVAL(1):end);
                ver = fixs(fx).y(TIMEINTERVAL(1):end);
            end

            % pick all the possible couples of values based on nT and the
            % maximum time interval
            enD = length(hor)-dt;
            % keep count of the total number of fixations selected per dt
            if enD>0
                NFix(dt) = NFix(dt)+1;
            end
            d_x = [ d_x (hor(1+dt:enD+dt)-hor(1:enD)) ];
            d_y = [ d_y (ver(1+dt:enD+dt)-ver(1:enD)) ];
            dd2 = [ dd2 (hor(1+dt:enD+dt)-hor(1:enD)).^2 + (ver(1+dt:enD+dt)-ver(1:enD)).^2 ];
            SingleSegmentDsq(fx, dt) = mean((hor(1+dt:enD+dt)-hor(1:enD)).^2 + (ver(1+dt:enD+dt)-ver(1:enD)).^2);
        end

        % calculate the area of the 2D distribution
        c = corrcoef(d_x',d_y');
        % area at .68 without multiplication by pi (to make it comparable with
        % the d)
        Area(dt) = 2*std(d_x)*std(d_y)*sqrt(1-c(2,1)^2);
        Dsq(dt) = mean(dd2);%mean(d_x.^2+d_y.^2);
        % calculate the area based on the PCA
        mat = [d_x - mean(d_x); ...
              d_y - mean(d_y)];    
        sigmas = eig(mat*mat')./length(d_x);
        AreaEig(dt) = sum(sigmas);

        Mean(dt,1:2) = [mean(d_x) mean(d_y)];
        C = cov(d_x,d_y);
        [theta, ax] = error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
        axisRatio(dt) = max(ax)/min(ax);
        angle(dt) = rad2deg(theta);
        % corrleation coefficient of the distributions on the two axis (if the
        % distribution is not symmeterical the correlation value is higher)
        xyCorrelation(dt) = c(2,1);

        if GENERATEMOVIE
            n = 100;
            %plotting the 2D histogram for each delta t
            out = histogram2(d_x,d_y,[-LIMIT,LIMIT,n; -LIMIT,LIMIT,n]);
            %h = fspecial('average', 5);
            %out = filter2(h, out);
            pcolor(linspace(-LIMIT,LIMIT,n),linspace(-LIMIT,LIMIT,n), out')
            shading interp
            C = cov(d_x,d_y);
            hold on
            error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
            hold on
            axis([-LIMIT LIMIT -LIMIT LIMIT])
            axis square
            xlabel('horizontal displacement (arcmin)')
            ylabel('vertical displacement (arcmin)')
            title(sprintf('%s dt: %.2f ms cov: %.2f Area: %.2f arcmin^2',MOVIENAME, dt, C(1,2), Area(dt)))
            set(gca, 'FontSize', 12)
            set(gcf,'renderer','zbuffer')
            colormap hot
            %colorbar
            %caxis([0 200])
            %drawnow
            currFrame(dt) = getframe(gcf);
            writeVideo(aviobj,currFrame);
            if dt < NT-1
                cla
            end

        end
    end
    th = cart2pol(d_x,d_y);
    std_th = std(th);

    % close movie
    if GENERATEMOVIE
        close(aviobj);
        close all
    end
    
    % creaet figure
    if FIGKEY
        figure
        % 2d graph for the last delta t
        n = 100;
        %plotting the 2D histogram for each delta t
        out = histogram2(d_x,d_y,[-LIMIT,LIMIT,n; -LIMIT,LIMIT,n]);
        %h = fspecial('average', 5);
        %out = filter2(h, out);
        pcolor(linspace(-LIMIT,LIMIT,n),linspace(-LIMIT,LIMIT,n), out')
        shading interp
        C = cov(d_x,d_y);
        hold on
        error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
        hold on
        axis([-LIMIT LIMIT -LIMIT LIMIT])
        axis square
        xlabel('horizontal displacement (arcmin)')
        ylabel('vertical displacement (arcmin)')
        title(sprintf('%s dt: %.2f ms cov: %.2f Area: %.2f arcmin^2',FIGNAME, dt, C(1,2), Area(dt)))
        set(gca, 'FontSize', 12)
        set(gcf,'renderer','zbuffer')
        colormap hot
        %    print2eps(sprintf('./Figures/2dhist_%s.eps', FIGNAME)) 
    end
end
end


