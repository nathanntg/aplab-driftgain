% directories
dirs = {'7 Mia - Fast' '9 Dylan - Fast' '12 Patrick - Fast'};

% speed tables
segments = {};
gains = [];

% min_duration
min_duration = 100;

% for each directory
for col = 1:numel(dirs)
    load(fullfile('..', 'Raw', dirs{col}, 'processed.mat'));
    
    % for each session
    for idx = 1:numel(Data.x)
        % fast stabilization only
        if ~Data.user{idx}.FastStabilization
            continue;
        end
        
        % find gain
        gain = Data.user{idx}.DriftGain;
        
        % find row
        row = find(gains == gain);
        if isempty(row)
            gains = [gains gain];
            row = numel(gains);
        end
        
        % get segment
        eye_x = Data.x{idx};
        eye_y = Data.y{idx};
        Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

        % state changes
        states = split_states2(Result, Data.stream02{idx});
        
        % make segments
        segs = {};
        
        for i = 1:size(states.stabilize.start, 2)
            % get timing
            strt = states.stabilize.start(i);
            dur = states.stabilize.duration(i);

            % enforce minimum duration
            if dur < min_duration
                continue;
            end

            % current excerpts
            cur_eye_x = double(Result.x.position(strt:(strt + dur - 1)));
            cur_eye_y = double(Result.y.position(strt:(strt + dur - 1)));

            % get movement
            segs{end + 1} = struct('x', cur_eye_x, 'y', cur_eye_y);
        end
        
        % add it up
        if all([row col] <= size(segments))
            segments{row, col} = cat(2, segments{row, col}, segs);
        else
            segments{row, col} = segs;
        end
    end
end

for row = 1:size(segments, 1)
    for col = 1:size(segments, 2)
        gain = gains(row);
        drifts = segments{row, col};
        if isempty(drifts)
            continue
        end
        save(sprintf('subj%d-gain%.1f-drifts.mat', col, gain), 'drifts', 'gain');
    end
end
