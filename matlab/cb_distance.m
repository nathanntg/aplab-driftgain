function ret = cb_distance(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, CS, par)

% minimum duration
if dur < 100
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 7
	par = 41;
end
if nargin < 6
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

%% EYE MOVEMENT
% apply filter, if specified
if ~isempty(par)
    cur_eye_x = sgolayfilt(cur_eye_x, 3, par);
    cur_eye_y = sgolayfilt(cur_eye_y, 3, par);
end

%% IMAGE MOVEMENT
% apply filter, if specified
if ~isempty(par)
    cur_adj_x = sgolayfilt(cur_adj_x, 3, par);
    cur_adj_y = sgolayfilt(cur_adj_y, 3, par);
end

%% DISTANCE
rel_x = cur_eye_x - cur_adj_x;
rel_y = cur_eye_y - cur_adj_y;

ret = sqrt((rel_x(end-CS) - rel_x(CS)) ^ 2 + (rel_y(end-CS) - rel_y(CS)) ^ 2);

end

