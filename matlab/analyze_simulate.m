function [adjs_t, adjs_x, adjs_y] = simulate(x, y, blink, notrack, emevent, drift_gain, drift_angle)

% setup stabilizer
stab_iir_a = [0.0005212926, 0.0020851702, 0.0031277554, 0.0020851702, 0.0005212926];
stab_ord_a = 5;
stab_iir_b = [-1.8668754558, 0.8752161367];
stab_ord_b = 2;
stab_last_x = 0.;
stab_last_y = 0.;
stab_state = 0; % 0 - no track, 1 - slow, 2 - fast
stab_input_x = zeros(1, stab_ord_a);
stab_input_y = zeros(1, stab_ord_a);
stab_output_x = zeros(1, stab_ord_b);
stab_output_y = zeros(1, stab_ord_b);

% drift cos
if abs(drift_angle) > pi
    drift_angle = drift_angle * pi / 180.;
end
drift_angle_sin = sin(drift_angle);
drift_angle_cos = cos(drift_angle);

% drift offset
eye_movement = true;
drift_offset_x = 0;
drift_offset_y = 0;

% output
adjs_t = [];
adjs_x = [];
adjs_y = [];

for t = 1:size(x, 2)
    %% run stabilization
    if blink(t) || notrack(t)
        stab_state = 0; % no track
        
        % use current value
        stab_x = x(t);
        stab_y = y(t);
    else
        % prepend new samples
        stab_input_x = [x(t) stab_input_x(1:end - 1)];
        stab_input_y = [y(t) stab_input_y(1:end - 1)];
        
        stab_output_x = [(stab_input_x * stab_iir_a' - stab_output_x * stab_iir_b') stab_output_x(1:end - 1)];
        stab_output_y = [(stab_input_y * stab_iir_a' - stab_output_y * stab_iir_b') stab_output_y(1:end - 1)];
        
        if emevent(t)
            stab_state = 2; %fast
            
            % use current value
            stab_x = x(t);
            stab_y = y(t);
        else
            % reset filter
            if stab_state ~= 1
                stab_input_x = x(t) * ones(1, stab_ord_a);
                stab_input_y = y(t) * ones(1, stab_ord_a);
                stab_output_x = x(t) * ones(1, stab_ord_b);
                stab_output_y = y(t) * ones(1, stab_ord_b);
            end
            
            stab_state = 1; % slow
            
            % get values (where rounding would occur)
            stab_x = stab_output_x(1);
            stab_y = stab_output_y(1);
        end
    end
    stab_last_x = stab_x;
    stab_last_y = stab_y;
    
    % convert to pixel
    [eye_x, eye_y] = a2p(stab_x, stab_y);
    
    %% stabilize image
    if emevent(t)
        eye_movement = true;
        continue;
    end
    
    if eye_movement
        eye_movement = false;
    else
        % delta
        delta_x = (eye_x - drift_offset_x);
        delta_y = (eye_y - drift_offset_y);
        
        % calculated adjustment
        adj_x = delta_x - drift_gain * (delta_x * drift_angle_cos - delta_y * drift_angle_sin);
        adj_y = delta_y - drift_gain * (delta_x * drift_angle_sin + delta_y * drift_angle_cos);
        
        % values
        adjs_t = [adjs_t t];
        adjs_x = [adjs_x round(adj_x)];
        adjs_y = [adjs_y round(adj_y)];
    end
    
    % store last values
    drift_offset_x = eye_x;
    drift_offset_y = eye_y;
end
