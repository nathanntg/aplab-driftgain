set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Patrick';

% custom cb
cb = @(cur_t, cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_rel_speed_instant2(cur_t, cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true, @utility_analyze_adj);
[group2, all_res2, cnt2] = utility_build_comparison(@cb_rel_speed2, fast, subject, true, @utility_analyze_adj);

%% PLOT GAIN >= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Retinal speed distribution; Increased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains > 0.9)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 300]);
print(gcf, utility_build_name('distribution-rel', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Retinal speed distribution; Decreased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains < 1.1)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 300]);
print(gcf, utility_build_name('distribution-rel', 'slower', fast, subject), '-dpng', '-r300');