function Length = CalculateLengthTrace(X,Y,Threshold, TSgolay)
% from
% http://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algori
% thm

% defaults
if nargin < 3
    Threshold = 1.5;
end
if nargin < 4
    TSgolay = 41;
end

% minimum length
if numel(X) <= (TSgolay + 1)
    Length = nan;
    return;
end

% % Filter the eye movements
Xf = sgolayfilt(double(X), 3, TSgolay);
Yf = sgolayfilt(double(Y), 3, TSgolay);
Xf = X(round(TSgolay/2):end-round(TSgolay/2));
Yf = Y(round(TSgolay/2):end-round(TSgolay/2));
% Calculate the length of the section due to the drifts
Length = 0;
X = Xf;
Y = Yf;
StartSegment = 1; EndSegment = length(X);

while StartSegment < EndSegment
    
    % Calculate the segments
    x0 = Xf(StartSegment:EndSegment);
    y0 = Yf(StartSegment:EndSegment);
    
    % Calculate the parameters of the segment
    a = (y0(1) - y0(end));
    b = (x0(end) - x0(1));
    c = -x0(1) * a - y0(1) * b;
    
    % Calculate the distance between the segment and all the
    % points of the curve
    if (a == 0 && b == 0)
        d = sqrt((x0(1) - x0).^2 + (y0(1) - y0).^2);
    else
        d = abs(a * x0 + b * y0 + c) / sqrt(a^2 + b^2);
    end
    
    % Find all distances greater than the threshold
    id = (find(d > Threshold));
    
    if ~isempty(id)
        xi = x0(id(1));
        yi = y0(id(1));
        Length = Length + sqrt((xi - x0(1))^2 + (yi - y0(1))^2);
        StartSegment = StartSegment + id(1);
    else
        xi = x0(end);
        yi = y0(end);
        Length = Length + sqrt((xi - x0(1))^2 + (yi - y0(1))^2);
        StartSegment = EndSegment + 1;
    end
end