set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Patrick';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_brownian, fast, subject);

%% OUTPUT

% bias corrected
clf;
nm = 'brownian-a';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 3), true);
utility_plot_box(group, all_res(:, 3), sig_set, p_value, 'Diffusion coefficient');
ylim([0 50]);
utility_set_title('Diffusion coefficient (remove bias)', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% bias
clf;
nm = 'brownian-b';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 4), true);
utility_plot_box(group, all_res(:, 4), sig_set, p_value, 'Diffusion bias');
ylim([0 320]);
utility_set_title('Diffusion bias', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% bias corrected
nm = 'brownian';

subplot(2, 1, 1);
[sig_set, p_value] = utility_signifigance(group, all_res(:, 3), true);
utility_plot_box(group, all_res(:, 3), sig_set, p_value, 'Diffusion coefficient');
ylim([0 50]);
utility_set_title('Diffusion coefficient (remove bias)', fast, subject);

% bias
subplot(2, 1, 2);
[sig_set, p_value] = utility_signifigance(group, all_res(:, 4), true);
utility_plot_box(group, all_res(:, 4), sig_set, p_value, 'Diffusion bias');
ylim([0 320]);
utility_set_title('Diffusion bias', fast, subject);

print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');
