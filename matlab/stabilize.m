function [all_stab_x, all_stab_y] = stabilize(x, y)
%STABILIZE Summary of this function goes here
%   Detailed explanation goes here

% setup stabilizer
stab_iir_a = [0.0005212926, 0.0020851702, 0.0031277554, 0.0020851702, 0.0005212926];
stab_ord_a = 5;
stab_iir_b = [-1.8668754558, 0.8752161367];
stab_ord_b = 2;
stab_last_x = 0.;
stab_last_y = 0.;
stab_state = 0; % 0 - no track, 1 - slow, 2 - fast
stab_input_x = zeros(1, stab_ord_a);
stab_input_y = zeros(1, stab_ord_a);
stab_output_x = zeros(1, stab_ord_b);
stab_output_y = zeros(1, stab_ord_b);

% return
all_stab_x = zeros(size(x));
all_stab_y = zeros(size(y));

for t = 1:size(x, 2)
    %% run stabilization
    if 1 == t% || blink(t) || notrack(t)
        stab_state = 0; % no track
        
        % use current value
        stab_x = x(t);
        stab_y = y(t);
    else
        % prepend new samples
        stab_input_x = [x(t) stab_input_x(1:end - 1)];
        stab_input_y = [y(t) stab_input_y(1:end - 1)];
        
        stab_output_x = [(stab_input_x * stab_iir_a' - stab_output_x * stab_iir_b') stab_output_x(1:end - 1)];
        stab_output_y = [(stab_input_y * stab_iir_a' - stab_output_y * stab_iir_b') stab_output_y(1:end - 1)];
        
        if false %emevent(t)
            stab_state = 2; %fast
            
            % use current value
            stab_x = x(t);
            stab_y = y(t);
        else
            % reset filter
            if stab_state ~= 1
                stab_input_x = x(t) * ones(1, stab_ord_a);
                stab_input_y = y(t) * ones(1, stab_ord_a);
                stab_output_x = x(t) * ones(1, stab_ord_b);
                stab_output_y = y(t) * ones(1, stab_ord_b);
            end
            
            stab_state = 1; % slow
            
            % get values (where rounding would occur)
            stab_x = stab_output_x(1);
            stab_y = stab_output_y(1);
        end
    end
    stab_last_x = stab_x;
    stab_last_y = stab_y;
    
    all_stab_x(t) = stab_last_x;
    all_stab_y(t) = stab_last_y;
end

end

