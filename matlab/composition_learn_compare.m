set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Mia';

% OUTPUT name
nm = 'learn-compare';

% COMPARE range
group1 = 1000; % frist 1 second
group2 = 5000; % everything after 5 seconds
consider = 150;

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true, @cb_analyze_state2);

%% OUTPUT
% gains
gains = sort(unique(group));

% all at once
for g = gains'
    speed = all_res(group == g, :);
    
    % rows from group 1 and 2
    group1_speed = speed(speed(:, 1) <= group1 & speed(:, 2) <= consider, 3);
    group2_speed = speed(speed(:, 1) >= group2 & speed(:, 2) <= consider, 3);
    
    if 0 == numel(group1_speed) || 0 == numel(group2_speed)
        continue
    end
    
    % groups
    group_names = [repmat({sprintf('First %.1f seconds', group1 / 1000)}, [numel(group1_speed) 1]); repmat({sprintf('After %.1f seconds', group2 / 1000)}, [numel(group2_speed) 1])];
    
    % make box plot
    [h, p] = ttest2(group1_speed, group2_speed);
    if p <= 0.05 && false
        p_val = p;
        sig_set = {[1 2]};
    else
        p_val = [];
        sig_set = {};
    end
    utility_plot_box(group_names, [group1_speed; group2_speed], sig_set, p_val, 'Instant speed', 'Time');
    title(sprintf('Adaptation; gain %.1f; p = %.3f', g, p));
    
    % print signifigance
    fprintf('Gain: %.1f; p: %f\n', g, p);
    
    % print
    print(gcf, utility_build_name(nm, sprintf('gain%d', round(g * 10)), fast, subject), '-dpng', '-r300');
end
