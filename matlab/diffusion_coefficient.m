function d_co = diffusion_coefficient(x, y, sample_freq)

dimensions = 2;

nT = numel(x);
tm = (0:nT-1) ./ sample_freq;   % create a time vector for plotting
Dx = cell(nT,1);
Dy = cell(nT,1);

% change
for dt = 1:nT-1
    Dx{dt+1} = diff(x);
    Dy{dt+1} = diff(y);
end

% displacements at zero time
area = zeros(1, nT);

for dt = 2:nT
    % method-2 finding std along the principle axes
    n = length(Dx{dt});
    data = [Dx{dt}' Dy{dt}'];

    % Calculate the eigenvectors and eigenvalues
    covariance = cov(data);
    sigmas = eig(covariance);
    area(dt) = 2 * prod(sigmas);
end

% compute diffusion coefficent
ind = 1:round(nT / 2);

slp2 = regress(area(ind)', tm(ind)');
d_co = slp2 / (2 * dimensions);

end

