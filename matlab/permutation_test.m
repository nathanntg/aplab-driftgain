function p = permutation_test(val_a, val_b, rep, two_sided)
%PERMUTATION_TEST A one- or two-sided permutation test.
%   Returns the p-value related to rep number of shuffled permutations of a and b using
%   the difference in sample means as the test statistic.

% default value
if nargin < 3
    rep = 10000;
end

if nargin < 4
    two_sided = true;
end

% observed T
T = mean(val_a) - mean(val_b);

% concatenate values
concat = [val_a(:); val_b(:)];
div = max(size(val_a));
new_T = zeros(1, rep);
for i = 1:rep
	% shuffle
	concat = concat(randperm(max(size(concat))));
	new_T(i) = mean(concat(1:div)) - mean(concat(div + 1:end));
end

if two_sided
    p = sum(abs(new_T) >= abs(T)) / rep;
else
    p = sum(new_T >= T) / rep;
end
