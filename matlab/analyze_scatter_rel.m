if ~exist('idx', 'var')
    idx = 2;
end

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);

% full adj
full_adj_x = zeros(size(eye_x));
full_adj_y = zeros(size(eye_y));

maxi = size(adj_t, 2);
cum_adj_x = cumsum(adj_x);
cum_adj_y = cumsum(adj_y);
for i = 1:maxi
    % get time range
    from = adj_t(i);
    if i < maxi
        through = adj_t(i + 1) - 1;
    else
        through = size(eye_x, 2);
    end
    
    % set time range
    full_adj_x(from:through) = cum_adj_x(i);
    full_adj_y(from:through) = cum_adj_y(i);
end

% get relative movements
rel_x = Result.x.position - full_adj_x;
rel_y = Result.y.position - full_adj_y;

% state changes
states = split_states(Result.samples, Data.stream02{idx});

% minimum duration
min_duration = 100;

%% extract matching drifts
n = 0;
d_mov = [];
d_adj = [];
mask_size = 200;
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    stop = strt + dur - 1;
    
    if dur < min_duration
        continue;
    end
    
    % get stabilized
    [stab_x, stab_y] = stabilize(eye_x(strt:stop), eye_y(strt:stop));
%     subplot(2, 1, 1);
%     plot(strt:stop, eye_x(strt:stop), strt:stop, stab_x);
%     legend('Actual', 'Stabilized');
%     subplot(2, 1, 2);
%     plot(strt:stop, eye_y(strt:stop), strt:stop, stab_y);
%     legend('Actual', 'Stabilized');
%     pause;

    % get stabilized relative movements
    [stab_rel_x, stab_rel_y] = stabilize(rel_x(strt:stop), rel_y(strt:stop));

    % continuous
    %eye_move = mean(sqrt(diff(stab_x) .^ 2 + diff(stab_y) .^ 2));
    %rel_move = mean(sqrt(diff(stab_rel_x) .^ 2 + diff(stab_rel_y) .^ 2));
    
    % full motion
    eye_move = sqrt((stab_x(end) - stab_x(1)) ^ 2 + (stab_y(end) - stab_y(1)) ^  2);
    rel_move = sqrt((stab_rel_x(end) - stab_rel_x(1)) ^ 2 + (stab_rel_y(end) - stab_rel_y(1)) ^  2);
     
    d_mov = [d_mov; eye_move];
    d_adj = [d_adj; rel_move];
    
    n = n + 1;
end

% only plot if...
if ~exist('make_plot', 'var') || make_plot
    % create scatter plot
    scatter(d_mov, d_adj);
    title(sprintf('Comparison; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));
    xlabel('Eye movement');
    ylabel('Relative movement');

    % set axis
    x_min = min(min(d_mov), min(d_adj));
    x_max = max(max(d_mov), max(d_adj));
    xlim([0 x_max]);
    ylim([0 x_max]);
    axis square;

    % figure out fit
    %fit = [d_mov ones(size(d_mov))] \ d_adj;
    fit = d_mov \ d_adj;
    hold on;
    x = x_min:((x_max - x_min) / 10):x_max;
    plot(x, fit(1) * x, 'r:');

    % figure out desired
    plot(x, x * abs(Data.user{idx}.DriftGain), 'k--');

    hold off;

    legend('Velocity', sprintf('Slope: %.3f\n', fit(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain)), 'Location', 'NorthWest');
end