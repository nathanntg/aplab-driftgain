function utility_plot_sem(group, result, sig_set, p_value, y_label, x_label)

% increase font size
set(0, 'DefaultAxesFontSize', 16);

% unique gains
gains = sort(unique(group));

% figure out mean and sem
val_to_plot = ones(numel(gains), 3);
for i = 1:numel(gains)
    g = gains(i);
    val_to_plot(i, :) = [g mean(result(group == g)) std(result(group == g)) / sqrt(numel(result(group == g)))];
end

% make error bar plot
errorbar(val_to_plot(:, 1), val_to_plot(:, 2), val_to_plot(:, 3), '*', 'MarkerSize', 5);

% add signifigance indicators
if nargin >= 4 && ~isempty(p_value)
    sigstar2(sig_set, p_value);
end

% add labels
if nargin >= 6 && ~isempty(x_label)
    xlabel(x_label);
else
    xlabel('Gain parameter');
end

if nargin >= 5 && ~isempty(y_label)
    ylabel(y_label);
end

end

