function [res, n] = cb_analyze_saccade(Data, idx, cb, convert_p2a)
%UTILITY_ANALYZE Summary of this function goes here
%   Detailed explanation goes here

% build result
eye_x = Data.x{idx};
eye_y = Data.y{idx};
Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

res = [Result.saccades.amplitude Result.microsaccades.amplitude]';
n = numel(res);

end
