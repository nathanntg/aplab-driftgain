set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Dylan';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_distance_eye, fast, subject, false);

%% COMPARE DURATION
[sig_set, p_value] = utility_signifigance(group, all_res, true);
sig_set = {};
p_value = [];

utility_plot_box(group, all_res, sig_set, p_value, 'Drift segment amplitude');
ylim([0 70]); % Mia 35, Patrick 70, Dylan 70
utility_set_title('Drift amplitude across gains', fast, subject);
print(gcf, utility_build_name('len', 'boxplot', fast, subject), '-dpng', '-r300');

return;

%% PLOT GAIN >= 1
bin_size = 2.5;
min_bin = 0;
max_bin = ceil(prctile(all_res, 99) / bin_size) * bin_size;
bins = 0:bin_size:max_bin;

clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Distance of drift; gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-len', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Distance of drift; gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-len', 'slower', fast, subject), '-dpng', '-r300');
