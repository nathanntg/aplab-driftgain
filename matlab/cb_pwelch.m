function ret = cb_pwelch(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur)

win_size = 256; % 128, 256, 512

% too short
if dur < (2 * win_size)
    ret = nan;
    return;
end

% window
win = hann(win_size, 'periodic');

% frequency
fs = 1000;

% rel_x
eye_x = cur_eye_x - mean(cur_eye_x);
eye_y = cur_eye_y - mean(cur_eye_y);

% return
[pxx, f] = pwelch(eye_x, win', win_size / 2, win_size, fs);
[pxy, f] = pwelch(eye_y, win', win_size / 2, win_size, fs);

ret = [pxx' pxy' f'];

end

