function Velocity = calculate_instant_velocity(T, drift_start, drift_duration, par, CS, MinDuration)
%CALCULATE_DRIFT_VELOCITY Get drift velocity for a single drift segment.
%   par - the smoothing factor for ocular drift (we generally use 41, which 
%         low pass with a 30 Hz cut off)
%   CS - the amount you want to cut the "edges" of your drift segment, in 
%        this case we can cut half the size of the filter (Par) on both 
%        edges to avoid artifacts from the filter, or we can be more 
%        conservative
%   MinDuration - the minimum duration for a drift to be considered (it 
%                 cannot be lower than the filter size, you can set it at 
%                 about 100 ms)

% default values
if nargin < 4
    par = 41;
end
if nargin < 5
    CS = round(par / 2);
end
if nargin < 6
    MinDuration = 100;
end

% none
Velocity = nan;

% get start and end
S = drift_start;
E = S + drift_duration;

% confirm end is within analysis period
if E>(T.analysis.start+T.analysis.duration-1)
    E = (T.analysis.start+T.analysis.duration-1);
end

% confirm start is within analysis period
if S<(T.analysis.start)
    S = T.analysis.start;
end

% get eye positions
X = T.x.position(S:E);
Y = T.y.position(S:E);

% longer than minimum duration
if ((E-S)>(MinDuration))
    % apply filter, if specified
    if ~isempty(par)
        X = sgolayfilt(double(X),3,par);
        Y = sgolayfilt(double(Y),3,par);
    end
    
    % plot(1:max(size(X)), X, 1:max(size(Y)), Y);

    % calculate differences
    X1 = diff(X(CS:end-CS));
    Y1 = diff(Y(CS:end-CS)); 

    % eliminate outliers
    %id = find(abs(X1*1000)>200 | abs(Y1*1000)>200);
    %if isempty(id)

    % calculate velocity
    Velocity = sqrt(X1.^ 2 + Y1.^ 2)* 1000;
    %end
end

end

