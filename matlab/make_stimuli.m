function make_stimuli(read_dir, write_dir, number, desired_width, desired_height)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% get list of files
files = find_files(read_dir);

% shuffle
files = files(randperm(size(files, 1)), :);

i = 0;
for file = transpose(files)
    x = file{1};
    if regexp(x, 'jpg$')
        out = fullfile(write_dir, ['stim' int2str(i) '.bmp']);
        
        % make stimuli
        immakestimuli(x, out, desired_width, desired_height);
        
        % increment
        i = i + 1;
        if i >= number
            break
        end
    end
end
    

end

