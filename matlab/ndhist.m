% % ndhist
% displays a radial 2d histogram
% 
% Data input:
%            X: x coordinate
%            Y: y coordinate
% 
% user parameters:
% filter: This will filter the data, you may choose to follow it with a
%         number. This number will represent the radius of the circular
%         gaussian filter. Other ways to call it: 'filt','filtering','f'
%   axis: This is to set the range of the plot, it will be the maximum
%         radius that will be plotted. The default radius is the smallest
%         it can be to fit every last point of data.
%         Other ways to call it: 'axis','range','maxr'
%         Note: Matlab occasionally plots the radius labels underneath the
%         other figure. I don't know why or how to avoid it. Its a good
%         idea then to pass an 'axis' measure so you can be sure of it.
% numbins: This parameter will determine the number 'bins','f'


function [Out] = ndhist(X,Y,varargin)


%% Set program defaults
filtering=0;
userParam=0;
rangeXY=max(abs(X))+max(abs(Y))/20; % padds the bound by 1/20th the maximum
numBins=100;
N = [];

%% interperet user parameters
k=1;
while k <= nargin-2
    if ischar(varargin{k})
        switch lower(varargin{k})
            case {'filter','filt','filtering'}
                filtering=2; % default filter radius
            case {'axis','range','maxr'}
                rangeXY=varargin{k+1};
                k = k + 1;
            case {'bins','numbins','f'} % 'f' comes from the binfactor of nhist
                numBins=varargin{k+1};
                k = k + 1;
            otherwise
                warning(['not a parameter']);
        end
    end
    k = k + 1; % increment it so it goes to the next one
end

edgesXY = linspace(-rangeXY,rangeXY,numBins);
edges={edgesXY,edgesXY};
if size(X,1)==1
    X = X';
    Y = Y';
end
if isempty(N)
[N] = hist3([X Y],'Ctrs',edges);
end
N = N';
% normalize by area to get probability distribution
pN=N/(sum(N(:))); 

if filtering
    H = fspecial('disk',filtering);
    filtN = imfilter(pN,H,'replicate');
    N=filtN;
end


[X Y]=meshgrid(edgesXY,edgesXY);
R2=X.^2+Y.^2;
R=sqrt(R2);
a = (N.*R)./sum(sum(N.*R));
Out = a;
pcolor(edgesXY,edgesXY,N.*R); % compensate for radialness
colormap hot;
colormap(flipud(colormap))
axis tight
shading interp;

myPolar3;

end % function over 


% This version is for plotting a certain figure.
% 
%   myPolar: Polar coordinate plot, draws a polar grid onto the axis
% 
%  myPolar(); % just adds the grid to the axis
%  myPolar(theta,rho) % makes a plot using polar coordinates of
%        the angle THETA, in radians, versus the radius RHO
%  myPolar(z) same as obove for z complex
%  myPolar(THETA,RHO,S) uses the linestyle specified in string, like 'z'
%  myPolar(THETA,RHO,'linespec','parameter',...) uses the linestyle
%                  specified by the linspec functions of plot
% 
% 
% Here are the specific list of improvements from Matlab's function:
% The ability to add a polar grid to any other plot you make
% the ability to use all linspec parameters, like 'linewidth' and 'color'
% the ability to plot complex data
% The plot is still square, it does not gray out anything.
% Grid axis ticks go to two decimal places
%
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Jonathan Lansey May 2009,     questions to Lansey at gmail.com          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
function myPolar3(varargin)
% find hold state so we can put it back when we are done
hold_state = ishold;

nCircles=3;
nDegrees=30;
    
if nargin==0 % plot the grid now!

    addPolarGrid(nCircles,nDegrees);
    addPolarText(nCircles,nDegrees);
    
else
    if ~isreal(varargin{1})% if its complex
%       we just get x and y from the complex data
        z=varargin{1};
        x=real(z);
        y=imag(z);
        curArg=2;
    else % there must be theta and rho parameters passed
        [x,y] = pol2cart(varargin{1},varargin{2});
        curArg=3;
    end
%     k=curArg;
%     while k <= nargin && ischar(varargin{k})
%         switch (lower(varargin{k}))
%           case 'xlabel'
%             SXLabel = varargin{k + 1};
%             k = k + 1;
%           case 'ylabel'
%         end
%     end
    plot(x,y,varargin{curArg:end}); % plots the curves, but just to get the axis to where it will stay.
    hold on;
    addPolarGrid(nCircles,nDegrees); %add the grid with no parameters so it adds the grid!
    plot(x,y,varargin{curArg:end}); % adds the plot again, this time above the grid
    addPolarText(nCircles,nDegrees); %add the text to the grid on top of the curves
end

if ~hold_state
    hold off;
    set(gca,'xtick',[],'ytick',[]);
end
    
end % myPolar over
%------------------------------------------------------------------%

% This function adds a polar grid
function addPolarGrid(nCircles,nDegrees)
    hold_state = ishold; hold on;
    
    p=axis;
    p=max(abs(p));
    axis([-p p -p p]);
    
    axis equal;  axis manual;
    dTheta=nDegrees*pi/180;
    axiss=abs(axis);
    maxR=max(axiss);
    max2R=min(max([axiss(1:2) axiss(3:4)]));
    drawlines(max2R,dTheta);
    drawcircle(maxR,nCircles);
%     axis auto; % bring it back to the normal mode
    if ~hold_state % if hold is off, then you can also remove the axis labels
        hold off;
    end
end
    
%------------------------------------------------------------------%
function drawlines(R,dTheta)
% This draws circles of the radius
% dTheta is equal to the angular distance between the lines
offset_angle = 0:dTheta:2*pi;
for n=1:length(offset_angle)-1
    x=real([0 R]*exp(1i*offset_angle(n)));
    y=imag([0 R]*exp(1i*offset_angle(n)));
    plot(x,y,'--k','linewidth',2);
end

end % drawlines over
%------------------------------------------------------------------%

function parameter = axis2
   parameter=char([108    97   110   115   101   121]);
end

%------------------------------------------------------------------%
function drawcircle(R,nCircles)

r=linspace(0,R,nCircles+1);
w=0:.01:2*pi;

for n=2:length(r)
    x=real(r(n)*exp(1i*w));
    y=imag(r(n)*exp(1i*w));
    plot(x,y,'--k','linewidth',2)
%     thetaForText=pi/2.1;
%     zTextSpot=1.1*r(n)*exp(1i*thetaForText);
%     text(real(zTextSpot),imag(zTextSpot),num2str(abs(zTextSpot),'%.2f'));
    
end

end
%------------------------------------------------------------------%

% This function adds the polar text
function addPolarText(nCircles,nDegrees)
    % fSize   = get(cax, 'DefaultTextFontSize');
    hold_state = ishold; hold on;
    axiss=abs(axis);
    maxR=max(axiss);
    maxTextR=axiss(4);
        
    r=linspace(0,maxR,nCircles+1);
    for n=2:length(r)
        thetaForText=pi/2.1;
        zTextSpot=1.2*r(n)*exp(1i*thetaForText);
        if abs(zTextSpot)<maxTextR
            
            textHandle  = text(real(zTextSpot),imag(zTextSpot),num2str(r(n),'%.0f'),'fontSize',18,'fontweight','bold');
            uistack(textHandle, 'top');

%             text(real(zTextSpot),imag(zTextSpot),num2str(abs(zTextSpot),'
%             %.2f'),'fontSize',18,'fontweight','bold','color','white');
        end
    end
    if ~hold_state, hold off; end;
end
