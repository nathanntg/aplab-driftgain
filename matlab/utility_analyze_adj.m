function [res, n] = utility_analyze_adj(Data, idx, cb, convert_p2a)
%UTILITY_ANALYZE_ADJ Summary of this function goes here
%   Detailed explanation goes here

% build result
eye_x = Data.x{idx};
eye_y = Data.y{idx};
Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% extract adj  (in pixel)
adj_t = double(Data.stream00{idx}.ts);
if nargin >= 4 && convert_p2a
    [adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);
else
    adj_x = Data.stream00{idx}.data;
    adj_y = Data.stream01{idx}.data;
end

% cumulative sum adjustments
adj_x = cumsum(adj_x);
adj_y = cumsum(adj_y);

% apply filter, if specified
par = 41;

if ~isempty(par)
    adj_x = sgolayfilt(double(adj_x), 3, par);
    adj_y = sgolayfilt(double(adj_y), 3, par);
    eye_x = sgolayfilt(double(eye_x), 3, par);
    eye_y = sgolayfilt(double(eye_y), 3, par);
end

% sub eye
sub_eye_x = zeros(size(adj_x));
sub_eye_y = zeros(size(adj_y));

for i = 1:size(adj_t, 2)
    t = adj_t(i);
    sub_eye_x(i) = eye_x(t);
    sub_eye_y(i) = eye_y(t);
end

% state changes
states = split_states2(Result, Data.stream02{idx});

% min_duration
min_duration = 100;

% number of images
num_images = numel(strsplit(Data.user{idx}.ImageNames, ';'));
per_image = round(Result.samples / num_images);

%% extract matching drifts
res = [];
n = 0;
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    
%     % only if processed by a saccade or microsaccade
%     dpos = isIncludedIn(strt, Result.drifts);
%     if 0 < dpos
%         if 0 == isIncludedIn(Result.drifts.start(dpos) - 1, Result.saccades)
%             continue;
%         end
%     else
%         continue;
%     end
    
    % enforce minimum duration
    if dur < min_duration
        continue;
    end
    
    % current excerpts
    idx = adj_t >= strt & adj_t < (strt + dur);
    cur_t = adj_t(idx);
    cur_eye_x = double(sub_eye_x(idx));
    cur_eye_y = double(sub_eye_y(idx));
    cur_adj_x = double(adj_x(idx));
    cur_adj_y = double(adj_y(idx));
    
%     if 0 == n
%         plot(cur_eye_x - cur_eye_x(1), cur_eye_y - cur_eye_y(1), 'b', cur_adj_x - cur_adj_x(1), cur_adj_y - cur_adj_y(1), 'r');
%         pause;
%     end
    
    % get movement
    new_res = cb(cur_t, cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur);
    
    % exclude nan
    to_add = new_res(~any(isnan(new_res), 2), :);
    res = [res; to_add];
    
    % increment n
    if 0 < numel(to_add)
        n = n + 1;
    end
end

%n = size(res, 1);

end

