% directories
dirs = {'7 Mia - Fast' '9 Dylan - Fast' '12 Patrick - Fast'};

% speed tables
diffusion_area = {};
diffusion_area_eig = {};
diffusion_ds_sq = {};
gains = [];

% for each directory
for col = 1:numel(dirs)
    load(fullfile('..', 'Raw', dirs{col}, 'processed.mat'));
    
    % for each session
    for idx = 1:numel(Data.x)
        % fast stabilization only
        if ~Data.user{idx}.FastStabilization
            continue;
        end
        
        % find gain
        gain = Data.user{idx}.DriftGain;
        
        % find row
        row = find(gains == gain);
        if isempty(row)
            gains = [gains gain];
            row = numel(gains);
        end
        
        % get diffusion coefficients
        [res, ~] = utility_analyze(Data, idx, @cb_brownian3, true);
        
        % add it up
        if all([row col] <= size(diffusion_area))
            diffusion_area{row, col} = [diffusion_area{row, col}; res(:, 1)];
        else
            diffusion_area{row, col} = res(:, 1);
        end
        
        % add it up
        if all([row col] <= size(diffusion_area_eig))
            diffusion_area_eig{row, col} = [diffusion_area_eig{row, col}; res(:, 2)];
        else
            diffusion_area_eig{row, col} = res(:, 2);
        end
        
        % add it up
        if all([row col] <= size(diffusion_ds_sq))
            diffusion_ds_sq{row, col} = [diffusion_ds_sq{row, col}; res(:, 3)];
        else
            diffusion_ds_sq{row, col} = res(:, 3);
        end
    end
end

diffusion_area_mean = nan(size(diffusion_area));
diffusion_area_std = nan(size(diffusion_area));
diffusion_area_eig_mean = nan(size(diffusion_area_eig));
diffusion_area_eig_std = nan(size(diffusion_area_eig));
diffusion_ds_sq_mean = nan(size(diffusion_ds_sq));
diffusion_ds_sq_std = nan(size(diffusion_ds_sq));

sorted_gains = sort(gains);
for new_row = 1:numel(sorted_gains)
    gain = sorted_gains(new_row);
    old_row = find(gains == gain);
    
    for col = 1:numel(dirs)
        diffusion_area_mean(new_row, col) = mean(diffusion_area{old_row, col});
        diffusion_area_std(new_row, col) = std(diffusion_area{old_row, col});
        diffusion_area_eig_mean(new_row, col) = mean(diffusion_area_eig{old_row, col});
        diffusion_area_eig_std(new_row, col) = std(diffusion_area_eig{old_row, col});
        diffusion_ds_sq_mean(new_row, col) = mean(diffusion_ds_sq{old_row, col});
        diffusion_ds_sq_std(new_row, col) = std(diffusion_ds_sq{old_row, col});
    end
end

% combine the two
diffusion_area_combined = [];
for col = 1:size(diffusion_area_mean, 2)
	diffusion_area_combined = [diffusion_area_combined diffusion_area_mean(:, col)];
	diffusion_area_combined = [diffusion_area_combined diffusion_area_std(:, col)];
end

diffusion_area_eig_combined = [];
for col = 1:size(diffusion_area_eig_mean, 2)
	diffusion_area_eig_combined = [diffusion_area_eig_combined diffusion_area_eig_mean(:, col)];
	diffusion_area_eig_combined = [diffusion_area_eig_combined diffusion_area_eig_std(:, col)];
end

diffusion_ds_sq_combined = [];
for col = 1:size(diffusion_ds_sq_mean, 2)
	diffusion_ds_sq_combined = [diffusion_ds_sq_combined diffusion_ds_sq_mean(:, col)];
	diffusion_ds_sq_combined = [diffusion_ds_sq_combined diffusion_ds_sq_std(:, col)];
end

% Reorganize data
ss = [sorted_gains' diffusion_area_combined];
CommonPoints = [1 2 4 5 6 7 8 9 10];
xx = 1-ss(:,1);
xxs = xx(CommonPoints);
ym = ss(:,[2 4 6]);
ye = ss(:,[3 5 7]);

ym1 =  ss(~isnan(ss(:,2)),2);
ye1 =  ss(~isnan(ss(:,3)),3);
ym2 =  ss(~isnan(ss(:,4)),4);
ye2 =  ss(~isnan(ss(:,5)),5);
ym3 =  ss(~isnan(ss(:,6)),6);
ye3 =  ss(~isnan(ss(:,7)),7);

ymm = mean(ym(CommonPoints,:)')';
yme = std(ym(CommonPoints,:)')'/sqrt(size(ym,2));

% Curvature
% Plot average data for all common points across subjects
figure
ax = axes('position', [.15 .15 .75 .75], 'FontSize', 16);
hold on;

errorbar(xxs,ymm,yme,'o');
a = plot(xxs,ymm,'o-b');
set(a, 'MarkerFaceColor', 'w', 'LineWidth', 2);
axis([-2.5 2.5 0 12]);

b = line([0 0],[0 12]);
set(b,'linestyle','--');
b = line([1 1],[0 12]);
set(b,'linestyle','--');

text(-2.3, 12 * 0.95, 'opposite to eye', 'FontSize', 15);
text(0.3, 12 * 0.95, 'moves with eye', 'FontSize', 15);

text(-0.15, 12 * 0.2, 'normal', 'FontSize', 15, 'Rotation', 90);
text(1.15, 12 * 0.1, 'stabilized', 'FontSize', 15, 'Rotation', 90);

xlabel('Motion gain on monitor (\Delta x = Gain EM)');
ylabel('Diffusion area');
title('Diffusion area with controlled stimulus gain');

hold off;

print -depsc2 status_diffusion_area.eps


% % Plot individual data 
% figure
% ax = axes('position',[.15 .15 .75 .75], 'FontSize', 16);
% hold
% 
% errorbar(xxs,ym1,ye1);
% a = plot(xxs,ym1,'o-');
% set(a,'MarkerFaceColor','w')
% 
% Off = -0.05;
% errorbar(xxs+Off,ym2,ye2);
% a = plot(xxs+Off,ym2,'sk-');
% set(a,'MarkerFaceColor','w')
% 
% Off = +0.05;
% errorbar(xx+Off,ym3,ye3);
% a = plot(xx+Off,ym3,'or-');
% set(a,'MarkerFaceColor','r')


% Reorganize data
ss = [sorted_gains' diffusion_ds_sq_combined];
CommonPoints = [1 2 4 5 6 7 8 9 10];
xx = 1-ss(:,1);
xxs = xx(CommonPoints);
ym = ss(:,[2 4 6]);
ye = ss(:,[3 5 7]);

ym1 =  ss(~isnan(ss(:,2)),2);
ye1 =  ss(~isnan(ss(:,3)),3);
ym2 =  ss(~isnan(ss(:,4)),4);
ye2 =  ss(~isnan(ss(:,5)),5);
ym3 =  ss(~isnan(ss(:,6)),6);
ye3 =  ss(~isnan(ss(:,7)),7);

ymm = mean(ym(CommonPoints,:)')';
yme = std(ym(CommonPoints,:)')'/sqrt(size(ym,2));

% Curvature
% Plot average data for all common points across subjects
figure
ax = axes('position', [.15 .15 .75 .75], 'FontSize', 16);
hold on;

errorbar(xxs,ymm,yme,'o');
a = plot(xxs,ymm,'o-b');
set(a, 'MarkerFaceColor', 'w', 'LineWidth', 2);
axis([-2.5 2.5 0 600]);

b = line([0 0],[0 600]);
set(b,'linestyle','--');
b = line([1 1],[0 600]);
set(b,'linestyle','--');

text(-2.3, 600 * 0.95, 'opposite to eye', 'FontSize', 15);
text(0.3, 600 * 0.95, 'moves with eye', 'FontSize', 15);

text(-0.15, 600 * 0.2, 'normal', 'FontSize', 15, 'Rotation', 90);
text(1.15, 600 * 0.1, 'stabilized', 'FontSize', 15, 'Rotation', 90);

xlabel('Motion gain on monitor (\Delta x = Gain EM)');
ylabel('Displacement squared');
title('Displacement squared with controlled stimulus gain');

hold off;

print -depsc2 status_diffusion_ds_sq.eps
