set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Patrick';

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1, 6, 100, 11);
cb2 = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 6, 11);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true);
[group2, all_res2, cnt2] = utility_build_comparison(cb2, fast, subject, true);

%% PLOT GAIN >= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
max_x = 0;
for g = gains(gains > 0.9)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % get max_x
    max_x = max(max_x, min(x(F > 0.995)));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Drift speed distribution; Increased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains > 0.9)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 max_x]);
print(gcf, utility_build_name('distribution-drift-raw', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
max_x = 0;
for g = gains(gains < 1.1)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % get max_x
    max_x = max(max_x, min(x(F > 0.995)));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Drift speed distribution; Decreased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains < 1.1)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 max_x]);
print(gcf, utility_build_name('distribution-drift-raw', 'slower', fast, subject), '-dpng', '-r300');