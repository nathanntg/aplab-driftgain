% load('../analysis/processed.mat');

stab = false;

for idx = 1:max(size(Data.x))
    % string id
    experiment_id = int2str(idx);
    
    % get drift gain and angle
    gain = Data.user{idx}.DriftGain;
    angle = Data.user{idx}.DriftAngle;
    subject = Data.user{idx}.SubjectName;
    
    % build EMAT trial
    Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);
    
    % make file name
    if stab
        file_name = sprintf('../analysis/drifts/stab-gain%.1f-angle%.2f.mat', gain, angle);
    else
        file_name = sprintf('../analysis/drifts/gain%.1f-angle%.2f.mat', gain, angle);
    end
    
    % load existing database
    clear drifts;
    if exist(file_name, 'file')
        load(file_name);
        
        % is member already?
        if ismember(experiment_id, experiments)
            fprintf('Experiment "%s" already in database, skipping...\n', experiment_id);
            continue;
        end
    else
        experiments = {};
    end
    
    % get drift events
    if stab
        trial_drifts = extract_stabilized_drift_events(Result, Data.stream02{idx});
    else
        trial_drifts = Result.drifts;
    end
    
    % iterate over drift events
    for i = 1:size(trial_drifts.start, 2)
        % data to add
        start = trial_drifts.start(i);
        dur = trial_drifts.duration(i);
        amp = trial_drifts.amplitude(i);
        ang = trial_drifts.angle(i);
        
        
        % make drift object
        drift_struct = struct('x', double(Result.x.position(start:(start + dur - 1))), 'y', double(Result.y.position(start:(start + dur - 1))));
        
        % analyze
        vel = calculate_drift_velocity(Result, start, dur);
%         try
%             area = Calculate2DArea(drift_struct);
%         catch err
%             disp(err);
%             area = nan;
%         end
        area = nan;
        len = CalculateLengthTrace(drift_struct.x, drift_struct.y);
        [curve1, curve2] = CalculateCurvature(drift_struct.x, drift_struct.y);
        
        % drift entry
        new_drift = struct('start', start, 'duration', dur, 'amplitude', amp, 'angle', ang, 'velocity', vel, 'area', area, 'length', len, 'curvature', curve1, 'curvature2', curve2);
        
        if ~exist('drifts', 'var')
            drifts = new_drift;
        else
            drifts(end + 1) = new_drift;
        end
    end
    
    % add to experiment list
    experiments(end + 1) = cellstr(experiment_id);
    
    % only save if it exists
    if exist('drifts','var')
        save(file_name, 'gain', 'angle', 'drifts', 'experiments', 'stab');
    end
end
