function ret = cb_brownian3(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur )

if dur < 257
    ret = nan;
    return;
end

[DCoefArea, DCoefAreaEig, DCoefDsSq, ~, ~] = Calculate2DArea_mini(struct('hor', cur_eye_x, 'ver', cur_eye_y));
ret = [DCoefArea, DCoefAreaEig, DCoefDsSq];

end

