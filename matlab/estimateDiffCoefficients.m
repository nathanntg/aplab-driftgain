function [D1, D2, D3, bias] = estimateDiffCoefficients (x, y, samplingFreq)

% x, y: horizontal and vertical traces of drift samples
%   D1: diffusion coefficient obtained from the slope of the regression
%       line between <d^2> and deltaT.
%   D2: diffusion coefficient obtained from the slope of the regression
%       line between Gaussian area and deltaT.
%   D3: diffusion coefficient obtained from the slope of the regression
%       line after removing bias term using PCA.
% BiasMeanX, BiasMeanY: Directional bias
%
% example:
% [x, y] = generate2DRandomWalk (41, 1000, 100, 1000, 60, 10, .02);
% [D1, D2, D3, bias] = estimateDiffCoefficients (x, y, 1000);
% % plot estimated bias direction
% plot(atan(bias.biasMeanY./bias.biasMeanX).*180/pi) %


dimensions = 2;

[~, nT] = size(x);
tm = (0:nT-1)./samplingFreq;   % create a time vector for plotting
Dx = cell(nT,1);
Dy = cell(nT,1);

for dt = 1:nT-1
    dx = x(:,1+dt:end) - x(:,1:end-dt);
    dy = y(:,1+dt:end) - y(:,1:end-dt);          % pool displacements
    
    Dx{dt+1} = [Dx{dt+1} dx(:)'];
    Dy{dt+1} = [Dy{dt+1} dy(:)'];
end

% displacements at zero time
dispSquared = zeros(1, nT);
area = zeros(1, nT);
area_unbiased = zeros(1, nT);
area_unbiased(1) = 0;

for dt = 2:nT
    % method-1
    dispSquared(dt) = mean(Dx{dt}.^2 + Dy{dt}.^2);
    
    % method-2 finding std along the principle axes
    n = length(Dx{dt});
    mat = [Dx{dt}; Dy{dt}];
    sigmas = eig(mat*mat')./n;
    area(dt) = sum(sigmas);
    
    % method-3 (removing the mean (bias))
    bias.biasMeanX(dt-1) = mean(Dx{dt});
    bias.biasMeanY(dt-1) = mean(Dy{dt});
    bias.biasStdX(dt-1) = std(Dx{dt});
    bias.biasStdY(dt-1) = std(Dy{dt});
    
    mat = [Dx{dt} - bias.biasMeanX(dt-1); ...
        Dy{dt} - bias.biasMeanY(dt-1)];
    
    sigmas = eig(mat*mat')./n;
    area_unbiased(dt) = sum(sigmas);
end

% compute diffusion coefficent
ind = 1:round(nT / 2);

slp1 = regress(dispSquared(ind)', tm(ind)');
D1 = slp1/(2*dimensions);

% DEBUGGING: plot regression
%figure;
%plot(tm, dispSquared, 'b-', tm(ind), tm(ind)*slp1, 'r:');
%legend('Actual', sprintf('Slope %f', slp1));
%title('Code by Murat; uses intervals up to T / 2');

slp2 = regress(area(ind)', tm(ind)');
D2 = slp2/(2*dimensions);

slp3 = regress(area_unbiased(ind)', tm(ind)');
D3 = slp3/(2*dimensions);
