function ret = cb_vel_relative(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, CS, par)

% default values
if nargin < 7
	par = 41;
end
if nargin < 6
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

% EYE SPEED
% apply filter, if specified
if ~isempty(par)
    cur_eye_x = sgolayfilt(cur_eye_x, 3, par);
    cur_eye_y = sgolayfilt(cur_eye_y, 3, par);
end

ret = [diff(cur_eye_x(CS:end+1-CS))' diff(cur_eye_y(CS:end+1-CS))'] * 1000;
end

