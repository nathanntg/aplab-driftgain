function utility_plot_box(group, result, sig_set, p_value, y_label, x_label)
%UTILITY_PLOT_BOX Summary of this function goes here
%   Detailed explanation goes here

% increase font size
set(0, 'DefaultAxesFontSize', 16);

% draw box plot, without silly outliers
h = boxplot(result, group);

line_width = get(0, 'DefaultLineLineWidth');
if line_width > 1
    for ih=1:6
        set(h(ih,:), 'LineWidth', 2);
    end
end

font_size = get(0, 'DefaultAxesFontSize');
if font_size > 12
    set(findobj(gca, 'Type', 'text'), 'FontSize', font_size - 1);
end

delete(findobj(gca,'tag','Outliers'));

% add signifigance indicators
if nargin >= 4 && ~isempty(p_value)
    sigstar(sig_set, p_value);
end

if nargin >= 6 && ~isempty(x_label)
    xlabel(x_label);
else
    xlabel('Gain parameter');
end

if nargin >= 5 && ~isempty(y_label)
    ylabel(y_label);
end

end

