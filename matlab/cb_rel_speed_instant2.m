function ret = cb_rel_speed_instant2(cur_t, cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, tm)

% default min duration
min_dur = 100;

% minimum duration
if dur < min_dur
    ret = nan(1, 2);
    return;
end

% time blocks
if nargin < 6
    tm = 20; % ms
end

% EYE SPEED

% REL SPEED
rel_x = diff(cur_eye_x) - diff(cur_adj_x);
rel_y = diff(cur_eye_y) - diff(cur_adj_y);

% speed vector
spd = sqrt(rel_x .^ 2 + rel_y .^ 2) ./ diff(cur_t) * 1000;

% windowed average
max_t = numel(spd);
max_j = ceil(max_t / tm);
ret = zeros(max_j, 2);
for j = 1:max_j
    t = (j - 1) * tm + 1;
    ret(j, :) = [t mean(spd(t:min(j * tm, max_t)))];
end

end

