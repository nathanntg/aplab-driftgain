function analyze_subplot(eye, adj_t, adj, states, trig, stream)
%ANALYZE_SUBPLOT Summary of this function goes here
%   Detailed explanation goes here

% max time
max_t = size(eye, 2);

%plot(eye);
xlim([1 max_t]);
ylim([-600 600]);
hold on;

% plot states
lim = get(gca, 'YLim');

% % 1. stabilize
% clr = [210 237 216] / 255;
% for i = 1:size(states.stabilize.start, 2)
%     strt = states.stabilize.start(i);
%     dur = states.stabilize.duration(i);
%     fill([strt strt strt + dur strt + dur], [lim(1) (lim(2) - 0.5) (lim(2) - 0.5) lim(1)], clr, 'EdgeColor', 'none', 'Clipping', 'On');
%     alpha(0.5);
% end

% 2. movements - light orange
clr = [255 175 100] / 255;
for i = 1:size(states.eyemovement.start, 2)
    strt = states.eyemovement.start(i);
    dur = states.eyemovement.duration(i);
    fill([strt strt strt + dur strt + dur], [lim(1) (lim(2) - 0.5) (lim(2) - 0.5) lim(1)], clr, 'EdgeColor', 'none', 'Clipping', 'On');
    alpha(0.5);
end

% 3. too fast - dark orange
clr = [222 125 0] / 255;
for i = 1:size(states.toofast.start, 2)
    strt = states.toofast.start(i);
    dur = states.toofast.duration(i);
    fill([strt strt strt + dur strt + dur], [lim(1) (lim(2) - 0.5) (lim(2) - 0.5) lim(1)], clr, 'EdgeColor', 'none', 'Clipping', 'On');
    alpha(0.5);
end

% 4. edge
clr = [183 183 219] / 255;
for i = 1:size(states.edge.start, 2)
    strt = states.edge.start(i);
    dur = states.edge.duration(i);
    fill([strt strt strt + dur strt + dur], [lim(1) (lim(2) - 0.5) (lim(2) - 0.5) lim(1)], clr, 'EdgeColor', 'none', 'Clipping', 'On');
    alpha(0.5);
end

% plot eye movement
plot(eye);
alpha(1.0);

if nargin >= 5
    ored = trig.blink | trig.em_event1 | trig.notrack;
    t = 1:max_t;
    t = t(ored);
    %scatter(t, 595 * ones(size(t)), 2, 'r.');
    for i = 1:size(t, 2)
        line([t(i) t(i)], [lim(1) (lim(2) - 0.5)], 'Color', [1 0 0]);
    end
end

% plot frames
if nargin >= 6
    for i = 1:size(stream.ts, 2)
        line([stream.ts(i) stream.ts(i)], [lim(1) (lim(2) - 0.5)], 'Color', [0 0 0]);
    end
end

% plot adjustments
for i = 1:size(states.stabilize.start, 2)
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    
    % get indices
    ii = (adj_t >= strt & adj_t < strt + dur);
    
    plot(adj_t(ii), eye(strt) + cumsum(adj(ii)), 'g-');
end

% other
non_stab = invertEvents(states.stabilize, max_t);
for i = 1:size(non_stab.start, 2)
    strt = non_stab.start(i);
    dur = non_stab.duration(i);
    
    % get indices
    ii = (adj_t >= strt & adj_t < strt + dur);
    
    plot(adj_t(ii), eye(strt) + cumsum(adj(ii)), 'rx-');
end




% isIncludedIn(start, events)
%scatter(adj_t, cumsum(adj), 'g');

hold off;

end

