set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Patrick';

% name
nm = 'curvature';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_curvature, fast, subject);


%% OUTPUT
[sig_set, p_value] = utility_signifigance(group, all_res, true, @permutation_test);

utility_plot_box(group, all_res, sig_set, p_value, 'Curvature');
ylim([0 1.3]);
utility_set_title('Drift curvature', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');
