set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

% true: fast only; false: slow only; nan: all
fast = false;

% directories
dirs = {'7 Mia - Fast' '8 Mia - Slow' '9 Dylan - Fast' '10 Dylan - Slow' '12 Patrick - Fast'};
output_dir = fullfile('~', 'Desktop');

% single example to save scatter plot
single_example = 1.7;


group = [];
all_adj = [];
all_mov = [];
count = [];
for d = dirs
    load(fullfile('..', 'Raw', d{1}, 'processed.mat'));
    for idx = 1:numel(Data.x)
        gain = Data.user{idx}.DriftGain;
        
        % fast
        if ~isnan(fast)
            if fast && ~Data.user{idx}.FastStabilization
                continue;
            end
            if ~fast && Data.user{idx}.FastStabilization
                continue;
            end
        end
        
        % save single example
        if ~isnan(single_example) && round(abs(gain - single_example) * 10) == 0
            make_plot = true;
            
            % calculate
            analyze_scatter_vel_instant;
            %analyze_scatter_rel_vel_instant;
            %analyze_scatter_rel_vel; % not instant
            %analyze_scatter_rel; %IIR
            
            if fast
                fname = 'scatter-fast.png';
            else
                fname = 'scatter-slow.png';
            end
            
            print(gcf, fullfile(output_dir, fname), '-dpng', '-r300');
            
            % save scatter plot
            single_example = nan;
        else
            make_plot = false;
            
            % calculate
            analyze_scatter_vel_instant;
            %analyze_scatter_rel_vel_instant;
            %analyze_scatter_rel_vel; % not instant
            %analyze_scatter_rel; %IIR
        end
        
        % append
        all_adj = [all_adj; d_adj];
        all_mov = [all_mov; d_mov];
        group = [group; gain * ones(size(d_adj))];
        count = [count; gain n];
    end
end

% for each gain
for gain = unique(group)'
    d_mov = all_mov(group == gain);
    d_adj = all_adj(group == gain);
    d = dataset(d_mov, d_adj);
    mdl = LinearModel.fit(d, 'd_adj ~ d_mov - 1');
    fprintf('%.1f\t& $%.2f \\pm %.3f$\t& %d\\\\\n', gain, mdl.Coefficients.Estimate(1), mdl.Coefficients.SE(1), sum(count(count(:, 1) == gain, 2)));
end

fprintf('Mean: %f\n', mean(abs((all_adj(group ~= 1) ./ all_mov(group ~= 1)) - abs(group(group ~= 1) - 1))));
fprintf('Std Dev: %f\n', std((all_adj(group ~= 1) ./ all_mov(group ~= 1)) - abs(group(group ~= 1) - 1)));

% % for each gain
% for gain = unique(group)'
%     d_mov = all_mov(group == gain);
%     d_adj = all_adj(group == gain);
%     d = dataset(d_mov, d_adj);
%     mdl = LinearModel.fit(d, 'd_adj ~ d_mov');
%     fprintf('%.2f\t%f +- %f\t%f +- %f\tn = %d\n', gain, mdl.Coefficients.Estimate(1), mdl.Coefficients.SE(1), mdl.Coefficients.Estimate(2), mdl.Coefficients.SE(2), sum(count(count(:, 1) == gain, 2)));
% end
