set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Patrick';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_duration, fast, subject, false, @cb_analyze_true_duration);

%% COMPARE DURATION
[sig_set, p_value] = utility_signifigance(group, all_res, true);
sig_set = {};
p_value = [];

utility_plot_box(group, all_res, sig_set, p_value, 'Drift duration (ms)');
ylim([100 2500]); % Mia 750, Patrick 2500, Dylan 1750
utility_set_title('Duration across gains', fast, subject);
print(gcf, utility_build_name('dur', 'boxplot', fast, subject), '-dpng', '-r300');

%% PLOT GAIN >= 1
bin_size = 25;
min_bin = 75;
max_bin = ceil(prctile(all_res, 99) / bin_size) * bin_size;
bins = 0:bin_size:max_bin;

clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Duration of drift (ms); gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-dur', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Duration of drift (ms); gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-dur', 'slower', fast, subject), '-dpng', '-r300');
