function ret = cb_pwelch_rel(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur)

win_size = 256; % 128, 256, 512

% too short
if dur < (2 * win_size)
    ret = nan;
    return;
end

% window
win = hann(win_size, 'periodic');

% frequency
fs = 1000;

% REL SPEED
rel_x = cur_eye_x - cur_adj_x;
rel_y = cur_eye_y - cur_adj_y;

% rel_x
rel_x = rel_x - mean(rel_x);
rel_y = rel_y - mean(rel_y);

% return
[pxx, f] = pwelch(rel_x, win', win_size / 2, win_size, fs);
[pxy, f] = pwelch(rel_y, win', win_size / 2, win_size, fs);

ret = [pxx' pxy' f'];

end

