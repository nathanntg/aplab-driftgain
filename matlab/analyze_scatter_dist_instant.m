% plot for Martina
idx = 10;

% 4: stabilized (0x gain)
% 5: move opposing egain (2x gain)
% 3: control (1x gain)
% 6: 1.5x gain
% 7: extreme (3x gain)
% 10: edge detection

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);

full_adj_x = zeros(size(eye_x));
full_adj_y = zeros(size(eye_y));

maxi = size(adj_t, 2);
cum_adj_x = cumsum(adj_x);
cum_adj_y = cumsum(adj_y);
for i = 1:maxi
    % get time range
    from = adj_t(i);
    if i < maxi
        through = adj_t(i + 1) - 1;
    else
        through = size(eye_x, 2);
    end
    
    % set time range
    full_adj_x(from:through) = cum_adj_x(i);
    full_adj_y(from:through) = cum_adj_y(i);
end

% state changes
states = split_states(Result.samples, Data.stream02{idx});

%% extract matching drifts
d_mov = [];
d_adj = [];
mask_size = 500;
mask = size(mask_size, 1) / mask_size;
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    stop = strt + dur - 1;
    
    % get movement
    cur_eye_x = eye_x(start:stop);
    cur_eye_y = eye_y(start:stop);
    
    % cur adjustment
    cur_adj_x = full_adj_x(start:stop);
    cur_adj_y = full_adj_y(start:stop);
    
    % smooth
    cur_eye_x = conv(cur_eye_x, ones(5, 1) / 5, 'same');
    cur_eye_y = conv(cur_eye_y, ones(5, 1) / 5, 'same');
    
    for j = 1:mask_size:size(cur_eye_x, 2)
        d_mov = [d_mov; sqrt((cur_eye_x(end) - cur_eye_x(1)) ^ 2 + (cur_eye_y(end) - cur_eye_y(1)) ^ 2)];
    end
    for j = 1:mask_size:size(cur_adj_x, 2)
        d_adj = [d_adj; sqrt((cur_adj_x(end) - cur_adj_x(1)) ^ 2 + (cur_adj_y(end) - cur_adj_y(1)) ^ 2)];
    end
end

% create scatter plot
scatter(d_mov, d_adj);
title(sprintf('Comparison; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));
xlabel('Eye movement');
ylabel('Image adjustment');

% set axis
x_min = min(min(d_mov), min(d_adj));
x_max = max(max(d_mov), max(d_adj));
xlim([0 x_max]);
ylim([0 x_max]);
axis square;

% figure out fit
%fit = [d_mov ones(size(d_mov))] \ d_adj;
fit = d_mov \ d_adj;
hold on;
x = x_min:((x_max - x_min) / 10):x_max;
plot(x, fit(1) * x, 'r:');

% figure out desired
plot(x, x * abs(Data.user{idx}.DriftGain - 1), 'k--');

hold off;

legend('Adjustment', sprintf('Slope: %.3f\n', fit(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain - 1)), 'Location', 'NorthWest');
