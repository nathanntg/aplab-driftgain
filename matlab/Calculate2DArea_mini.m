function [DCoefArea, DCoefAreaEig, DCoefDsSq, Mean, xyCorrelation] = Calculate2DArea_mini(fix)
%, angle

% set default parameters
NT = 256; % ms
TIMEINTERVAL = 1; %ms
FSAMPLING = 1000;
DIMENSIONS = 2; % 2d

time = linspace(1,NT-1,NT-1)./FSAMPLING;

max_dt = numel(time);

% initiate variables
angle = zeros(max_dt, 1);
Area = zeros(max_dt, 1);
DiffCoef = zeros(max_dt, 1);
AreaEig = zeros(max_dt, 1);
Mean = zeros(max_dt, 2);
xyCorrelation = zeros(max_dt, 1);

% loop through all delta t
for dt = 1:max_dt
    d_x = [];
    d_y = [];
    dd2 = [];
    
    % loop through all events
    for fx = 1:length(fix)
        if 2 == length(TIMEINTERVAL)
            hor = fix(fx).hor(TIMEINTERVAL(1):TIMEINTERVAL(2));
            ver = fix(fx).ver(TIMEINTERVAL(1):TIMEINTERVAL(2));
        else
            hor = fix(fx).hor(TIMEINTERVAL(1):end);
            ver = fix(fx).ver(TIMEINTERVAL(1):end);
        end

        % pick all the possible couples of values based on nT and the
        % maximum time interval
        enD = length(hor)-dt;
        d_x = [d_x (hor(1+dt:enD+dt)-hor(1:enD))];
        d_y = [d_y (ver(1+dt:enD+dt)-ver(1:enD))];
        dd2 = [ dd2 (hor(1+dt:enD+dt)-hor(1:enD)).^2 + (ver(1+dt:enD+dt)-ver(1:enD)).^2 ];
    end

    % calculate the area of the 2D distribution
    c = corrcoef(d_x',d_y');
    DiffCoef(dt) = mean(dd2);
    Area(dt) = 2*std(d_x)*std(d_y)*sqrt(1-c(2,1)^2);
    Mean(dt,1:2) = [mean(d_x) mean(d_y)];
    xyCorrelation(dt) = c(2,1);
    %C = cov(d_x,d_y);
    %[cur_theta, ~] = error_ellipse(C, [mean(d_x); mean(d_y)],'conf', .95, 'style', 'w');
    %angle(dt) = rad2deg(cur_theta);

    mat = [d_x - mean(d_x); ...
        d_y - mean(d_y)];    
    sigmas = eig(mat*mat')./length(d_x);
    AreaEig(dt) = sum(sigmas);
end

% regress the area and find the regression coefficients%
RegCoefArea = regress(Area, time');
RegCoefDsSq = regress(DiffCoef, time');
RegCoefAreaEig = regress(AreaEig, time');

% rate of increase for sec
DCoefArea = (RegCoefArea(1)/(2*DIMENSIONS));
DCoefAreaEig = (RegCoefAreaEig(1)/(2*DIMENSIONS));
DCoefDsSq = (RegCoefDsSq(1)/(2*DIMENSIONS));
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

end
