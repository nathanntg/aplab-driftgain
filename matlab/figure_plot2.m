% load data
load(fullfile('..', 'Raw', '7 Mia - Fast', 'processed.mat'));

output_dir = fullfile('~', 'Desktop');

% index
idx = 5; % gain 1.7 for Mia - Fast

% time range
start = 2700;
stop = 3650;

% y range
x_min = min(Data.x{idx}(start:stop - 15)) - 5;
x_max = max(Data.x{idx}(start:stop - 15));

y_min = min(Data.y{idx}(start:stop - 15));
y_max = max(Data.y{idx}(start:stop - 15));

if (x_max - x_min) > (y_max - y_min)
    range_diff = ((x_max - x_min) - (y_max - y_min)) / 2;
    y_min = y_min - range_diff;
    y_max = y_max + range_diff;
else
    range_diff = ((y_max - y_min) - (x_max - y_min)) / 2;
    x_min = x_min - range_diff;
    x_max = x_max + range_diff;
end
range = (x_max - x_min);
x_max = x_max + 0.05 * range;
x_min = x_min - 0.05 * range;
y_max = y_max + 0.05 * range;
y_min = y_min - 0.05 * range;

% plot
analyze_plot;

% adjust top plot
subplot(2, 1, 1);
xlim([start stop]);
ylim([x_min x_max]);
ylabel('Horizontal position (arcmin)');

% adjust bottom plot
subplot(2, 1, 2);
xlim([start stop]);
ylim([y_min y_max]);
ylabel('Vertical position (arcmin)');
xlabel('Time (ms)');

print(gcf, fullfile(output_dir, 'plot.png'), '-dpng', '-r300');
