idx = 15;

% plot indices for Martina
% 4: stabilized (0x gain)
% 5: move opposing egain (2x gain)
% 3: control (1x gain)
% 6: 1.5x gain
% 7: extreme (3x gain)
% 10: edge detection

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(round(Data.stream00{idx}.data), round(Data.stream01{idx}.data));

% state changes
states = split_states(Result.samples, Data.stream02{idx});

%% extract matching drifts
d_mov = [];
d_adj = [];
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    stop = strt + dur - 1;
    
    % get movement
    mov = sqrt((eye_x(stop) - eye_x(strt)) ^ 2 + (eye_y(stop) - eye_y(strt)) ^ 2);
    d_mov = [d_mov; mov];
    
    % get index
    adj_index = (adj_t >= strt & adj_t <= stop);
    
    adj = sqrt(sum(adj_x(adj_index)) ^ 2 + sum(adj_y(adj_index)) ^ 2);
    d_adj = [d_adj; adj];
end

% create scatter plot
scatter(d_mov, d_adj);
title(sprintf('Comparison; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));
xlabel('Eye movement');
ylabel('Image adjustment');

% set axis
x_min = min(min(d_mov), min(d_adj));
x_max = max(max(d_mov), max(d_adj));
xlim([0 x_max]);
ylim([0 x_max]);
axis square;

% figure out fit
%fit = [d_mov ones(size(d_mov))] \ d_adj;
fit = d_mov \ d_adj;
hold on;
x = x_min:((x_max - x_min) / 10):x_max;
plot(x, fit(1) * x, 'r:');

% figure out desired
plot(x, x * abs(Data.user{idx}.DriftGain - 1), 'k--');

hold off;

legend('Adjustment', sprintf('Slope: %.3f\n', fit(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain - 1)), 'Location', 'NorthWest');
