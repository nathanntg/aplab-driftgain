set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% NAME
nm = 'speed-mean';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_speed, fast, subject, true);

% extract all speed results
all_speed = all_res(:, 1);

% estimate gain hat
m = double(group >= 1) * 2 - 1; % -1 for gain < 1; 1 for gain > 1
all_gain_hat = 1 + (m .* (all_res(:, 2) ./ all_speed));

%% OUTPUT
[sig_set, p_value] = utility_signifigance(group, all_speed, true);

utility_plot_box(group, all_speed, sig_set, p_value, 'Drift speed (arcmin/sec)');
ylim([0 375]);
utility_set_title('Mean drift speed across trials', fast, subject);
ylim([20 200]);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');
