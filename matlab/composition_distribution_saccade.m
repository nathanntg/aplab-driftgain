set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Dylan';

% custom cb
cb = @(x) x;

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, false, @cb_analyze_saccade);

%% PLOT GAIN >= 1
bin_size = 5;
min_bin = 0;
max_bin = ceil(prctile(all_res, 99) / bin_size) * bin_size;
bins = 0:bin_size:max_bin;

clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Saccades; gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-sacc', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % get bin counts
    cnt = histc(all_res(group == g, :), bins);
    
    % sub plot
    subtightplot(sum(gains > 0.9), 1, clr);
    
    % make plot
    bar(bins, cnt, 'FaceColor', color(clr, :));
    xlim([min_bin max_bin]);
    
    legend(sprintf('Saccades; gain %.1f', g));
    clr = clr + 1;
end
for goa = findobj(gcf, 'type', 'Axes')'
    if strcmp(goa.YTickLabel(1), '0')
        goa.YTickLabel{1} = ' ';
    end
end
print(gcf, utility_build_name('distribution-sacc', 'slower', fast, subject), '-dpng', '-r300');
