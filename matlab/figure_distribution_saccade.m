%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% OUTPUT name
nm = 'speed';

% custom cb
cb = @(x) x;

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, false, @cb_analyze_saccade);

%% PLOT GAIN >= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, :), 10);
    
    % convert to pdf
    p = diff(F);
    
    plot(x(2:end), p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Saccade amplitude distribution; Increased retinal motion');
legend(lbl);
print(gcf, utility_build_name('distribution-sacc', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, :), 10);
    
    % convert to pdf
    p = diff(F);
    
    plot(x(2:end), p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Saccade amplitude distribution; Decreased retinal motion');
legend(lbl);
print(gcf, utility_build_name('distribution-sacc', 'slower', fast, subject), '-dpng', '-r300');