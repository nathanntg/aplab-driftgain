%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Dylan';

% OUTPUT name
nm = 'speed';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_speed, fast, subject, true);

% extract all speed results
all_speed = all_res(:, 1);

% estimate gain hat
m = double(group >= 1) * 2 - 1; % -1 for gain < 1; 1 for gain > 1
all_gain_hat = 1 + (m .* (all_res(:, 2) ./ all_speed));

%% OUTPUT
all_gain_hat_square = all_gain_hat .^ 2;
d = dataset(all_speed, all_gain_hat, all_gain_hat_square);
lm = LinearModel.fit(d, 'all_speed ~ all_gain_hat + all_gain_hat_square');
x = linspace(max(min(all_gain_hat), -1.5), min(max(all_gain_hat),3.5));
plot(all_gain_hat, all_speed, 'bo', x, lm.Coefficients.Estimate(1) + lm.Coefficients.Estimate(2) * x + lm.Coefficients.Estimate(3) * x .^ 2, 'r');
xlabel('Gain estimate (based on speed ratio')
ylabel('Drift speed (arcmin/second)');
utility_set_title('Effect of gain on drift speed', fast, subject);
print(gcf, utility_build_name(nm, 'scatter', fast, subject), '-dpng', '-r300');

[sig_set, p_value] = utility_signifigance(group, all_speed, true);

utility_plot_sem(group, all_speed, utility_sig_idx_to_val(group, sig_set), p_value, 'Mean drift speed (arcmin/sec)');
ylim([0 200]);
utility_set_title('Mean drift speed across trials', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(group, all_speed, sig_set, p_value, 'Drift speed (arcmin/sec)');
ylim([0 375]);
utility_set_title('Mean drift speed across trials', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

for g = sort(unique(group))'
    fprintf('%.1f: %d\n', g, sum(cnt(cnt(:, 1) == g, 2)));
end