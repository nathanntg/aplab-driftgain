function [px_x, px_y] = a2p(arcmin_x, armin_y)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

% arcmin to pix
monitor_distance = 1200; %mm
monitor_width = 390; %mm
monitor_height = 290; %mm
resolution_width = 800; %px
resolution_height = 600; %px

[px_x, px_y] = arcmin2pixel(arcmin_x, armin_y, resolution_width, resolution_height, monitor_width, monitor_height, monitor_distance);

end

