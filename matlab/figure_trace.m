%
load(fullfile('..', 'Raw', '7 Mia - Fast', 'processed.mat'));

gain_a = 1;
gain_b = 0.3;

idx_a = 0;
idx_b = 0;
for i = 1:numel(Data.user)
    if abs(Data.user{i}.DriftGain - gain_a) < 0.01
        idx_a = i;
    end
    if abs(Data.user{i}.DriftGain - gain_b) < 0.01
        idx_b = i;
    end
    if idx_a > 0 && idx_b > 0
        break;
    end
end

[t1x, t1y] = trace(Data, idx_a, 400);
[t2x, t2y] = trace(Data, idx_b, 400);
plot(t1x, t1y, 'b', t2x, t2y, 'r');