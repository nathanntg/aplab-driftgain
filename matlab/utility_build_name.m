function save_name = utility_build_name(nm, type, fast, subject, extension)

% output directory
output_dir = fullfile('~', 'Desktop');

% Optional: fast
% true: fast only; false: slow only; nan: all
if nargin < 3
    fast = [];
end

% Optional: subject
% '' or string
if nargin >= 4 && ~isempty(subject)
    suffix = ['-' lower(subject)];
else
    suffix = '';
end

% Optional: extension
if nargin < 5 || isempty(extension)
    extension = 'png';
end

% fast: subject
if isnan(fast)
    suffix = [suffix ''];
elseif fast
    suffix = [suffix '-fast'];
else
    suffix = [suffix '-slow'];
end

save_name = fullfile(output_dir, [nm '-' type suffix '.' extension]);

end

