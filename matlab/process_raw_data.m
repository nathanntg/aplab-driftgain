trial_name = '12 Patrick - Fast';

% Read the EIS data
List = eis_readData({}, 'x');
List = eis_readData(List, 'y');
List = eis_readData(List, 'trigger', 'blink');
List = eis_readData(List, 'trigger', 'notrack');
List = eis_readData(List, 'trigger', 'em_event1');

List = eis_readData(List, 'uservar', 'SubjectName');
List = eis_readData(List, 'uservar', 'Task');
List = eis_readData(List, 'uservar', 'DriftGain');
List = eis_readData(List, 'uservar', 'DriftAngle');
List = eis_readData(List, 'uservar', 'HRes');
List = eis_readData(List, 'uservar', 'VRes');
List = eis_readData(List, 'uservar', 'RefreshRate');
List = eis_readData(List, 'uservar', 'ImageNames');
List = eis_readData(List, 'uservar', 'FastStabilization');
List = eis_readData(List, 'uservar', 'XShift');
List = eis_readData(List, 'uservar', 'YShift');

List = eis_readData(List, 'stream', 0, 'double');
List = eis_readData(List, 'stream', 1, 'double');
List = eis_readData(List, 'stream', 2, 'int');
List = eis_readData(List, 'stream', 3, 'int');

% Apply the reduction rules (if any)
eis_eisdir2mat(['../Raw/' trial_name '/'], List, ['../Raw/' trial_name '/processed.mat']);
