%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1, 1);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true);

%% MODEL
d = dataset();
d.speed = all_res(:, 2);
d.gain = group;
mdl1 = fitglm(d, 'speed ~ gain + gain^2', 'Distribution', 'gamma');

d = dataset();
d.distance = all_res(group > -0.1 & group < 1.1, 2);
d.gain = group(group > -0.1 & group < 1.1);
mdl1 = fitglm(d, 'distance ~ gain + gain^2', 'Distribution', 'gamma');

utility_plot_sem(group, all_res(:, 2), [], [], 'Drift speed (arcmin/sec)');
ylim([40 100]);
xlim([-0.1 1.1]);
utility_set_title('Instantaneous drift speed across trials', fast, subject);
hold on;
gains = sort(unique(group));
plot(gains, mdl1.predict(gains));
hold off;
%print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

d = dataset();
d.speed = all_res(:, 2);
d.gain = categorical(group);
mdl2 = fitglm(d, 'speed ~ gain', 'Distribution', 'gamma');

utility_plot_sem(group, all_res(:, 2), [], [], 'Drift speed (arcmin/sec)');
ylim([40 200]);
utility_set_title('Instantaneous drift speed across trials', fast, subject);
hold on;
gains = sort(unique(group));
plot(gains, mdl2.predict(categorical(gains)));
hold off;
%print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% make CDF
g = gains(5);
[F, x] = cecdf(all_res(group == g, 2));

% convert to pdf
p = diff(F);

mu = mdl2.predict(categorical(g));
pd = fitdist(all_res(group == g, 2), 'gamma');
plot(x(2:end), p, x(2:end), diff(gamcdf(x, mu / pd.b, pd.b)));

ks(gamcdf(x, mu / pd.b, pd.b), F, sum(group == g), 'Drift instantaneous speed vs gamma distribution');

%% PLOT GAIN >= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot(x(2:end), p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Drift speed distribution; Increased retinal motion');
legend(lbl);
xlim([0 300]);
print(gcf, utility_build_name('distribution-drift', 'faster', fast, subject), '-dpng', '-r300');

%% PLOT GAIN <= 1
clf;
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot(x(2:end), p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Drift speed distribution; Decreased retinal motion');
legend(lbl);
xlim([0 440]);
print(gcf, utility_build_name('distribution-drift', 'slower', fast, subject), '-dpng', '-r300');