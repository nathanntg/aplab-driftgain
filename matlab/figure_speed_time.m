%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% OUTPUT name
nm = 'speed-tm';

% MIN segments
min_segments = 20;
min_duration = 250;

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 10, 1, min_duration + 40);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true);

gains = sort(unique(group));
clf;
for g = gains'
    speed = all_res(group == g, :);
    
    % get last_t (at least 10 data point)
    t_s = sort(unique(speed(:, 1)));
    i = numel(t_s);
    last_t = t_s(i);
    while sum(speed(:, 1) == last_t) < min_segments
        i = i - 1;
        last_t = t_s(i);
    end
    
    % start vectors
    x = zeros(i, 1);
    y = zeros(i, 1);
    y_e = zeros(i, 1);
    
    % combine time
    j = 1;
    for t = t_s(t_s <= last_t)'
        % get speed
        sp = speed(speed(:, 1) == t, 2);
        
        % append
        x(j) = t;
        y(j) = mean(sp);
        y_e(j) = std(sp) / sqrt(numel(sp));
        
        j = j + 1;
    end
    
    % create plot
    plot(x, y, '-b', x, y + y_e, ':r', x, y - y_e, ':r');
    xlim([1 last_t]);
    xlabel('Time (ms)');
    ylabel('Drift speed (arcmin/sec)');
    title(sprintf('Mean speed over drift segment (10ms bins); Gain %.1f', g));
    print(gcf, utility_build_name(nm, sprintf('gain%d', round(g * 10)), fast, subject), '-dpng', '-r300');
end

% all at once
clf;
gains = sort(unique(group));
color = lines(numel(gains));
clr = 1;
lbl = {};
min_x = min_duration;
hold on;
for g = gains'
    speed = all_res(group == g, :);
    
    % get last_t (at least 10 data point)
    t_s = sort(unique(speed(:, 1)));
    i = numel(t_s);
    last_t = t_s(i);
    while sum(speed(:, 1) == last_t) < min_segments
        i = i - 1;
        last_t = t_s(i);
    end
    
    % start vectors
    x = zeros(i, 1);
    y = zeros(i, 1);
    y_e = zeros(i, 1);
    
    % combine time
    j = 1;
    for t = t_s(t_s <= last_t)'
        % get speed
        sp = speed(speed(:, 1) == t, 2);
        
        % append
        x(j) = t;
        y(j) = mean(sp);
        y_e(j) = std(sp) / sqrt(numel(sp));
        
        j = j + 1;
    end
    
    % min x
    min_x = min(min_x, last_t);
    
    % create plot
    plot(x, y, '-', 'Color', color(clr, :));
    xlabel('Time (ms)');
    ylabel('Drift speed (arcmin/sec)');
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
legend(lbl);
xlim([1 min_x]);
title('Mean speed over drift segment (10ms bins)');
print(gcf, utility_build_name(nm, 'all', fast, subject), '-dpng', '-r300');

% all at once
clf;
gains = sort(unique(group));
color = lines(numel(gains));
clr = 1;
lbl = {};
min_x = min_duration;
hold on;
for g = gains(gains < 1.1)'
    speed = all_res(group == g, :);
    
    % get last_t (at least 10 data point)
    t_s = sort(unique(speed(:, 1)));
    i = numel(t_s);
    last_t = t_s(i);
    while sum(speed(:, 1) == last_t) < min_segments
        i = i - 1;
        last_t = t_s(i);
    end
    
    % start vectors
    x = zeros(i, 1);
    y = zeros(i, 1);
    y_e = zeros(i, 1);
    
    % combine time
    j = 1;
    for t = t_s(t_s <= last_t)'
        % get speed
        sp = speed(speed(:, 1) == t, 2);
        
        % append
        x(j) = t;
        y(j) = mean(sp);
        y_e(j) = std(sp) / sqrt(numel(sp));
        
        j = j + 1;
    end
    
    % min x
    min_x = min(min_x, last_t);
    
    % create plot
    plot(x, y, '-', 'Color', color(clr, :));
    xlabel('Time (ms)');
    ylabel('Drift speed (arcmin/sec)');
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
legend(lbl);
xlim([1 min_x]);
ylim([35 185]);
title('Mean speed over drift segment (10ms bins)');
print(gcf, utility_build_name(nm, 'slower', fast, subject), '-dpng', '-r300');

% all at once
clf;
gains = sort(unique(group));
color = lines(numel(gains));
clr = 1;
lbl = {};
min_x = min_duration;
hold on;
for g = gains(gains > 0.9)'
    speed = all_res(group == g, :);
    
    % get last_t (at least 10 data point)
    t_s = sort(unique(speed(:, 1)));
    i = numel(t_s);
    last_t = t_s(i);
    while sum(speed(:, 1) == last_t) < min_segments
        i = i - 1;
        last_t = t_s(i);
    end
    
    % start vectors
    x = zeros(i, 1);
    y = zeros(i, 1);
    y_e = zeros(i, 1);
    
    % combine time
    j = 1;
    for t = t_s(t_s <= last_t)'
        % get speed
        sp = speed(speed(:, 1) == t, 2);
        
        % append
        x(j) = t;
        y(j) = mean(sp);
        y_e(j) = std(sp) / sqrt(numel(sp));
        
        j = j + 1;
    end
    
    % min x
    min_x = min(min_x, last_t);
    
    % create plot
    plot(x, y, '-', 'Color', color(clr, :));
    xlabel('Time (ms)');
    ylabel('Drift speed (arcmin/sec)');
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
legend(lbl);
xlim([1 min_x]);
title('Mean speed over drift segment (10ms bins)');
print(gcf, utility_build_name(nm, 'faster', fast, subject), '-dpng', '-r300');