function immakestimuli(in_file, out_file, desired_width, desired_height)
%IMGETSTIMULI Summary of this function goes here
%   Detailed explanation goes here

% read file and info
imdata = imread(in_file);
height = size(imdata, 1);
width = size(imdata, 2);

% already small enough
if width < desired_width || height < desired_height
    imwrite(imdata, out_file);
    return;
end

% resize first
max_mul = 4;
h_mul = height / desired_height;
w_mul = width / desired_width;
if h_mul > max_mul || w_mul > max_mul
    if h_mul > w_mul
        imdata = imresize(imdata, [nan max_mul * desired_height]);
    else
        imdata = imresize(imdata, [max_mul * desired_width nan]);
    end
    height = size(imdata, 1);
    width = size(imdata, 2);
end

% get offset
x = unidrnd(width - desired_width, 1, 1);
y = unidrnd(height - desired_height, 1, 1);

% new image
new_imdata = imcrop(imdata, [x y desired_width - 1 desired_height - 1]);

% imshow(new_imdata);
imwrite(new_imdata, out_file);

end

