if ~exist('idx', 'var')
    idx = 5;
end

% plot indices for Martina
% 4: stabilized (0x gain)
% 5: move opposing egain (2x gain)
% 3: control (1x gain)
% 6: 1.5x gain
% 7: extreme (3x gain)
% 10: edge detection

clf;

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(round(Data.stream00{idx}.data), round(Data.stream01{idx}.data));

% state changes
states = split_states(Result.samples, Data.stream02{idx});

sum(adj_x)

%% start building the plot
% start building plot
subplot(2,1,1);
analyze_subplot(eye_x, adj_t, adj_x, states); %, Data.triggers{idx}, Data.stream02{idx});
%title(sprintf('Horizontal Movement; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));

subplot(2,1,2);
analyze_subplot(eye_y, adj_t, adj_y, states); %, Data.triggers{idx}, Data.stream02{idx});
%title(sprintf('Vertical Movement; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));