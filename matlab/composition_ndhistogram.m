set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Dylan';

% OUTPUT -- name
nm = 'ndhist';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_pos_relative, fast, subject, true);

%% OUTPUT
gains = sort(unique(group));
clf;
for g = gains'
    pos = all_res(group == g, :);
    ndhist(pos(:, 1), pos(:, 2), 'axis', 50);
    title(sprintf('Eye position during stabilization; Gain %.1f', g));
    print(gcf, utility_build_name(nm, sprintf('gain%d', round(g * 10)), fast, subject), '-dpng', '-r300');
end
