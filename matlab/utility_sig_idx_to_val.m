function new_sig_set = utility_sig_idx_to_val(group, sig_set)
%UNTITLED9 Summary of this function goes here
%   Detailed explanation goes here

gains = sort(unique(group));
new_sig_set = cell(1, numel(sig_set));

for j = 1:numel(sig_set)
    new_sig_set{j} = gains(sig_set{j});
end

end

