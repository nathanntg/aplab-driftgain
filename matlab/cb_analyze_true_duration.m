function [res, n] = cb_analyze_true_duration(Data, idx, cv, convert_p2a)

% build result
eye_x = Data.x{idx};
eye_y = Data.y{idx};
Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% extract adj  (in pixel)
adj_t = double(Data.stream00{idx}.ts);
if nargin >= 4 && convert_p2a
    [adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);
else
    adj_x = Data.stream00{idx}.data;
    adj_y = Data.stream01{idx}.data;
end

% full adj
full_adj_x = zeros(size(eye_x));
full_adj_y = zeros(size(eye_y));

maxi = size(adj_t, 2);
cum_adj_x = cumsum(adj_x);
cum_adj_y = cumsum(adj_y);
for i = 1:maxi
    % get time range
    from = adj_t(i);
    if i < maxi
        through = adj_t(i + 1) - 1;
    else
        through = size(eye_x, 2);
    end
    
    % set time range
    full_adj_x(from:through) = cum_adj_x(i);
    full_adj_y(from:through) = cum_adj_y(i);
end

% state changes
states = split_states2(Result, Data.stream02{idx});
events = joinEvents(states.toofast, states.stabilize);

% min_duration
min_duration = 100;

%% extract matching drifts
res = [];
n = 0;
for i = 1:size(events.start, 2)
    % get timing
    strt = events.start(i);
    dur = events.duration(i);
    
    % enforce minimum duration
    if dur < min_duration
        continue;
    end
    
    % get movement
    to_add = dur;
    
    res = [res; to_add];
    
    % increment n
    if 0 < numel(to_add)
        n = n + 1;
    end
    
end

%n = size(res, 1);

end

