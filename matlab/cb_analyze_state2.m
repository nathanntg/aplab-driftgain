function [res, n] = cb_analyze_state2(Data, idx, cb, convert_p2a)

% build result
eye_x = Data.x{idx};
eye_y = Data.y{idx};
Result = processTrial(idx, eye_x, eye_y, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% extract adj  (in pixel)
adj_t = double(Data.stream00{idx}.ts);
if nargin >= 4 && convert_p2a
    [adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);
else
    adj_x = Data.stream00{idx}.data;
    adj_y = Data.stream01{idx}.data;
end

% full adj
full_adj_x = zeros(size(eye_x));
full_adj_y = zeros(size(eye_y));

maxi = size(adj_t, 2);
cum_adj_x = cumsum(adj_x);
cum_adj_y = cumsum(adj_y);
for i = 1:maxi
    % get time range
    from = adj_t(i);
    if i < maxi
        through = adj_t(i + 1) - 1;
    else
        through = size(eye_x, 2);
    end
    
    % set time range
    full_adj_x(from:through) = cum_adj_x(i);
    full_adj_y(from:through) = cum_adj_y(i);
end

% state changes
states = split_states2(Result, Data.stream02{idx});

% min_duration
min_duration = 100;

% consider first
consider = 50; % 50ms

%% extract matching drifts
res = [];
n = 0;
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    
     % only if proceeded by a saccade or microsaccade
     dpos = isIncludedIn(strt, Result.drifts);
     if 0 < dpos
         if 0 == isIncludedIn(Result.drifts.start(dpos) - 1, Result.saccades)
             continue;
         end
     else
         continue;
     end
    
    % enforce minimum duration
    if dur < min_duration
        continue;
    end
    
    % current excerpts
    cur_eye_x = double(Result.x.position(strt:(strt + dur - 1)));
    cur_eye_y = double(Result.y.position(strt:(strt + dur - 1)));
    cur_adj_x = double(full_adj_x(strt:(strt + dur - 1)));
    cur_adj_y = double(full_adj_y(strt:(strt + dur - 1)));
    
    % get movement
    new_res = cb(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur);
    
    % exclude nan
    to_add = new_res(~any(isnan(new_res), 2), :);
    
    % switch it around
    rows = to_add(:, 1) <= consider;
    to_add = [strt + to_add(rows, 1) to_add(rows, 1) to_add(rows, 2)];
    
    res = [res; to_add];
    
    % increment n
    if 0 < numel(to_add)
        n = n + 1;
    end
    
end

%n = size(res, 1);

end

