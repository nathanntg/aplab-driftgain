%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_brownian, fast, subject);

%% OUTPUT
% no bias
nm = 'brownian1';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 1), true);

utility_plot_sem(group, all_res(:, 1), utility_sig_idx_to_val(group, sig_set), p_value, 'Diffusion coefficient');
utility_set_title('Diffusion coefficient (assuming no bias)', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(group, all_res(:, 1), sig_set, p_value, 'Curvature');
utility_set_title('Diffusion coefficient (assuming no bias)', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% bias
nm = 'brownian2';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 2), true);

utility_plot_sem(group, all_res(:, 2), utility_sig_idx_to_val(group, sig_set), p_value, 'Diffusion coefficient');
utility_set_title('Diffusion coefficient (Gaussian)', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(group, all_res(:, 2), sig_set, p_value, 'Curvature');
utility_set_title('Diffusion coefficient (Gaussian)', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% bias corrected
nm = 'brownian3';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 3), true);

utility_plot_sem(group, all_res(:, 3), utility_sig_idx_to_val(group, sig_set), p_value, 'Diffusion coefficient');
utility_set_title('Diffusion coefficient (remove bias)', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(group, all_res(:, 3), sig_set, p_value, 'Curvature');
utility_set_title('Diffusion coefficient (remove bias)', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

% bias
nm = 'brownian-bias';
[sig_set, p_value] = utility_signifigance(group, all_res(:, 4), true);

utility_plot_sem(group, all_res(:,4), utility_sig_idx_to_val(group, sig_set), p_value, 'Diffusion bias');
utility_set_title('Diffusion mean bias', fast, subject);
print(gcf, utility_build_name(nm, 'sem', fast, subject), '-dpng', '-r300');

utility_plot_box(group, all_res(:, 4), sig_set, p_value, 'Curvature');
utility_set_title('Drift coefficient', fast, subject);
print(gcf, utility_build_name(nm, 'boxplot', fast, subject), '-dpng', '-r300');

