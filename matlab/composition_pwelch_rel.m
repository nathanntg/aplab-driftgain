set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = 'Dylan';

% OUTPUT NAME
nm = 'pwelch-rel';

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(@cb_pwelch_rel, fast, subject, true);

%% OUTPUT
% gains
gains = sort(unique(group));

% columns
c = size(all_res, 2) / 3;

freq = all_res(end, c * 2 + 1:c * 3);

% all at once
% SLOWER, HORIZONTAL
clf;
color = lines(6);
clr = 1;
lbl = {};
h = [];
for g = gains(gains < 1.1)'
    rows = (group == g);
    if 0 == sum(rows)
        continue;
    end
    h1 = semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 0 + 1:c * 1))), '.-', 'MarkerSize', 25, 'Color', color(clr, :));
    h = [h h1(1)];
    lbl{end + 1} = sprintf('Gain = %.1f (n = %d)', g, sum(rows));
    if clr == 1
        hold on;
    end
    clr = clr + 1;
end
hold off;
xlim([freq(1) 200]);
legend(h, lbl);
title('Horizontal spectrum of retinal motion; Decreased retinal motion');
print(gcf, utility_build_name('pwelch-rel-hor', 'slower', fast, subject), '-dpng', '-r300');

% SLOWER, VERTICAL
clf;
color = lines(6);
clr = 1;
lbl = {};
h = [];
for g = gains(gains < 1.1)'
    rows = (group == g);
    if 0 == sum(rows)
        continue;
    end
    h1 = semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 1 + 1:c * 2))), '.-', 'MarkerSize', 25, 'Color', color(clr, :));
    h = [h h1(1)];
    lbl{end + 1} = sprintf('Gain = %.1f (n = %d)', g, sum(rows));
    if clr == 1
        hold on;
    end
    clr = clr + 1;
end
hold off;
xlim([freq(1) 200]);
legend(h, lbl);
title('Vertical spectrum of retinal motion; Decreased retinal motion');
print(gcf, utility_build_name('pwelch-rel-ver', 'slower', fast, subject), '-dpng', '-r300');

% FASTER, HORIZONTAL
clf;
color = lines(6);
clr = 1;
lbl = {};
h = [];
for g = gains(gains > 0.9)'
    rows = (group == g);
    if 0 == sum(rows)
        continue;
    end
    h1 = semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 0 + 1:c * 1))), '.-', 'MarkerSize', 25, 'Color', color(clr, :));
    h = [h h1(1)];
    lbl{end + 1} = sprintf('Gain = %.1f (n = %d)', g, sum(rows));
    if clr == 1
        hold on;
    end
    clr = clr + 1;
end
hold off;
xlim([freq(1) 200]);
legend(h, lbl);
title('Horizontal spectrum of retinal motion; Increased retinal motion');
print(gcf, utility_build_name('pwelch-rel-hor', 'faster', fast, subject), '-dpng', '-r300');

% FASTER, VERTICAL
clf;
color = lines(6);
clr = 1;
lbl = {};
h = [];
for g = gains(gains > 0.9)'
    rows = (group == g);
    if 0 == sum(rows)
        continue;
    end
    h1 = semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 1 + 1:c * 2))), '.-', 'MarkerSize', 25, 'Color', color(clr, :));
    h = [h h1(1)];
    lbl{end + 1} = sprintf('Gain = %.1f (n = %d)', g, sum(rows));
    if clr == 1
        hold on;
    end
    clr = clr + 1;
end
hold off;
xlim([freq(1) 200]);
legend(h, lbl);
title('Vertical spectrum of retinal motion; Increased retinal motion');
print(gcf, utility_build_name('pwelch-rel-ver', 'faster', fast, subject), '-dpng', '-r300');

% separate plots
for g = gains'
    rows = (group == g);
    subplot(2, 1, 1);
    %plot(10*log10(all_res(rows, c * 2 + 1:c * 3)), 10*log10(mean(all_res(rows, c * 0 + 1:c * 1))));
    semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 0 + 1:c * 1))));
    xlim([7 max(freq)]);
    ylim([-50 10]);
    title(sprintf('Horizontal retinal motion; gain = %.1f (n = %d)', g, sum(rows)));
    subplot(2, 1, 2);
    %plot(10*log10(all_res(rows, c * 2 + 1:c * 3)), 10*log10(mean(all_res(rows, c * 1 + 1:c * 2))));
    semilogx(all_res(rows, c * 2 + 1:c * 3), 10*log10(mean(all_res(rows, c * 1 + 1:c * 2))));
    xlim([7 max(freq)]);
    ylim([-50 10]);
    title(sprintf('Vertical retinal motion; gain = %.1f (n = %d)', g, sum(rows)));
    
    % print
    print(gcf, utility_build_name(nm, sprintf('gain%d', round(g * 10)), fast, subject), '-dpng', '-r300');
end