function ret = cb_distance_eye(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, CS, par)

% minimum duration
if dur < 100
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 7
	par = 41;
end
if nargin < 6
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

%% EYE MOVEMENT
% apply filter, if specified
if ~isempty(par)
    cur_eye_x = sgolayfilt(cur_eye_x, 3, par);
    cur_eye_y = sgolayfilt(cur_eye_y, 3, par);
end

ret = sqrt((cur_eye_x(end-CS) - cur_eye_x(CS)) ^ 2 + (cur_eye_y(end-CS) - cur_eye_y(CS)) ^ 2);

end

