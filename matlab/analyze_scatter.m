idx = 15;

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);
indices = 1:max(size(adj_t));

% state changes
states = split_states(Result.samples, Data.stream02{idx});

%% extract matching drifts
d_mov = [];
d_adj = [];
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    stop = strt + dur - 1;
    
    % get stabilized
    [stab_x, stab_y] = stabilize(eye_x(strt:stop), eye_y(strt:stop));
    
%     if dur > 200
%     vel_x = sgolayfilt(double(eye_x(strt:stop)), 3, 41);
%     vel_y = sgolayfilt(double(eye_y(strt:stop)), 3, 41);
%     subplot(2, 1, 1);
%     plot(strt:stop, eye_x(strt:stop), strt:stop, stab_x, strt:stop, vel_x);
%     legend('Actual', 'Stabilized', 'Smoothed');
%     subplot(2, 1, 2);
%     plot(strt:stop, eye_y(strt:stop), strt:stop, stab_y, strt:stop, vel_y);
%     legend('Actual', 'Stabilized', 'Smoothed');
%     pause;
%     end

    eye_move = sqrt((stab_x(end) - stab_x(1)) ^ 2 + (stab_y(end) - stab_y(1)) ^  2);
    img_move = sqrt(sum(adj_x(adj_t >= strt & adj_t <= stop)) ^ 2 + sum(adj_y(adj_t >= strt & adj_t <= stop)) ^ 2);
    
%     since = strt;
%     eye_move = [];
%     img_move = [];
%     for j = indices(adj_t >= strt & adj_t <= stop)
%         t = adj_t(j);
%         
%         % eye movement
%         eye_move(end + 1) = sqrt((stab_x(1 + t - strt) - stab_x(1 + since - strt)) ^ 2 + (stab_y(1 + t - strt) - stab_y(1 + since - strt)) ^ 2);
%         
%         % image movement
%         img_move(end + 1) = sqrt(adj_x(j) ^ 2 + adj_y(j) ^ 2);
%     end
%     
%     eye_move = mean(eye_move);
%     img_move = mean(img_move);
    
    d_mov(end + 1) = eye_move;
    d_adj(end + 1) = img_move;    
end

% create scatter plot
scatter(d_mov, d_adj);
title(sprintf('Comparison; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));
xlabel('Eye movement');
ylabel('Image adjustment');

% set axis
x_min = min(min(d_mov), min(d_adj));
x_max = max(max(d_mov), max(d_adj));
xlim([0 x_max]);
ylim([0 x_max]);
axis square;

% figure out fit
%fit = [d_mov ones(size(d_mov))] \ d_adj;
fit = d_mov' \ d_adj';
hold on;
x = x_min:((x_max - x_min) / 10):x_max;
plot(x, fit(1) * x, 'r:');

% figure out desired
plot(x, x * abs(Data.user{idx}.DriftGain - 1), 'k--');

hold off;

legend('Velocity', sprintf('Slope: %.3f\n', fit(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain - 1)), 'Location', 'NorthWest');
