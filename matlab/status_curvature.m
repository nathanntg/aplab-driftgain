% directories
dirs = {'7 Mia - Fast' '9 Dylan - Fast' '12 Patrick - Fast'};

% speed tables
curvature = {};
gains = [];

% for each directory
for col = 1:numel(dirs)
    load(fullfile('..', 'Raw', dirs{col}, 'processed.mat'));
    
    % for each session
    for idx = 1:numel(Data.x)
        % fast stabilization only
        if ~Data.user{idx}.FastStabilization
            continue;
        end
        
        % find gain
        gain = Data.user{idx}.DriftGain;
        
        % find row
        row = find(gains == gain);
        if isempty(row)
            gains = [gains gain];
            row = numel(gains);
        end
        
        % get speed
        [res, ~] = utility_analyze(Data, idx, @cb_curvature, true);
        
        % add it up
        if all([row col] <= size(curvature))
            curvature{row, col} = [curvature{row, col}; res(:, 1)];
        else
            curvature{row, col} = res(:, 1);
        end
    end
end

curvature_mean = nan(size(curvature));
curvature_std = nan(size(curvature));

sorted_gains = sort(gains);
for new_row = 1:numel(sorted_gains)
    gain = sorted_gains(new_row);
    old_row = find(gains == gain);
    
    for col = 1:numel(dirs)
        curvature_mean(new_row, col) = mean(curvature{old_row, col});
        curvature_std(new_row, col) = std(curvature{old_row, col});
    end
end

% combine the two
combined = [];
for col = 1:size(curvature_mean, 2)
	combined = [combined curvature_mean(:, col)];
	combined = [combined curvature_std(:, col)];
end