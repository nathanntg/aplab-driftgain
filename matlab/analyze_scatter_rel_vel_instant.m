if ~exist('idx', 'var')
    idx = 2;
end

% plot for Martina
% 4: stabilized (0x gain)
% 5: move opposing egain (2x gain)
% 3: control (1x gain)
% 6: 1.5x gain
% 7: extreme (3x gain)
% 10: edge detection

% min duration
min_duration = 100;

%% prepare data
% build result
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);

% eye position
% in arcmin
eye_x = Data.x{idx};
eye_y = Data.y{idx};

% [eye_x, eye_y] = a2p(Data.x{idx}, Data.y{idx});
% Result.x.position = eye_x;
% Result.y.position = eye_y;

% extract adj in arcmin
adj_t = double(Data.stream00{idx}.ts);
[adj_x, adj_y] = p2a(Data.stream00{idx}.data, Data.stream01{idx}.data);
% adj_x = Data.stream00{idx}.data;
% adj_y = Data.stream01{idx}.data;

full_adj_x = zeros(size(eye_x));
full_adj_y = zeros(size(eye_y));

maxi = size(adj_t, 2);
cum_adj_x = cumsum(adj_x);
cum_adj_y = cumsum(adj_y);
for i = 1:maxi
    % get time range
    from = adj_t(i);
    if i < maxi
        through = adj_t(i + 1) - 1;
    else
        through = size(eye_x, 2);
    end
    
    % set time range
    full_adj_x(from:through) = cum_adj_x(i);
    full_adj_y(from:through) = cum_adj_y(i);
end

% alternative result with image movements
AdjResult = Result;
AdjResult.x.position = (Result.x.position - full_adj_x);
AdjResult.y.position = (Result.y.position - full_adj_y);

% state changes
states = split_states(Result.samples, Data.stream02{idx});

%% extract matching drifts
n = 0;
d_mov = [];
d_adj = [];
mask_size = 300;
mask = size(mask_size, 1) / mask_size;
for i = 1:size(states.stabilize.start, 2)
    % get timing
    strt = states.stabilize.start(i);
    dur = states.stabilize.duration(i);
    
    % enforce minimum duration
    if dur < min_duration
        continue
    end
    
    % get movement
    mov = calculate_instant_velocity(Result, strt, dur);
    adj = calculate_instant_velocity(AdjResult, strt, dur);
    
    % unable to calculate velocity?
    if isnan(mov)
        continue
    end
    if isnan(adj)
        continue
    end
    
%     % debug weird values
%     if sum(mov - 50 > adj)
%         subplot(3,1,1);
%         plot(strt:(strt+dur), AdjResult.x.position(strt:(strt+dur)) - mean(AdjResult.x.position(strt:(strt+dur))), strt:(strt+dur), AdjResult.y.position(strt:(strt+dur)) - mean(AdjResult.y.position(strt:(strt+dur))));
%         legend('X', 'Y');
%         subplot(3,1,2);
%         plot(strt:(strt+dur), Result.x.position(strt:(strt+dur)) - mean(Result.x.position(strt:(strt+dur))), strt:(strt+dur), Result.y.position(strt:(strt+dur)) - mean(Result.y.position(strt:(strt+dur))));
%         legend('X', 'Y');
%         subplot(3,1,3);
%         plot(1:max(size(mov)), mov, 1:max(size(adj)), adj);
%         legend('Abs', 'Rel');
%         pause;
%     end
    
    for j = 1:mask_size:size(mov, 2)
        d_mov = [d_mov; mean(mov(j:min(j + mask_size, size(mov, 2))))];
    end
    for j = 1:mask_size:size(adj, 2)
        d_adj = [d_adj; mean(adj(j:min(j + mask_size, size(adj, 2))))];
    end
    
    n = n + 1;
    
    %d_mov = [d_mov; conv(mov, mask, 'valid')'];
    %d_adj = [d_adj; conv(adj, mask, 'valid')'];
end

% only plot if...
if ~exist('make_plot', 'var') || make_plot
    % create scatter plot
    scatter(d_mov, d_adj);
    title(sprintf('Comparison; gain x%.1f; angle %.1f', Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle));
    xlabel('Eye position velocity');
    ylabel('Perceived image velocity');

    % set axis
    x_min = min(min(d_mov), min(d_adj));
    x_max = max(max(d_mov), max(d_adj));
    xlim([0 x_max]);
    ylim([0 x_max]);
    axis square;

    % figure out fit
    d = dataset(d_mov, d_adj);
    mdl = LinearModel.fit(d, 'd_adj ~ d_mov - 1');
    hold on;
    x = x_min:((x_max - x_min) / 10):x_max;
    plot(x, mdl.Coefficients.Estimate(1) * x, 'r:');

    % figure out desired
    plot(x, x * abs(Data.user{idx}.DriftGain), 'k--');

    hold off;

    legend('Velocity', sprintf('Slope: %.3f\n', mdl.Coefficients.Estimate(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain)), 'Location', 'NorthWest');
    %legend('Velocity', sprintf('Slope: %.3f\n', fit(1)), sprintf('Expected: %.3f\n', abs(Data.user{idx}.DriftGain)), 'Location', 'NorthWest');
end
