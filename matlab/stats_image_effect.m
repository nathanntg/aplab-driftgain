rows = all_res(:, 3) > 1 & all_res(:, 3) < 5;
d = dataset();
d.gain = group(rows);
d.speed = all_res(rows, 1);
d.image = categorical(all_res(rows, 3));
lm1 = LinearModel.fit(d, 'speed ~ gain + image');
anova(lm1)
% 1 - fcdf(1.6214, 2, lm1.DFE)

%% ADD TO UTILITY ANALYZE:
% to_add = [to_add ones(size(to_add, 1), 1) + floor(strt / per_image)];