function ret = cb_rel_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, tm, CS, min_dur, par)

% default min duration
if nargin < 8
    min_dur = 100;
end

% minimum duration
if dur < min_dur
    ret = nan(1, 2);
    return;
end

% default values
if nargin < 9
	par = 41;
end
if nargin < 7
    if isempty(par)
        CS = 1;
    else
        CS = round(par / 2);
    end
end

% time blocks
if nargin < 6
    tm = 20; % ms
end

%% RELATIVE SPEED

rel_x = cur_eye_x - cur_adj_x;
rel_y = cur_eye_y - cur_adj_y;

% apply filter, if specified
if ~isempty(par)
    rel_x = sgolayfilt(rel_x, 3, par);
    rel_y = sgolayfilt(rel_y, 3, par);
    
    % trim noise
    rel_x = rel_x(CS:end + 1 - CS);
    rel_y = rel_y(CS:end + 1 - CS);
end

if false && any(cur_adj_x > 0)
    subplot(2, 2, 1);
    plot(1:numel(cur_eye_x), cur_eye_x - cur_eye_x(1), 1:numel(cur_adj_x), cur_adj_x - cur_adj_x(1));
    xlim([1 numel(cur_eye_x)]);
    title('Horizontal');
    legend('Eye', 'Image', 'Location', 'NorthEast');
    subplot(2, 2, 2);
    plot(1:numel(rel_x), cur_eye_x - cur_adj_x - cur_eye_x(1) + cur_adj_x(1), 1:numel(rel_x), rel_x - rel_x(1));
    xlim([1 numel(rel_x)]);
    title('Horizontal');
    legend('Relative', 'Relative (smoothed)', 'Location', 'NorthEast');
    subplot(2, 2, 3);
    plot(1:numel(cur_eye_x), cur_eye_x - cur_eye_x(1), 1:numel(cur_adj_x), cur_adj_x - cur_adj_x(1));
    xlim([1 numel(cur_eye_x)]);
    title('Vertical');
    legend('Eye', 'Image', 'Location', 'NorthEast');
    subplot(2, 2, 4);
    plot(1:numel(rel_y), cur_eye_y - cur_adj_y - cur_eye_y(1) + cur_adj_y(1), 1:numel(rel_y), rel_y - rel_y(1));
    xlim([1 numel(rel_y)]);
    title('Vertical');
    legend('Relative', 'Relative (smoothed)', 'Location', 'NorthEast');
    pause;
end

% speed vector
diff_x = rel_x(1 + tm:end) - rel_x(1:end - tm);
diff_y = rel_y(1 + tm:end) - rel_y(1:end - tm);

spd = sqrt(diff_x .^ 2 + diff_y .^ 2) * 1000 / tm;

ret = [(1:numel(spd))' spd'];

end

