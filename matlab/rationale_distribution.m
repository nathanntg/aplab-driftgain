set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultLineLineWidth', 3);
set(0, 'DefaultAxesFontSize', 16);

%% PARAMETERS
% FAST -- true: fast only; false: slow only; nan: all
fast = true;

% SUBJECT -- '' or name
subject = '';

% custom cb
cb = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1, 1);

%% EXECUTION
[group, all_res, cnt] = utility_build_comparison(cb, fast, subject, true);
[group2, all_res2, cnt2] = utility_build_comparison(@cb_speed, fast, subject, true);

%% PLOT GAIN >= 1
subplot(2, 1, 1);
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains > 0.9 & gains < 3.5)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Increased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains > 0.9 & gains < 3.5)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 200]);
xlabel('Drift speed (arcmin/sec)');
ylabel('Proportion');

%% PLOT GAIN <= 1
subplot(2, 1, 2);
gains = sort(unique(group));
hold on;
lbl = {};
clr = 1;
color = lines(6);
for g = gains(gains < 1.1 & 0.01 < gains)'
    % make CDF
    [F, x] = cecdf(all_res(group == g, 2));
    
    % convert to pdf
    p = diff(F);
    
    plot((x(2:end) + x(1:end - 1)) / 2, p, 'Color', color(clr, :));
    
    % add legend and advance color
    lbl{end + 1} = sprintf('Gain %.1f', g);
    clr = clr + 1;
end
hold off;
title('Decreased retinal motion');
legend(lbl);
clr = 1;
for g = gains(gains < 1.1 & 0.01 < gains)' 
    % mean
    mean_x = mean(all_res2(group2 == g, 1));
    line([mean_x mean_x], [0.1 * range(ylim) 0], 'Color', color(clr, :), 'LineWidth', 4);
    clr = clr + 1;
end
xlim([0 200]);
xlabel('Drift speed (arcmin/sec)');
ylabel('Proportion');

print(gcf, utility_build_name('distribution-drift', 'slower', fast, subject), '-dpng', '-r300');