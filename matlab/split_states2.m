function states = split_states2(Result, stream)
%SPLIT_STATES Summary of this function goes here
%   Detailed explanation goes here

% stabilization modes
state_val = stream.data;
state_tm = stream.ts;

% states
states = {};

% analysis_period
analysis_period = Result.analysis.start:(Result.analysis.start + Result.analysis.duration - 1);

% state 5: outside of analysis
state = 5 * ones(1, Result.samples);
state(analysis_period) = 0;

% full time
last_stabilize = false;
for t = 1:max(size(state_tm))
    % stabilization period (default mode, nothing needs to change)
    if state_val(t) == 0
        last_stabilize = true;
		continue;
    end
	
    % start time of current state
	from = state_tm(t);
    
    % if last mode was stabilization, then start excluding from right when
    % it ended
    if last_stabilize
        from = state_tm(t - 1) + 1;
        last_stabilize = false;
    end
    
    % end time of current state
    if t >= size(state_tm, 2)
        to = Result.samples;
    else
        to = state_tm(t + 1) - 1;
    end
    
	state(from:to) = state_val(t);
end

% stabilize
states.stabilize = intersectEvents(vector2events(state == 0), Result.drifts);
% eye move trigger
states.eyemovement = vector2events(state == 1);
% moving too fast
states.toofast = vector2events(state == 2);
% edge
states.edge = vector2events(state == 4);
% other
all = joinEvents(states.stabilize, states.eyemovement);
all = joinEvents(all, states.toofast);
all = joinEvents(all, states.edge);
% exclude outside analysis
all = joinEvents(all, vector2events(state == 5));
states.other = invertEvents(all, Result.samples);

end

