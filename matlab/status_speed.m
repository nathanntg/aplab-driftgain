% directories
dirs = {'7 Mia - Fast' '9 Dylan - Fast' '12 Patrick - Fast'};

% speed tables
speed = {};
speed_ins = {};
gains = [];

% custom cb
cb_instant = @(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur) cb_speed_instant(cur_eye_x, cur_eye_y, cur_adj_x, cur_adj_y, dur, 1);

% for each directory
for col = 1:numel(dirs)
    load(fullfile('..', 'Raw', dirs{col}, 'processed.mat'));
    
    % for each session
    for idx = 1:numel(Data.x)
        % fast stabilization only
        if ~Data.user{idx}.FastStabilization
            continue;
        end
        
        % find gain
        gain = Data.user{idx}.DriftGain;
        
        % find row
        row = find(gains == gain);
        if isempty(row)
            gains = [gains gain];
            row = numel(gains);
        end
        
        % get speed
        [res, ~] = utility_analyze(Data, idx, @cb_speed, true);
        
        % add it up
        if all([row col] <= size(speed))
            speed{row, col} = [speed{row, col}; res(:, 1)];
        else
            speed{row, col} = res(:, 1);
        end
        
        % get instant speed
        [res, ~] = utility_analyze(Data, idx, cb_instant, true);
        
        % add it up
        if all([row col] <= size(speed_ins))
            speed_ins{row, col} = [speed_ins{row, col}; res(:, 2)];
        else
            speed_ins{row, col} = res(:, 2);
        end
    end
end

speed_mean = nan(size(speed));
speed_std = nan(size(speed));
speed_ins_mean = nan(size(speed_ins));
speed_ins_std = nan(size(speed_ins));

sorted_gains = sort(gains);
for new_row = 1:numel(sorted_gains)
    gain = sorted_gains(new_row);
    old_row = find(gains == gain);
    
    for col = 1:numel(dirs)
        speed_mean(new_row, col) = mean(speed{old_row, col});
        speed_std(new_row, col) = std(speed{old_row, col});
        speed_ins_mean(new_row, col) = mean(speed_ins{old_row, col});
        speed_ins_std(new_row, col) = std(speed_ins{old_row, col});
    end
end
