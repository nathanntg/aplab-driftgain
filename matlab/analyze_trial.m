load('../Analysis/processed.mat');

idx = 2;

% plot raw image movement
subplot(2,1,1); stairs(Data.stream00{idx}.ts, cumsum(Data.stream00{idx}.data));
subplot(2,1,2); stairs(Data.stream01{idx}.ts, cumsum(Data.stream01{idx}.data), 'r');

% build EMAT trial
Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);
displayTrial(Result);
pause;
displayEM(Result);
pause;

% processed index
for idx = 1:6
    % build EMAT trial
    Result = processTrial(idx, Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack);
    
    % in pixels
    % extract eye position
    [eye_x, eye_y] = a2p(Data.x{idx}, Data.y{idx});
    
    % in arc/min
    eye_x = Data.x{idx};
    eye_y = Data.y{idx};
    
    % extract adj_x
    adj_t = double(Data.stream00{idx}.ts);
    adj_x = Data.stream00{idx}.data;
    adj_y = Data.stream01{idx}.data;

    %% plot drifts alone
    drift_t = [];
    drift_y = [];
    drift_x = [];
    for i = 1:size(Result.drifts.start, 2)
        start = Result.drifts.start(i);
        dur = Result.drifts.duration(i);

        ts = start:(start + dur - 1);
        drift_t = [drift_t ts nan];
        drift_x = [drift_x eye_x(ts) nan];
        drift_y = [drift_y eye_y(ts) nan];
    end
    subplot(2,1,1);
    stairs(drift_t, drift_x);
    title(sprintf('Horizontal Drift; gain x%.1f', Data.user{idx}.DriftGain));
    xlabel('t');
    ylabel('x');
    xlim([1 size(eye_x, 2)]);

    subplot(2,1,2);
    plot(drift_t, drift_y, 'r');
    title(sprintf('Vertical Drift; gain x%.1f', Data.user{idx}.DriftGain));
    xlabel('t');
    ylabel('y');
    xlim([1 size(eye_x, 2)]);

    print(gcf, [int2str(idx) '-drift.png'], '-dpng', '-r300');

    %% plot relative drifts
%     [sim_adj_t, sim_adj_x, sim_adj_y] = analyze_simulate(Data.x{idx}, Data.y{idx}, Data.triggers{idx}.blink, Data.triggers{idx}.notrack, Data.triggers{idx}.em_event1, Data.user{idx}.DriftGain, Data.user{idx}.DriftAngle);
    
    drift_t = [];
    drift_x = [];
    drift_y = [];
    alt_t = [];
    alt_x = [];
    alt_y = [];
%     sim_alt_t = [];
%     sim_alt_x = [];
%     sim_alt_y = [];
    for i = 1:size(Result.drifts.start, 2)
        start = Result.drifts.start(i);
        dur = Result.drifts.duration(i);

        ts = start:(start + dur - 1);
        drift_t = [drift_t ts nan];
        drift_x = [drift_x (eye_x(ts) - eye_x(start)) nan];
        drift_y = [drift_y (eye_y(ts) - eye_y(start)) nan];

        % make relative alt
        tt = (adj_t >= start & adj_t < start + dur);
        alt_t = [alt_t double(adj_t(tt)) nan];
        alt_x = [alt_x cumsum(adj_x(tt)) nan];
        alt_y = [alt_y cumsum(adj_y(tt)) nan];

%         % make simulated relative alt
%         tt = (adj_t >= start & adj_t < start + dur);
%         sim_alt_t = [sim_alt_t double(adj_t(tt)) nan];
%         sim_alt_x = [sim_alt_x cumsum(adj_x(tt)) nan];
%         sim_alt_y = [sim_alt_y cumsum(adj_y(tt)) nan];
    end
    subplot(2,1,1);
    plot(drift_t, drift_x, 'b', alt_t, alt_x, 'g');
    title(sprintf('Horizontal Movement; gain x%.1f', Data.user{idx}.DriftGain));
    legend('Relative Drift', 'Relative Image Adjustment', 'Location', 'NorthWest');
    xlabel('t');
    ylabel('x');
    xlim([1 size(eye_x, 2)]);

    subplot(2,1,2);
    plot(drift_t, drift_y, 'r', alt_t, alt_y, 'c');
    title(sprintf('Vertical Movement; gain x%.1f', Data.user{idx}.DriftGain));
    legend('Relative Drift', 'Relative Image Adjustment', 'Location', 'NorthWest');
    xlabel('t');
    ylabel('y');
    xlim([1 size(eye_x, 2)]);

    print(gcf, [int2str(idx) '-img.png'], '-dpng', '-r300');
    
%     %% simulate adjustments
%     subplot(2,1,1);
%     plot(drift_t, drift_x, 'b', alt_t, alt_x, 'g', sim_alt_t, sim_alt_x, 'g:');
%     title(sprintf('Horizontal Movement; gain x%.1f', Data.user{idx}.DriftGain));
%     legend('Relative Drift', 'Relative Image Adjustment', 'Location', 'NorthWest');
%     xlabel('t');
%     ylabel('x');
%     xlim([1 size(eye_x, 2)]);
% 
%     subplot(2,1,2);
%     plot(drift_t, drift_y, 'r', alt_t, alt_y, 'c', sim_alt_t, sim_alt_y, 'c:');
%     title(sprintf('Vertical Movement; gain x%.1f', Data.user{idx}.DriftGain));
%     legend('Relative Drift', 'Relative Image Adjustment', 'Location', 'NorthWest');
%     xlabel('t');
%     ylabel('y');
%     xlim([1 size(eye_x, 2)]);
% 
%     print(gcf, [int2str(idx) '-sim.png'], '-dpng', '-r300');
    
    %% plot image adjustments
    %Compare adjustments made to the imade during drifts and non-drift
    %movements to ensure that the image is only being moved during drifts.
    % false starts
    non_drift_t = [];
    non_drift_x = [];
    non_drift_y = [];

    drift_t = [];
    drift_x = [];
    drift_y = [];

    last = 1;
    for i = 1:size(Result.drifts.start, 2)
        % current drift
        start = Result.drifts.start(i);
        dur = Result.drifts.duration(i);

        % non drifts, from last:start - 1
        if last < start - 1
            % make relative alt
            tt = (adj_t >= last & adj_t < start);
            % only append if non-zero movement
            if abs(sum(adj_x(tt))) > 0 || abs(sum(adj_y(tt)) > 0)
                non_drift_t = [non_drift_t adj_t(tt) nan];
                non_drift_x = [non_drift_x cumsum(adj_x(tt)) nan];
                non_drift_y = [non_drift_y cumsum(adj_y(tt)) nan];
            end
        end

        % drifts
        tt = (adj_t >= start & adj_t < start + dur);
        drift_t = [drift_t adj_t(tt) nan];
        drift_x = [drift_x cumsum(adj_x(tt)) nan];
        drift_y = [drift_y cumsum(adj_y(tt)) nan];

        % update last
        last = start + dur;
    end

    % final non-drift
    start = size(Result.drifts.start, 2);
    if last < start - 1
        % make relative alt
        tt = (Data.stream00{idx}.ts >= last & Data.stream00{idx}.ts < start);

        % only append if non-zero movement
        if abs(sum(Data.stream00{idx}.data(tt))) > 0 || abs(sum(Data.stream01{idx}.data(tt)) > 0)
            non_drift_t = [non_drift_t adj_t(tt) nan];
            non_drift_x = [non_drift_x cumsum(adj_x(tt)) nan];
            non_drift_y = [non_drift_y cumsum(adj_y(tt)) nan];
        end
    end

    % plot
    subplot(2,2,1); plot(drift_t, drift_x); title('Horizontal adjustments during drift'); xlabel('t'); ylabel('x'); xlim([1 size(eye_x, 2)]);
    subplot(2,2,3); plot(non_drift_t, non_drift_x, 'r'); title('Horizontal adjustments outside drift'); xlabel('t'); ylabel('x'); xlim([1 size(eye_x, 2)]);
    subplot(2,2,2); plot(drift_t, drift_y); title('Vertical adjustments during drift'); xlabel('t'); ylabel('y'); xlim([1 size(eye_x, 2)]);
    subplot(2,2,4); plot(non_drift_t, non_drift_y, 'r'); title('Vertical adjustments outside drift'); xlabel('t'); ylabel('y'); xlim([1 size(eye_x, 2)]);

    print(gcf, [int2str(idx) '-verify.png'], '-dpng', '-r300');
end
