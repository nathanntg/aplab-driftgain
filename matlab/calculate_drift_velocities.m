function [V, VX, VY, M_v, M_vend]  = calculate_drift_velocities(Trials, par, CS, MinDuration)
%CALCULATE_DRIFT_VELOCITIES List of all drift velocities in armin/sec for
%one or more trials.
%   par - the smoothing factor for ocular drift (we generally use 41, which 
%         low pass with a 30 Hz cut off)
%   CS - the amount you want to cut the "edges" of your drift segment, in 
%        this case we can cut half the size of the filter (Par) on both 
%        edges to avoid artifacts from the filter, or we can be more 
%        conservative
%   MinDuration - the minimum duration for a drift to be considered (it 
%                 cannot be lower than the filter size, you can set it at 
%                 about 100 ms)

% default values
if nargin < 2
    par = 41;
end
if nargin < 3
    CS = round(par / 2);
end
if nargin < 4
    MinDuration = 100;
end

V = []; 
VX = []; VY = [];
cont = 0;

% for each trial
for ii = 1:length(Trials)
    if (length(Trials)>1)
        T = Trials{ii};
    else
        T = Trials;
    end
    
    % for each drift
    for nn = 1:length(T.drifts.start)
        % get start and end
        S = T.drifts.start(nn);
        E = S + T.drifts.duration(nn) - 1;
        
        % confirm end is within analysis period
        if E>(T.analysis.start+T.analysis.duration-1)
            E = (T.analysis.start+T.analysis.duration-1);
        end
        
        % confirm start is within analysis period
        if S<(T.analysis.start)
            S = T.analysis.start;
        end

        % get eye positions
        X = T.x.position(S:E);
        Y = T.y.position(S:E);
        
        % longer than minimum duration
        if ((E-S)>(MinDuration))
            % apply filter, if specified
            if ~isempty(par)
                X = sgolayfilt(double(X),3,par);
                Y = sgolayfilt(double(Y),3,par);
            end
            
            % calculate differences
            X1 = diff(X(CS:end-CS));
            Y1 = diff(Y(CS:end-CS)); 
            
            % eliminate outliers
            %id = find(abs(X1*1000)>200 | abs(Y1*1000)>200);
            %if isempty(id)

            % calculate velocity
            Velocity = sqrt(X1.^ 2 + Y1.^ 2)* 1000;
            
            % append to data
            V = [V mean(Velocity)];
            VX = [VX X1*1000];
            VY = [VY Y1*1000];
            % velocity matrix
            if length(Velocity)>200
                cont = cont + 1;
                M_v(cont, 1:200) = Velocity(1:200);
                M_vend(cont, 1:200) = Velocity(end-199:end);
            end
            %end
        else
            V = [V nan];
        end
    end
end